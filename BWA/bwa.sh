module load bwa/0.7.5a
module load samtools/1.7

REF_GENOME="labs/Wegrzyn/Urban_RAD_ArcticGrayling/ref_genome/bwa_mem_files/eg_index"
DATAFOLDER="/labs/Wegrzyn/Urban_RAD_ArcticGrayling/STACKS/process/fq_files"
cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/
mkdir -p bwa_bam_files/LG_bam_files
cd bwa_bam_files
for r in "$DATAFOLDER"/*.1.fq.gz
do
        FQ1=$r
        FQ2=${r/.1.fq.gz/}.2.fq.gz
        name=$(basename "$r")
        echo $FQ1 $FQ2
        ID="@RG\tID:ind\tSM:ind\tPL:IonProton"
        
        bwa mem -t 45 -k 19 -c 500 -O 0,0 -E 2,2 -T 0 -R "$ID" 
                $REF_GENOME $FQ1 $FQ2 | \
                samtools view -Sb -q 1 -F 4 -F 256 -F 2048 \
                - > "${name%.1.fq.gz}".bam

        samtools sort -@45 -o "${name%.1.fq.gz}".sorted.bam \
                "${name%.1.fq.gz}".bam

        samtools index "${name%.1.fq.gz}".sorted.bam 

        rm "${name%.1.fq.gz}".bam
done

cd bwa_bam_files
mkdir LG_bam_files

file=LG.ind.txt

cat LG.ind.txt | tr -d '\r' | while read line;
do
        cp -v $line.sorted.bam ./landscape_genomics_sorted_bam_files/$line.sorted.bam
done

module load bamtools

cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/bwa/bwa_sorted_bam_files

for i in *.bam;
do
    echo $i
    bamtools stats -in $i | grep "Mapped reads:" -
done
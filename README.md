Main Directory **/labs/Urban/Urban_RAD_ArcticGrayling** 

[[_TOC_]]

# Introduction

This project presents the bioinformatic methods applied to a landscape genomic study based on arctic grayling (Thymallus arcticus) populations in North Slope, AK.
ddRAD sequences from 478 individuals were genotyped and associated with ibutton and migratory data to determine if clinal variation can be explained by environmental heterogeneity.
Within this README.md you will find the programs and computational resources used on the CBC Xanada cluster. 
Within the Landscape Genomics directory you will code necessary for data wrangling and visualization.  

# Cleaning/Demultiplexing Reads

Cleaning & Demultiplexing Script

Pooled reads underwent demultiplexing, trimming, and quality control under the program process_radtags. 

```shell
#!/bin/bash
#SBATCH --job-name=process_radtags
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --array=[0-11]
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling

module load stacks/2.53


INDIR=./raw_fastq/raw_fastq_files/
META=./STACKS/process/p1_barcodes/
OUTDIR=./STACKS/process/fq_files
mkdir -p $OUTDIR

POOL=($(ls $INDIR/*R1*))
IND=({001..012})
FQ1=${POOL[$SLURM_ARRAY_TASK_ID]}
FQ2=$(echo ${POOL[$SLURM_ARRAY_TASK_ID]} | sed 's/R1/R2/')
IND1=${IND[$SLURM_ARRAY_TASK_ID]}
BARCODE="$META"barcodes_${IND1}.txt

# run stacks process_radtags
process_radtags \
-i gzfastq \
-1 $FQ1 \
-2 $FQ2 \
-b ./STACKS/process/p1_barcodes/$BARCODE \
-o $OUTDIR \
-c -q -r \
--inline_null \
--renz_1 sbfI --renz_2 mseI


mv $OUTDIR/*rem* ./STACKS/process/rem_seq
```

**Table 2.** Percentage of reads retained for the 12 Golden-Pool samples following process radtags

| Golden-Pool Sample (n) | Total Sequences  (n)       	| Retained Reads (%)	| 
|--------------------	|------------	|-----------------------|
| 1                  	| 105029414              |    98.3       	|
| 2                  	| 106114720              |    97.4       	|
| 3                  	| 111539008              |    97.6       	|
| 4                  	| 104574586              | 98         	|
| 5                  	| 104667018              | 97.1       	|
| 6                  	| 104634692              | 97.4       	|
| 7                  	| 109806242              | 98.3       	|
| 8                  	| 114361838              | 97.8       	|
| 9                  	| 106929538              | 98.2       	|
| 10                 	| 105308820              | 98         	|
| 11                 	| 93926374              | 98.3       	|
| 12                 	| 103878178              | 98.3       	|

A **full summary** from process radtags can be found **here** [.err file](Process RADtags/process_radtags_err_summary.xlsx). 

Paired reads were demulitplexed into their respective individual resulting in 4 different file outputs per individual:

* **Individual.1.fq.gz** Retained forward reads  

* **Individual.2.fq.gz** Retained reverse reads     

* **Individual.rem.1.fq.gz** Removed forward reads

* **Individual.rem.2.fq.gz**  Removed reverse reads 

Path to **retained** reads ./STACKS/process/fq_files

Path to **discarded** reads ./STACKS/process/rem_seq. 


## FastQC & MultiQC Reports

Following Cleaning/Demultiplexing, FastQC reports were performed on all demultiplexed samples outputed from process radtags generated for all individual Golden samples forward and reverse reads (n=1106). MultiQC compiles FastQC reports into one file and its output is stored within ./STACKS/process/multiqc_output. In respect to **per sequence GC content**, *24* samples recieved warning messages and *101* samples received failing messages. For **sequence duplication levels**, *1102* forward and reverse samples failed to meet the criteria. 
Lastly, for **overrepresented sequences**, *988* forward and reverse samples received warning messages and *11* samples failed to meet the criteria. 
Although, with ddRAD protocal we can expect flaggings for **sequence duplication levels** and **overrepresented sequences**. Therefore, no samples were dropped from analysis based on MultiQC reports. 

# Aligning Reads To A Reference Genome

All processed reads were aligned to a sister species, the European grayling(_Thymallus Thymallus_), using the burrows-wheeler alignment tool. 
Generated files were converted to bam format and sorted based on chromosomal location via samtools sort. 
Percentage of reads mapped to the reference genome was measured using bamtools stats. 

**Table 3.** _Thymallus Thymallus_ reference genome global statistics. Accessable through GenBank with GCA_004348285.1.

| Stastic Description         |  (n)         |
|--------------------	|------------	|
| Total sequence length    | 1,564,834,359  |
| Total ungapped length    | 1,455,249,978	|
| Gaps between scaffolds | 0       	|
| Number of scaffolds | 3,831         	|
| Scaffold N50 | 32,985,317       	|
| Scaffold L50 | 21       	|
| Number of contigs | 204,386       	|
| Contig N50         | 31,774       	|
| Contig L50       |  12,082         |
| Total number of chromosomes and plasmids       |   52        |
| Number of component sequences (WGS or clone)	    |   3,831        |

Read Alignment Script

```shell
#!/bin/bash
#SBATCH --job-name=bwa
#SBATCH -o ../err_out/bwa/%x_%A_%a.out
#SBATCH -e ../err_out/bwa/%x_%A_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=8G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[0-552]%100

cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling

module load bwa/0.7.17
module load samtools/1.10

INDIR=./STACKS/process/fq_files
OUTDIR=./bwa
mkdir -p $OUTDIR
REFERENCE=./ref_genome/bwa_index/eg_index
FASTQS=($(ls $INDIR/*1.fq.gz | grep -v "rem...fq.gz"))
FQ1=$(echo ${FASTQS[$SLURM_ARRAY_TASK_ID]})
FQ2=$(echo $FQ1 | sed 's/1.fq.gz/2.fq.gz/')
SAM=$(basename $FQ1 .1.fq.gz)
BAM=${SAM}.bam

# get the sample ID and use it to specify the read group.
RG=$(echo \@RG\\tID:$SAM\\tSM:$SAM)

echo $OUTDIR
echo $BAM

bwa mem -t 4 -R $RG $REFERENCE $FQ1 $FQ2 | \
samtools view -S -h -u - | \
samtools sort -T /scratch/${SAM}_${USER} - >$OUTDIR/$BAM

samtools index $OUTDIR/$BAM

```

Path to **aligned reads**  ./bwa/bwa_sorted_bam_files

Aligned reads were then further split up based on their experimental goals:

Path to **Landscape Genomics** aligned reads: ./bwa/bwa_sorted_bam_files/landscape_genomics_sorted_bam_files

**Table 4.** Individuals dropped due to low mapping percentages

|submission.id | Aligned_Reads|location |region |project           |
|:-------------|-------------:|:--------|:------|:-----------------|
|Golden1A06    |        0.1271|Itk4     |Itk    |LandscapeGenomics |
|Golden1A08    |        0.1206|Itk4.5   |Itk    |LandscapeGenomics |
|Golden1B06    |        0.1329|Itk4     |Itk    |LandscapeGenomics |
|Golden1B07    |        0.1207|Itk4     |Itk    |LandscapeGenomics |
|Golden1C07    |        0.1330|Itk4     |Itk    |LandscapeGenomics |
|Golden1C08    |        0.1187|Itk4.5   |Itk    |LandscapeGenomics |
|Golden1D08    |        0.1196|Itk4.5   |Itk    |LandscapeGenomics |
|Golden1E06    |        0.1281|Itk4     |Itk    |LandscapeGenomics |
|Golden1E07    |        0.1194|Itk4     |Itk    |LandscapeGenomics |
|Golden1F07    |        0.1209|Itk4.5   |Itk    |LandscapeGenomics |
|Golden1G05    |        0.1280|Itk4     |Itk    |LandscapeGenomics |
|Golden1G06    |        0.1312|Itk4     |Itk    |LandscapeGenomics |
|Golden1G07    |        0.1194|Itk4.5   |Itk    |LandscapeGenomics |
|Golden1H05    |        0.1305|Itk4     |Itk    |LandscapeGenomics |
|Golden1H06    |        0.1238|Itk4     |Itk    |LandscapeGenomics |
|Golden1H07    |        0.1172|Itk4.5   |Itk    |LandscapeGenomics |
|Golden2G10    |        0.1015|LKup3    |Kup    |LandscapeGenomics |
|Golden4E12    |        0.0271|KUS      |Kup    |SNP_Selection     |
|Golden4F12    |        0.0171|KUS      |Kup    |SNP_Selection     |
|Golden4G12    |        0.0027|KUS      |Kup    |SNP_Selection     |

# Landscape Genomics Project

At this step Landscape genomics project comprises 490 samples, 43 populations and 1 group.

in case you need to remove individuals from the popmap.txt use: awk 'NR==FNR{a[$1];next} !($1 in a)' bad_samples.txt LG_popmap.txt > LG_popmap_F.txt

Call variants using Freebayes Software

```shell
#!/bin/bash
#SBATCH --job-name=freebayes_parallel
#SBATCH --mail-user=gabriel.barrett@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 30
#SBATCH --mem=90G
#SBATCH --qos=general
#SBATCH --partition=general

cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/

hostname
date

##################################
# run freebayes to call variants
##################################
####### load software---------------------------------------------------------------
module load htslib/1.10.2
module load freebayes/1.3.1
module load vcflib/1.0.0-rc1
module load parallel/20180122

####### input, output files, directories--------------------------------------------
INDIR=./bwa/
OUTDIR=./freebayes/results
mkdir -p $OUTDIR

#reference genome
GEN=./ref_genome/GCA_004348285.1_ASM434828v1_genomic.fna

# make a list of bam files
        # use popmap_total.txt to construct the list
POPMAP=./meta/LG_popmap.txt
BAMLIST=$OUTDIR/bam.list
cat $POPMAP | cut -f 1 | sed 's/$/.bam/' | sed  "s,^,$INDIR," > $BAMLIST

# these are freebayes scripts found in the same location as the executable
MAKEREGIONS=/isg/shared/apps/freebayes/1.3.1/scripts/fasta_generate_regions.py

######### run freebayes-parallel--------------------------------------------------------
bash freebayes_parallel.sh \
        <(python $MAKEREGIONS ${GEN}.fai 5000000) 30 \
        -f ${GEN} \
        --bam-list $OUTDIR/bam.list \
        -m 30 \
        -q 20 \
        --min-coverage 1000 \
        --skip-coverage 50000 | \

bgzip -c > $OUTDIR/fb_parallel.vcf.gz

# index the vcf file
tabix -p vcf $OUTDIR/fb_parallel.vcf.gz
```

Freebayes software genotyped 213578 loci

## Filtering VCF

**Steps For Filtering**

---

1. Generate statistics with vcftools on a per site basis
2. Filter bad sites
	- mean site depth (mean + 2*sqrt(sd))
	- per-watershed missingness
	- minor allele frequency
	- per-watershed hardy-weinberg equilibrium
3. Identify problematic individuals 
    - missing data
    - Relatedness
    - Heterozygosity
4. Remove bad samples from original VCF
5. Remove potential paralogs
6. Re-apply Filter on new VCF

---

**Generate statistics with vcftools on a per site basis**

<img src="./Landscape Genomics/VCF/VCF_PLOTS/SNP_stats1.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "450"
     width = "600"/>

**Figure 1.** 


**Filter VCF**

vcftools parameter key

- minDP : minimum depth allowed for a genotype (marks as missing)
- maxDP : maximum depth allowed for a genotype (marks as missing)
- min-meanDP : minimum mean depth values (over all individuals)
- max-meanDP : maximum mean depth values (over all individuals)
- maf : minor allele frequency (Removed alleles that are called less than 8.82x (.9% of 980))
- max-missing : excludes sites based on the proportion missing, where 0 allows completely missing sites and 1 indicates no missing data allowed

bcftools parameter key

- AB : Allele Balance (allows for the catchment of fixed variants)
- SAF & SAR & SRF & SRR : filters reads from both forward and reverse strands
- MQM & MQMR : mapping quality ratio between reference and alternative alleles respectively
- PAIRED & PAIREDDR : properly paired status for reads supporting reference or alternative alleles
- INFO/DP / QUAL : removes potential Parologs http://arxiv.org/pdf/1404.0929.pdf (may substitute other method)

```shell
#!/bin/bash
module load vcftools
module load bcftools
module load vcflib
#cd /labs/Urban/Urban_RAD_ArcticGrayling/LG_PIPELINE
FILE=freebayes.vcf.gz
FILTERFILE=fb.maf.01.minQ20.minDP3.maxDP67.vcf.gz
FILTERFILE2=$(echo $FILTERFILE | sed 's/vcf.gz/miss65.vcf/')
FILTERFILE3=$(echo $FILTERFILE2 | sed 's/vcf/ab.bs.mq.ps.pp.vcf/')
FILTERFILE4=$(echo $FILTERFILE3 | sed 's/vcf/snps.vcf/')
FILTERFILE5=$(echo $FILTERFILE4 | sed 's/vcf/ri.vcf/')
FILTERFILE6=$(echo $FILTERFILE6 | sed 's/vcf/hwe.vcf')

# Filter Low Quality Sites
##########################

vcftools --gzvcf $FILE --maf .01 --minQ 20 \
--minDP 3 --min-meanDP 3 \
--maxDP 67 --max-meanDP 67 \
--recode --recode-INFO-all --stdout | gzip -c > $FILTERFILE

# PER-POP MISSINGNESS
##########################

POPMAP=REGMAP.txt
sh filter_miss_by_pop.sh $FILTERFILE $POPMAP .65 1 $FILTERFILE2
rm *Kup* *Sag* *Col*

# INFO
########

bcftools filter -i'AB>0.25 && AB<0.75 | AB < 0.01' $FILTERFILE2 | \
bcftools filter -i'SAF / SAR > 100 && SRF / SRR > 100 | SAR / SAF > 100 && SRR / SRF > 100' | \
bcftools filter -i'MQM / MQMR > 0.9 && MQM / MQMR < 1.05' | \
bcftools filter -i'PAIRED > 0.05 && PAIREDR > 0.05 && PAIREDR / PAIRED < 1.75 & PAIREDR / PAIRED > 0.25 | PAIRED < 0.05 & PAIREDR < 0.05' | \
bcftools filter -i'QUAL/ INFO/DP > .25' > $FILTERFILE3

# HWE !Decompose into SNPs!
############################

vcfallelicprimitives $FILTERFILE3 --keep-info --keep-geno > $FILTERFILE4
vcftools --vcf $FILTERFILE4 --remove-indels --recode --recode-INFO-all --stdout > $FILTERFILE5
perl filter_hwe_by_pop.pl -v $FILTERFILE5 -p $POPMAP -h 0.01 -c .5 -o $FILTERFILE6
rm *Kup* *Sag* *Col*
vcftools --vcf $FILTERFILE5 --exclude-positions exclude.hwe --recode --recode-INFO-all --stdout > $FILTERFILE6
```

**Figure 3.** Statistics on a per individual basis.

**Removed** individuals that were missing 50% or greater of the total sites, had an individual mean site depth greater than 100 and less than 3, and selected individuals with a AJK statistic that's greater than .75 (displayed in Figure 2.). 

**Table 5.** Problematic Individuals with their corresponding flaggings (DP:depth, fmiss: fraction genotypes missing, AJK: Relatedness Statistic, HET: inbreeding coefficient)

| Individual | Flag(s)	    	|
|------------|------------	|
| Golden4E07 | fmiss       	|
| Golden4H11 | low_DP, fmiss    |
| Golden1A09 | fmiss, AJK       |
| Golden3H10 | low_DP, fmiss, Het |
| Golden1A03 | HET, AJK		|
| Golden1A01 | AJK       	|
| Golden1C03 | AJK       	|
| Golden1F02 | AJK       	|


After filtering 482 Individuals, kept 5802 out of 213578 variant sites.

## Population Structure

### Linkage Disequilibrium

PCA and Population Structure analysis assumes loci that are independent from one another. Therefore, we measured linkage across sites and removed sites highly correlated with one another from analysis using plink software. (Note Plink expects compressed vcf and number of chromosomes equal to a human, however, this can be overrided with --allow-extra-chr)
```shell
PLINK_OUT=../plink/LG.snps1.FFP5.MAF.008.HWE.01.MISS.65.MD.69
PLINK_OUT_PRUNED=../plink/LG.snps1.FFP5.MAF.008.HWE.01.MISS.65.MD.69.pruned

module load plink/1.90.beta.4.4

plink --vcf $FINAL_OUT --allow-extra-chr --double-id \
--set-missing-var-ids @:# \
--indep-pairwise 100 10 0.1 --recode --out $PLINK_OUT

plink --file $PLINK_OUT --double-id --allow-extra-chr \
--set-missing-var-ids @:# --extract $PLINK_OUT.prune.in \
--make-bed --pca --out $PLINK_OUT_PRUNED
```
Any variant site within a 100bp sliding window (advanced by 10 base paires each time) that has a R^2 (linkage correlation) value greater than 0.1 are removed/pruned.

Retained **3301** out of 5802 variants for PCA and structure analysis. 


### Principal Component Analysis (PCA) 

PCA analysis was performed using plink software. 
```shell
module load plink/1.90.beta.4.4

VCF=../raw_vcf/LG.snps.FF.vcf.gz
OUT=LG
plink --vcf $VCF --double-id --allow-extra-chr \
--set-missing-var-ids @:# --extract $OUT.prune.in \
--make-bed --pca --out $OUT
```

Plink software resulted in 

<details>
<summary markdown="span"> Population Sample Size</summary>

*Table 6.* Population Sample Size
| Population  | n  |
| :---------- | -: |
| IMO         | 20 |
| IMR1        |  3 |
| IMR2        |  3 |
| BJANE       |  4 |
| LIMS        | 25 |
| IMR3        |  5 |
| IM3         | 13 |
| LL3         |  8 |
| Itk3        |  8 |
| Itk4        |  8 |
| Itk4.5      |  1 |
| GCL         | 20 |
| CGK         | 24 |
| Kup2        | 18 |
| KUS         | 17 |
| Toolik-S3   |  1 |
| LTER        |  5 |
| KupR2       |  4 |
| Toolik-TOAS |  1 |
| Kup6        | 20 |
| KupR4       |  3 |
| Kup7        | 17 |
| KLAS5       |  5 |
| Kup         |  3 |
| Kup8        | 20 |
| LKup        | 10 |
| LKup0       | 13 |
| LKup1       | 13 |
| LKup3       |  6 |
| OksLCS      | 12 |
| UZ          | 12 |
| OKm1        |  5 |
| OksR1       |  8 |
| Oks0        | 20 |
| OksLTER     |  3 |
| OksBH       |  1 |
| Oks2        | 20 |
| OksRN4      |  5 |
| Oks3        | 20 |
| OKS3        | 20 |
| OksSag      |  2 |
| CGO3        |  9 |
| LSag        |  9 |
| HV          | 11 |
| LSag2       |  3 |
| TC          | 20 |


</details>

<img src="./Landscape Genomics/PCA/PCA-1-1.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "575"/>

**Figure 3.** Principal Component Analysis (PCA) 

### fastStructure

Analyzed population structuring from the LD pruned bed file using fastStructure.

```shell
#!/bin/bash
#SBATCH --job-name=faststructure_array
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 17
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=gabriel.barrett@uconn.edu
#SBATCH --array=[0-16]
#SBATCH -o ../err_out/%x_%A_%a.out
#SBATCH -e ../err_out/%x_%A_%a.err

cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/STACKS/LG_refmap/vcf_files/faststructure

module load fastStructure/1.0

BED="../plink/LG.snps1.FFP5.MAF.01.HWE.01.MISS.75.MD.69.pruned"
OUT="./simple/LG.snps1.FFP5.MAF.01.HWE.01.MISS.75.MD.69.simple"

K=($(seq 3 20))
ARRAY_K=${K[$SLURM_ARRAY_TASK_ID]}

echo "SLURM_JOB_ID=${SLURM_JOB_ID}"
echo "SLURM_ARRAY_TASK_ID=${ARRAY_K}"

structure.py -K ${ARRAY_K} --input $BED --output $OUT --cv 25
```

Determining the correct choice of model complexity (K) can be quiet problematic for populations with low genetic divergence. 
Therefore, we chose a model complexity K that expressed the lowest cross-validation error, maximimizes the marginal-likelihood, and mean-field variational scheme. 
Real Population structure will never conform to model assumptions, thus, there will never be a true value of K.
It's important to note that these results are likely to be influenced by sampling ascertainment scheme bias. 

**Latitude** dataset: 
Model complexity that maximizes marginal likelihood = 3
Model components used to explain structure in data = 9
Cross-Validation error = 18

**pit** dataset: 
Model complexity that maximizes marginal likelihood = 3
Model components used to explain structure in data = 5
Cross-Validation error = 19

**temp** dataset:
Model complexity that maximizes marginal likelihood = 3
Model components used to explain structure in data = 9
Cross-Validation error = 19

**temp2** dataset:
Model complexity that maximizes marginal likelihood = 3
Model components used to explain structure in data = 6
Cross-Validation error = 8

<img src="./Landscape Genomics/ADMIXTURE/"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "300"
     width = "560"/>

**Figure 5.** Cross-validation error for different model complexities K 

<img src="./Landscape Genomics/faststructure/Plots/fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.lat.pruned.logistic.7.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "250"
     width = "1450"/>

<img src="./Landscape Genomics/faststructure/Plots/fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.lat.pruned.logistic.12.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "250"
     width = "1450"/>

<img src="./Landscape Genomics/faststructure/Plots/fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.lat.pruned.logistic.18.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "250"
     width = "1450"/>


**Figure 6.** Population structuring for K = 14, K = 15, K = 16. Lower regions are expected to have allelic combinations most similar to the ancestral population and appear as admixtured in respect to the modern individuals in the headwaters (Lawson et al. 2018).

**Figure 7.** Pie Charts showing sampling locations average ancestral coefficients. 

## Population Summary Statistics

**Table 7.** Summary Statistics in order based on headwaters to coast(Colville, Kuparuk, and Sagavanirktok). location, Alleles private to that location, observed heterozygosity, expected heterozygosity, nucleotide diversity, and inbreeding coefficent.

<details>
<summary markdown="span"> Population Sample Size</summary> 

| Location | Region        | up\_down\_lower |      lat |       long | H<sub>o</sub> | H<sub>e</sub> | pi            | F<sub>is</sub>  |
| :------- | :------------ | :-------------- | -------: | ---------: | :------------ | :------------ | :------------ | :-------------- |
| IMO      | Colville      | up              | 68.55795 | \-149.5888 | 0.264 ± 0.044 | 0.256 ± 0.03  | 0.264 ± 0.032 | 0.009 ± 0.062   |
| IMR1     | Colville      | up              | 68.56369 | \-149.6494 | 0.237 ± 0.092 | 0.202 ± 0.042 | 0.255 ± 0.072 | 0.03 ± 0.094    |
| IMR2     | Colville      | up              | 68.58665 | \-149.7223 | 0.27 ± 0.105  | 0.212 ± 0.043 | 0.268 ± 0.075 | \-0.004 ± 0.084 |
| BJANE    | Colville      | down            | 68.59220 | \-149.7278 | 0.256 ± 0.099 | 0.202 ± 0.042 | 0.254 ± 0.071 | \-0.003 ± 0.077 |
| LIMS     | Colville      | down            | 68.60561 | \-149.7363 | 0.261 ± 0.041 | 0.257 ± 0.029 | 0.264 ± 0.031 | 0.017 ± 0.059   |
| IMR3     | Colville      | down            | 68.62248 | \-149.7758 | 0.259 ± 0.075 | 0.225 ± 0.037 | 0.258 ± 0.051 | \-0.001 ± 0.09  |
| IM3      | Colville      | down            | 68.62373 | \-149.7778 | 0.266 ± 0.053 | 0.248 ± 0.032 | 0.263 ± 0.037 | \-0.003 ± 0.072 |
| LL3      | Colville      | down            | 68.62343 | \-149.7926 | 0.259 ± 0.063 | 0.235 ± 0.034 | 0.257 ± 0.042 | \-0.001 ± 0.09  |
| Itk3     | Colville      | lower           | 68.93907 | \-150.1313 | 0.268 ± 0.065 | 0.245 ± 0.034 | 0.269 ± 0.043 | 0.008 ± 0.095   |
| Itk4     | Colville      | lower           | 69.45285 | \-150.7112 | 0.264 ± 0.059 | 0.246 ± 0.033 | 0.269 ± 0.041 | 0.015 ± 0.095   |
| Itk4.5   | Colville      | lower           | 69.93510 | \-150.8555 | 0.271 ± 0.198 | 0.136 ± 0.049 | 0.271 ± 0.198 | 0 ± 0           |
| GCL      | Kuparuk       | up              | 68.53717 | \-149.2482 | 0.213 ± 0.049 | 0.205 ± 0.035 | 0.212 ± 0.038 | 0.004 ± 0.053   |
| CGK      | Kuparuk       | up              | 68.53722 | \-149.2483 | 0.218 ± 0.047 | 0.209 ± 0.035 | 0.215 ± 0.037 | \-0.003 ± 0.046 |
| Kup2     | Kuparuk       | up              | 68.54037 | \-149.2827 | 0.205 ± 0.064 | 0.176 ± 0.039 | 0.182 ± 0.042 | \-0.054 ± 0.049 |
| KUS      | Kuparuk       | up              | 68.57208 | \-149.3470 | 0.21 ± 0.047  | 0.204 ± 0.035 | 0.212 ± 0.038 | 0.009 ± 0.054   |
| LTER     | Kuparuk       | down            | 68.64480 | \-149.4040 | 0.229 ± 0.07  | 0.197 ± 0.037 | 0.22 ± 0.048  | \-0.018 ± 0.066 |
| KupR2    | Kuparuk       | down            | 68.67006 | \-149.4439 | 0.213 ± 0.086 | 0.175 ± 0.04  | 0.212 ± 0.063 | \-0.002 ± 0.07  |
| Kup6     | Kuparuk       | down            | 68.76611 | \-149.5547 | 0.215 ± 0.048 | 0.207 ± 0.035 | 0.213 ± 0.037 | 0.001 ± 0.054   |
| KupR4    | Kuparuk       | down            | 68.77458 | \-149.5718 | 0.211 ± 0.105 | 0.156 ± 0.042 | 0.21 ± 0.086  | \-0.001 ± 0.058 |
| Kup7     | Kuparuk       | down            | 68.90850 | \-149.6880 | 0.223 ± 0.054 | 0.207 ± 0.036 | 0.216 ± 0.04  | \-0.012 ± 0.053 |
| KLAS5    | Kuparuk       | down            | 68.96521 | \-149.7017 | 0.23 ± 0.079  | 0.193 ± 0.04  | 0.224 ± 0.055 | \-0.012 ± 0.075 |
| Kup      | Kuparuk       | down            | 68.99595 | \-149.7528 | 0.233 ± 0.099 | 0.174 ± 0.041 | 0.219 ± 0.069 | \-0.025 ± 0.055 |
| Kup8     | Kuparuk       | down            | 68.99595 | \-149.7528 | 0.224 ± 0.049 | 0.213 ± 0.034 | 0.22 ± 0.037  | \-0.005 ± 0.057 |
| LKup     | Kuparuk       | down            | 68.99595 | \-149.7528 | 0.219 ± 0.055 | 0.206 ± 0.035 | 0.22 ± 0.041  | 0.004 ± 0.069   |
| LKup0    | Kuparuk       | lower           | 69.26168 | \-150.2392 | 0.227 ± 0.053 | 0.212 ± 0.035 | 0.225 ± 0.04  | \-0.003 ± 0.062 |
| LKup1    | Kuparuk       | lower           | 69.60034 | \-150.0373 | 0.221 ± 0.052 | 0.209 ± 0.034 | 0.221 ± 0.038 | 0.006 ± 0.067   |
| LKup3    | Kuparuk       | lower           | 69.96382 | \-149.4587 | 0.214 ± 0.065 | 0.198 ± 0.038 | 0.226 ± 0.052 | 0.026 ± 0.08    |
| OksLCS   | Sagavanirktok | up              | 68.60567 | \-149.1981 | 0.182 ± 0.06  | 0.164 ± 0.038 | 0.178 ± 0.046 | \-0.004 ± 0.05  |
| UZ       | Sagavanirktok | up              | 68.61399 | \-149.1989 | 0.182 ± 0.055 | 0.17 ± 0.037  | 0.179 ± 0.042 | \-0.003 ± 0.051 |
| OKm1     | Sagavanirktok | up              | 68.61997 | \-149.1895 | 0.173 ± 0.074 | 0.144 ± 0.038 | 0.167 ± 0.052 | \-0.009 ± 0.062 |
| OksR1    | Sagavanirktok | up              | 68.65972 | \-149.1880 | 0.179 ± 0.064 | 0.152 ± 0.037 | 0.166 ± 0.044 | \-0.027 ± 0.047 |
| Oks0     | Sagavanirktok | up              | 68.63948 | \-149.2170 | 0.182 ± 0.053 | 0.167 ± 0.037 | 0.173 ± 0.039 | \-0.019 ± 0.039 |
| OksLTER  | Sagavanirktok | up              | 68.67043 | \-149.1373 | 0.174 ± 0.085 | 0.141 ± 0.039 | 0.176 ± 0.064 | 0.005 ± 0.07    |
| Oks2     | Sagavanirktok | down            | 68.76423 | \-148.9043 | 0.185 ± 0.056 | 0.17 ± 0.037  | 0.176 ± 0.04  | \-0.016 ± 0.052 |
| OksRN4   | Sagavanirktok | down            | 68.85051 | \-148.8541 | 0.202 ± 0.073 | 0.17 ± 0.038  | 0.194 ± 0.051 | \-0.013 ± 0.064 |
| Oks3     | Sagavanirktok | down            | 68.86289 | \-148.8441 | 0.2 ± 0.05    | 0.186 ± 0.036 | 0.192 ± 0.038 | \-0.014 ± 0.041 |
| OKS3     | Sagavanirktok | down            | 68.86308 | \-148.8449 | 0.2 ± 0.051   | 0.189 ± 0.037 | 0.195 ± 0.039 | \-0.006 ± 0.046 |
| OksSag   | Sagavanirktok | down            | 68.86308 | \-148.8449 | 0.194 ± 0.102 | 0.138 ± 0.04  | 0.191 ± 0.08  | \-0.005 ± 0.043 |
| CGO3     | Sagavanirktok | down            | 68.86339 | \-148.8450 | 0.195 ± 0.058 | 0.18 ± 0.038  | 0.194 ± 0.044 | 0 ± 0.06        |
| LSag     | Sagavanirktok | lower           | 68.86611 | \-148.8389 | 0.195 ± 0.057 | 0.182 ± 0.037 | 0.196 ± 0.044 | 0.004 ± 0.061   |
| HV       | Sagavanirktok | lower           | 69.15049 | \-148.8365 | 0.188 ± 0.058 | 0.173 ± 0.037 | 0.187 ± 0.045 | 0 ± 0.056       |
| LSag2    | Sagavanirktok | lower           | 69.73319 | \-148.6916 | 0.199 ± 0.092 | 0.159 ± 0.041 | 0.202 ± 0.069 | 0.006 ± 0.074   |
| TC       | Sagavanirktok | lower           | 69.95057 | \-148.6107 | 0.202 ± 0.056 | 0.181 ± 0.037 | 0.188 ± 0.039 | \-0.033 ± 0.044 |
</details>

## Environmental Correlations

Workflows presented below were performed 3 independent times based on individual environmental data:

| Environment | Sample Size | 
| :---------- | ----------: | 
|   Latitude  |     478     |
|     PIT     |     110     |
| Temperature |     461     |
| Temperature2|     285     |

<img src="./Landscape Genomics/GWAS/2018ENV_LineGraph.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "350"
     width = "1050"/>

**Figure 8.** Temperature2 regime observations in Celsius for Lower Colville, Kuparuk, and Sagavirnirktok Watersheds from the period 6/21/2018 - 9/1/2018 

### BayPass/2.2

Calculates population differientiation (XtX) and Bayes Factor for association from population allele frequencies.

1. vcf2baypass.sh - prepares full_group_file and subsets vcf for writing into baypass format
2. GWAS_preprocess_SCRIPT.R - averages by location and orders based on baypass pop order
3. Core Model - variance-covariance matrix, XtX, and Bayes Factor
4. iter_median.sh - calculates the median XtX, Bayes Factors, and Pearson correlation coefficent across 15 independent runs 

```shell
#!/bin/bash
#SBATCH --job-name=baypass_env
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=25
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem-per-cpu=2G
#SBATCH --mail-user=
#SBATCH -o err_out/%x_%A_%a.out
#SBATCH -e err_out/%x_%A_%a.err
#SBATCH --array=[0-14]

#BAYPASS=/labs/Wegrzyn/Urban_RAD_ArcticGrayling/STACKS/LG_refmap/vcf_files/baypass/source_code/baypass_2.2/sources/g_baypass
module load baypass/2.1
DIR=/labs/Urban/Urban_RAD_ArcticGrayling

ARRAY="lat pit temp temp2"
for i in $ARRAY;do
        GENO=""${DIR}"/BayPass/geno/baypass_$i"
        POP=$(head -n 1 $GENO | awk '{print NF}')

        ENV=""${DIR}"/BayPass/env/"${i}"_baypass.env"
        TENV=""${DIR}"/BayPass/env/int_"${i}"_baypass.env"
        EPOP=$(head -n 1 $ENV | awk '{print NF}')
        awk 'FNR == 2 {print}' $ENV > "${TENV}"

        echo "GENO: ${GENO}"
        echo "ENV: ${ENV} and ${TENV}"

        echo "pop in geno: ${POP}"
        echo "pop in env: ${EPOP}"

        r=${SLURM_ARRAY_TASK_ID}

        echo "output ${i}_output/${i}_${r}_aux" >> err_out/prefix.out

        g_baypass -npop $EPOP -gfile $GENO -efile $TENV \
        -nval 10000 -thin 25 -burnin 5000 -npilot 20 -pilotlength 10000 \
        -outprefix ${i}_output/${i}_${r}_core \
        -nthreads 25 -seed $RANDOM
done
```


### LEA (Latent Factor Mixed Model)

1. vcf2env.sh - joins environmental information per individual
2. LEA_{prefix}K.sh - explores population structure and supports a reasanable range of K's
3. lfmm_{prefix}.sh - Runs snmf(populationg genetic differientation) and lfmm (ecological association)
4. combined_results.R - Calculates z-scores from the snmf run that minimizes cross-entropy criteria and median z-scores across 15 independent runs of lfmm

```r
#!/bin/Rscript

library("LEA", lib="~/local/R_libs/")
library("dplyr", lib="~/local/R_libs")
library("ggplot2")

# input ----------------------------------------------------------------------
prefix <- "fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.beagle"
ped <- "../../freebayes/results/fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.beagle.ped"

lat_geno <- ped2geno(ped, output.file=paste0(prefix,".geno"),force=F)
lat_lfmm <- ped2lfmm(ped, output.file=paste0(prefix,".lfmm"),force=F)

# population genetic differentiation -----------------------------------------
proj.snmf <- snmf(lat_geno,K=5,entropy=T,ploidy=2,project="new",alpha=10,tolerance=0.0001,repetitions=15,iterations=1000000,CPU=25,percentage=.75)

# fst values from min cross-entropy run #
source('../fst_function.R')
best <- which.min(cross.entropy(proj.snmf, K = 5))
fst.values <- fst(proj.snmf, K = 5, run = best)

# z-scores #
n <- dim(Q(proj.snmf, K =5, run = best))[1]
fst.values[fst.values<0] <- 0.000001
z.scores <- sqrt(fst.values*(n - 5)/(1 - fst.values))
write.table(x=z.scores,file="lat_GD_zscores.txt",quote=F,row.names=F,col.names="lat_z")

# latent factor mixed model -----------------------------------------------------------------------
proj.lfmm <- lfmm(lat_lfmm, "lat.env", K = 5, repetitions = 15, project = "new", iterations = 100000, burnin = 50000, CPU = 25, missing.data = FALSE)

# z-scores from all repititions #
zv <- data.frame(z.scores(proj.lfmm, K = 5))
zv_median <- zv %>% rowwise() %>% mutate(lat_z = median(c_across(everything()))) %>% select(lat_z)
write.table(x=zv_median, file = "lat_EA_zmedian.txt",quote=F,row.names=F,col.names=T)
```

### Univariate Data Distribution

<img src="./Landscape Genomics/GWAS/GWAS/plots/univariate GWAS-1.png"
     alt="Markdown Monster icon"
     style="float: left;"
     width = 45%/>
<img src="./Landscape Genomics/GWAS/GWAS/plots/univariate GWAS-2.png"
     alt="Markdown Monster icon"
     style="float: left;"
     width = 45%/>

<img src="./Landscape Genomics/GWAS/GWAS/plots/univariate GWAS-3.png"
     alt="Markdown Monster icon"
     style="float: left;"
     width = 45%/>
<img src="./Landscape Genomics/GWAS/GWAS/plots/univariate GWAS-4.png"
     alt="Markdown Monster icon"
     style="float: left;"
     width = 45%/>

### Composite Statistic

'Landscape Genomics/GWAS/GWAS_PostProcess.R' = compiles GWAS information and performs composite statistic calculation

<img src="./Landscape Genomics/GWAS/GWAS/plots/multivariate GWAS-1.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 2px;" 
     height = "275"
     width = "275"/>

**Figure 9.** mahalonobis distance of SNPs associated with latitude, pit, temperature, and temperature 2 datasets.

# Common Garden Project

___


Trying to identify regions within the genome potentially responsible for the phenotypic differences between 2 populations.

<details>
<summary markdown="span"> Population Sample Size</summary>

*Table 7.** Population Sample Size

| Populations | Sample Size (n) |
|-------------|-----------------|
| CGK         | 23              |
| CGO3        | 9               |

</details>

    BWA=../bwa/bwa_sorted_bam_files/common_garden_sorted_bam_files
    POPMAP=../STACKS/preprocess_popmap/cg_popmap.txt
    OUT=./ref_map/common_garden
    module load stacks/2.53

    gstacks \
    -I $BWA \
    -M $POPMAP \
    -S '.sorted.bam' \
    -O $OUT \
    -t 40 \
    --min-mapq 20 \
    --var-alpha 0.05 \
    --details

    populations \
    -P $OUT \
    -O $OUT \
    -M $POPMAP \
    -t 40 \
    --min-populations 2 --min-samples-per-pop 7 \
    --ordered-export --vcf

---

# Directory Contents

**./raw_fastq:** raw ddRAD sequences and fastqc output

    /raw_fastq_files = output from illumina seq. (.fastq.gz)
    /fastqc_output =  output from FastQC
    /multiqc_data = output from MultiQC/1.1

**./ref_genome:** reference genome and index for bwa software for aligning short reads

    ref_genome = whole genome shotgun sequence Thymallus thymallus chromosome 10A
    bwa_index = intermediate files from bwa index files for aligning reads to the reference genome

**./kraken:** Evaluating for Potential Contamination
    
    kraken reports on Golden-Pool_002/012.out
    
**./err_out:** General Output from Previously Run Scripts

    encompasses .err and .out files from run programs

**./backup_files:** Zipped Fastq Files
    
    redownload_1_backup
    redownload_2_backup

**./bwa:** reads aligned via BWA mem

    /bwa_sorted_bam_files = sorted reads aligned to a reference genome
        /landscape_genomics_sorted_bam_files = aligned reads pertaining to the landscape genomics study
        /common_garden_sorted_bam_files = aligned reads pertaining to the common garden study
    
**./STACKS:** STACKS pipline
    
    /preprocess_popmap
        /cg_popmap = file specifying which common garden samples belong to which population
        /landscape_genomics_popmap = file specifying which landscape genomics samples belong to which population
    
    /process
        /p1_barcodes
        /fastqc_output
        /multiqc_output
        /fq_files
        /rem_seq
        /log_files
    
    /de_novo
        /common_garden
        /landscape_genomics
    
    /ref_map
        /common_garden
        /landscape_genomics 

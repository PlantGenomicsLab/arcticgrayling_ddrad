---
title: "PreProcess"
author: "Gabriel Barrett"
date: "10/19/2020"
output:
    keep_md: True
---

```{r}
library(dplyr)
library(tidyverse)
library(knitr)
library(ggplot2)
library(stringr)

knitr::opts_knit$set(root.dir = "D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape Genomics")
knitr::opts_chunk$set(
  fig.path = ""
  )

LG_meta <- read.csv("../GenomicsBioinformatics_EnvVersion_20210111.csv") %>% 
  filter(project == "LandscapeGenomics" | project == "SNP_Selection" | project == "CommonGarden")

mapping_stats <- read.csv("../BWA/merged_mapping_read_and_population_table.csv") %>% select(submission.id, aligned.reads)
ggplot() + geom_density(data = mapping_stats,aes(x=aligned.reads))
low_mapping <- mapping_stats %>% filter(aligned.reads <= 0.75) %>% left_join(.,LG_meta, by = c("submission.id" = "bioinformatics.id"))

population_order_list <- c("IMO", "IMR1", "IMR2", "BJANE", "LIMS", "IMR3", "IM3", "LL3", "Itk3", "Itk4", "Itk4.5", "GCL", "CGK", "Kup2", "KUS", "LTER", "KupR2", "Kup6", "KupR4", "Kup7", "KLAS5", "Kup", "Kup8", "LKup", "LKup0", "LKup1", "LKup3", "OksLCS", "UZ", "OKm1", "OksR1", "Oks0","OksLTER", "Oks2", "OksRN4", "Oks3", "OKS3", "OksSag", "LSag", "CGO3", "HV", "LSag2", "TC")

STACKS_POPMAP <- LG_meta %>% filter(!(bioinformatics.id %in% low_mapping$submission.id)) %>% 
  select(bioinformatics.id, location) %>% 
  mutate(location=recode(location, "Toolik-TOAS" = "Kup6", "Toolik-S3" = "LTER", "OksBH" = "Oks2")) %>%
  arrange(factor(location, levels = population_order_list)) %>%
  write.table("Ref_map.pl/LG_popmap.txt", sep = "\t", col.names = F, row.names = F, quote = F)
```

```{r}
# Restrict to LG study
LG_meta <- meta %>%
  select(submission.id,Aligned_Reads=Aligned.Reads....,location,region,project)%>%
  filter(project == "LandscapeGenomics" | project == "SNP_Selection" | project == "CommonGarden")

# Report on Individuals with low mapping rate
# Generate table in markdown format
# Reflects samples in original VCF
LG_low_map <- LG_meta %>%
  filter(Aligned_Reads <= .75)
knitr::kable(LG_low_map, row.names = FALSE, label = "Low Mapping Individuals")

# Create DataFrame with high mapping rate Individuals
LG_samples <- LG_meta %>%
  filter(Aligned_Reads >= .75)

# Create DataFrame with non-problematic individuals
# Reflects samples within filtered VCF
final <- LG_meta %>%
  filter(Aligned_Reads>=.75) %>%
  filter(!(submission.id %in% bad_samples$ind))
write.table(final[,c(1,3)],"LG_FF_popmap.txt", col.names = FALSE, row.names = FALSE, quote = FALSE)
```

```{r}
# Create file with individuals that will be copied into LG directory
ind <- LG_meta %>%
  select(submission.id)
write.table(ind, "LG_ind.txt", col.names = FALSE, row.names = FALSE, quote = FALSE)
```

```{r}
# Create tab seperated file with two columns 
# First being individual and second population
pop_map <- LG_meta %>%
  filter(Aligned_Reads >= .75) %>%
  select(submission.id,location)
write.table(pop_map, "LG_popmap.txt", col.names = FALSE, row.names = FALSE, quote = FALSE, sep = '\t')


```

```{r}
LG_VCF_META <- 

pop_map <- LG_meta %>%
  filter(Aligned_Reads >= .75) %>%
  select(submission.id,location)
 
region_map <- LG_meta %>%
  filter(Aligned_Reads >= .75)%>%
  filter(!(submission.id %in% bad_samples$ind))%>%
  select(submission.id,region)%>%
  mutate(region = ifelse(as.character(region) == "HV", "Oks", as.character(region)))%>%
  mutate(region = ifelse(as.character(region) == "Sag", "Oks", as.character(region)))%>%
  mutate(region = ifelse(as.character(region) == "TC", "Oks", as.character(region)))

 write.table(LG_VCF_META, "VCF_FF.txt", col.names = FALSE, row.names = FALSE, quote = FALSE, sep = "\t")
 write.table(regon_map, "LG_regmap.txt", col.names = FALSE, row.names = FALSE, quote = FALSE, sep = "\t")
```

```{r}
# Sample size for each region
sum <- final%>%
  group_by(location)%>%
  summarise(avg = n())

loc<-sum %>%
  filter(avg <= 3)

meta%>%
  group_by(location)%>%
  summarise(n())

# Create Dataframe with non-problematic individuals
final <- meta %>%
  filter(Aligned_Reads>=.75) %>%
  filter(!(submission.id %in% bad_samples$ind)) 

# Filter data frame based on region and select column containing individual names
vcf_Itk<-final %>%
  filter(region == "Itk")%>%
  select(submission.id)

vcf_Kup<-final %>%
  filter(region == "Kup")%>%
  select(submission.id)
  
vcf_Oks<-final %>%
  filter(region == "HV" | region == "Oks" | region == "Sag" | region == "TC")%>%
  select(submission.id)

# one sample per line
# bcftools view -Ou -S sample1 file.vcf
vcf <- c(vcf_Itk, vcf_Kup, vcf_Oks)
lapply(vcf, function(x, vcf) write.table(x, paste(x, ".txt", sep=""), col.names = FALSE, row.names = FALSE, quote = FALSE), vcf)
  
```

# Consolidate locations into populations


```{r}

focus <- left_join(samples, meta, by = "bioinformatics.id")

write.table(focus, "VCF_META.csv", sep = ",")

View(focus %>% select(bioinformatics.id, year,location, region, H3_WShd_up.down.lower, latitude, longitude) %>% 
  arrange(factor(H3_WShd_up.down.lower, levels = c("up", "down", "lower"))) %>% arrange(factor(region, levels = c("Colville", "Kuparuk", "Sagavanirktok"))))

```







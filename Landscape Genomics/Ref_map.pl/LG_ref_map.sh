
cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/STACKS/


BWA_FILES="../bwa/bwa_sorted_bam_files/landscape_genomics_sorted_bam_files"
POPMAP="../preprocess_popmap/landscape_genomics_popmap.txt"
STACKS_FOLDER="./ref_map/landscape_genomics_refmap"

module load stacks/2.53

gstacks \
-I "$BWA_FILES" \
-M "$POPMAP" \
-S '.sorted.bam' \
-O "$STACKS_FOLDER" \
-t 40 \
--min-mapq 25 \
--model marukilow \
--max-clipped 0.1

populations \
-P "$STACKS_FOLDER" \
-O "$STACKS_FOLDER" \
-M "$POPMAP" \
-t 40 \
--min-samples-per-pop .6 --min-mac 3 \
--ordered-export --fasta-loci --vcf
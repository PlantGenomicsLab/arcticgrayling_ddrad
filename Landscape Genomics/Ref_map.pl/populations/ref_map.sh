cd /labs/Wegrzyn/Urban_RAD_ArcticGrayling/STACKS
mkdir ./ref_map/landscape_genomics_refmap

module load stacks/2.53

gstacks \
-I ../bwa/bwa_sorted_bam_files/landscape_genomics_sorted_bam_files \
-M ../preprocess_popmap/landscape_genomics_popmap.txt \
-S '.sorted.bam' \
-O ./ref_map/landscape_genomics_refmap \
-t 40 \
--min-mapq 25 \
--model marukilow \
--rm-pcr-duplicates

populations \
-P ./ref_map/landscape_genomics_refmap \
-O ./ref_map/landscape_genomics_refmap \
-M ../preprocess_popmap/landscape_genomics_popmap.txt \
-t 40 \
--ordered-export \
--min-samples-per-pop .65 \
--min-mac 3 \
--smooth \
--vcf

Landscape Genomics PCA Analysis
================
Gabriel Barrett
9/17/2020

## PCA Analysis From Plink

``` r
#load eigenvectors/values 
pca <- read.delim("FILES/fb.F.maf.01.minQ20.minDP3.maxDP67.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.lat.pruned.eigenvec", header = FALSE, sep = " ")
eigenval <- scan("FILES/fb.F.maf.01.minQ20.minDP3.maxDP67.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.lat.pruned.eigenval")



meta <- read.delim("../../GenomicsBioinformatics_EnvVersion_20210111.csv", sep=",")
meta <-meta %>%
  dplyr::select(ind=bioinformatics.id,pop=location,reg=region,H1=H1_coast.noncoast,H2=H2_Aufeis_above.below.coast,udl=H3_WShd_up.down.lower)
#remove the first column since it's repeated twice
pca <- pca[,-1]
#name the first column indv
names(pca)[1] <- "ind"
#name 2nd to number of columns in pca as PC 1 to 20
names(pca)[2:ncol(pca)] <- paste0("PC",1:(ncol(pca)-1))
#add population data to the pca dataframe by individual
joined_pca <- left_join(pca, meta, by = "ind") #%>% left_join(.,big_data,by="ind")
df <- joined_pca %>% select(ind,pop,reg)
df_popsum <- df %>% group_by(pop) %>% summarise(n=n()) %>% arrange(factor(pop, levels = c("IMO", "IMR1", "IMR2", "BJANE", "LIMS", "IMR3", "IM3", "LL3", "Itk3", "Itk4", "Itk4.5", "GCL", "CGK", "Kup2", "KUS", "Toolik-S3", "LTER", "KupR2", "Toolik-TOAS", "Kup6", "KupR4", "Kup7", "KLAS5", "Kup", "Kup8", "LKup", "LKup0", "LKup1", "LKup3", "OksLCS", "UZ", "OKm1", "OksR1", "Oks0","OksLTER", "OksBH", "Oks2", "OksRN4", "Oks3", "OKS3", "OksSag", "CGO3", "LSag", "HV", "LSag2", "TC")))
knitr::kable(
  df_popsum,caption = "Population Sample Sizes",
  booktabs = TRUE
)
```

| pop         |  n |
| :---------- | -: |
| IMO         | 21 |
| IMR1        |  3 |
| IMR2        |  3 |
| BJANE       |  4 |
| LIMS        | 27 |
| IMR3        |  5 |
| IM3         | 13 |
| LL3         |  7 |
| Itk3        |  8 |
| Itk4        |  8 |
| Itk4.5      |  1 |
| GCL         | 20 |
| CGK         | 24 |
| Kup2        | 18 |
| KUS         | 17 |
| Toolik-S3   |  1 |
| LTER        |  5 |
| KupR2       |  4 |
| Toolik-TOAS |  1 |
| Kup6        | 20 |
| KupR4       |  3 |
| Kup7        | 17 |
| KLAS5       |  5 |
| Kup         |  3 |
| Kup8        | 20 |
| LKup        | 11 |
| LKup0       | 13 |
| LKup1       | 13 |
| LKup3       |  6 |
| OksLCS      | 12 |
| UZ          | 12 |
| OKm1        |  5 |
| OksR1       |  8 |
| Oks0        | 20 |
| OksLTER     |  3 |
| OksBH       |  1 |
| Oks2        | 20 |
| OksRN4      |  5 |
| Oks3        | 20 |
| OKS3        | 20 |
| OksSag      |  2 |
| CGO3        |  9 |
| LSag        |  9 |
| HV          | 12 |
| LSag2       |  3 |
| TC          | 20 |

Population Sample Sizes

``` r
pve <- data.frame(PC = 1:20, pve = eigenval/sum(eigenval)*100)
a <- ggplot(pve, aes(PC, pve)) + geom_bar(stat = "identity")
a + ylab("Percentage variance explained") + theme_classic()
```

![](Variance%20Explained-1.png)<!-- -->

``` r
library(ggsci)
library(viridis)
```

    ## Warning: package 'viridis' was built under R version 4.0.5

    ## Loading required package: viridisLite

    ## Warning: package 'viridisLite' was built under R version 4.0.5

``` r
#b1 <- 
# ITK = PC3 + PC18
# key = Region + Locality
colors2 <- list("tan1", "cornsilk", "chartreuse4", "burlywood4", "azure4", "deepskyblue4",
                "darkseagreen3","cadetblue3","yellow", "darkkhaki", "forestgreen", "goldenrod1","firebrick1", 
                "indianred1", "coral1", "mediumseagreen","tan2")

ggplot(joined_pca, aes(PC1, PC2, col = udl, shape=reg)) + geom_point(size = 3, alpha = .875) + 
  coord_equal() + 
  xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) + ylab(paste0("PC2 (", signif(pve$pve[2], 3), "%)")) + 
  labs(colour = "") + #stat_ellipse(aes(group = reg)) +
  annotate("text",x = -0.03,y = -0.017, label = "Sag", size = 5.5) +
  annotate("text",x = -0.0060,y = 0.0023, label = "Kup", size = 5.5) +
  annotate("text",x = 0.065,y = -0.02, label = "Itk", size = 5.5) + 
  annotate("text", x = -0.05, y = .0045, label = paste0("PC1 (", signif(pve$pve[1], 3), "%)"),size=4.95) +
  annotate("text", x = -.0045, y = -.045, label = paste0("PC2 (", signif(pve$pve[2], 3), "%)"), angle = 90,size=4.95) +
  #scale_color_igv(breaks = c("up","down","lower")) + 
  geom_hline(yintercept=0, linetype="longdash") + geom_vline(xintercept=0, linetype="dotdash") +
  theme(
    panel.grid = element_blank(),
    axis.title = element_blank(),
    axis.text = element_blank(),
    axis.ticks = element_blank(),
    panel.background = element_blank(),
    legend.key = element_rect(fill = "white", colour = "black"),
    legend.position = c(.7,.9)
  ) + 
  guides(fill = guide_legend(override.aes = list(linetype = 0)),
         color = guide_legend(override.aes = list(linetype = 0))) + 
  scale_color_igv(breaks = c("up","down","lower"))
```

![](PCA-1-1.png)<!-- -->

``` r
  scale_colour_manual(values=c("red","blue","green"))
```

    ## <ggproto object: Class ScaleDiscrete, Scale, gg>
    ##     aesthetics: colour
    ##     axis_order: function
    ##     break_info: function
    ##     break_positions: function
    ##     breaks: waiver
    ##     call: call
    ##     clone: function
    ##     dimension: function
    ##     drop: TRUE
    ##     expand: waiver
    ##     get_breaks: function
    ##     get_breaks_minor: function
    ##     get_labels: function
    ##     get_limits: function
    ##     guide: legend
    ##     is_discrete: function
    ##     is_empty: function
    ##     labels: waiver
    ##     limits: NULL
    ##     make_sec_title: function
    ##     make_title: function
    ##     map: function
    ##     map_df: function
    ##     n.breaks.cache: NULL
    ##     na.translate: TRUE
    ##     na.value: grey50
    ##     name: waiver
    ##     palette: function
    ##     palette.cache: NULL
    ##     position: left
    ##     range: <ggproto object: Class RangeDiscrete, Range, gg>
    ##         range: NULL
    ##         reset: function
    ##         train: function
    ##         super:  <ggproto object: Class RangeDiscrete, Range, gg>
    ##     rescale: function
    ##     reset: function
    ##     scale_name: manual
    ##     train: function
    ##     train_df: function
    ##     transform: function
    ##     transform_df: function
    ##     super:  <ggproto object: Class ScaleDiscrete, Scale, gg>

``` r
#ggsave("LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.PCA.png",last_plot(),device="png")
```

``` r
ggplot(joined_pca, aes(PC1, PC3, col = udl)) + geom_point(size = 3, alpha = .5) + 
  coord_equal() + theme_classic() + 
  xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) + ylab(paste0("PC3 (", signif(pve$pve[3], 3), "%)")) + 
  labs(colour = "") + stat_ellipse(aes(group = reg)) +
  annotate("text",x = -0.030,y = -0.02, label = "Sag") +
  annotate("text",x = -0.0030,y = 0.005, label = "Kup") +
  annotate("text",x = 0.055,y = -0.02, label = "Itk") + 
  scale_color_igv(breaks = c("up","down","lower")) + theme(legend.position = "top") + 
  geom_hline(yintercept=0, linetype="dotted") + geom_vline(xintercept=0, linetype="dotted")
```

![](PCA-2-1.png)<!-- -->

#!/bin/bash
module load bcftools
module load plink/1.90.beta.4.4
# Correct VCF & Group_List(BayPass) for GWAS
# Removes problematic individuals from group_list and recodes into ped format for LEA

DIR=/labs/Urban/Urban_RAD_ArcticGrayling

###### Input Files ##########
# Genotypic Datasets
FBVCF=${DIR}/freebayes/results/fb.F.mac3.minQ20.minDP3.meanDP3.miss65.ab.bs.mq.ps.pp.snps.ri.vcf
BED=${DIR}/freebayes/results/fb.F.mac3.minQ20.minDP3.meanDP3.miss65.ab.bs.mq.ps.pp.snps.ri
PRUNED_BED=${DIR}/freebayes/results/fb.F.mac3.minQ20.minDP3.meanDP3.miss65.ab.bs.mq.ps.pp.snps.ri.pruned

# Environmental Datasets
TEMP=${DIR}/meta/PED_TEMP_UPDATE_IDS.txt
PIT=${DIR}/meta/PED_PIT_UPDATE_IDS.txt

# Group File
FULL_G=${DIR}/meta/BayPass_FullGroup_list.txt


###### BayPass ###################

# LAT
bcftools query -l $FBVCF > samples.txt
awk 'NR==FNR{a[$1];next} $1 in a' samples.txt $FULL_G > $DIR/BayPass/lat_Group_list.txt

# PIT
cut -d ' ' -f 1 $PIT > pit_indv.txt
PIT_VCF=$(echo $FBVCF | sed 's/vcf/pit.vcf/')
bcftools view --force-samples --no-update --samples-file pit_indv.txt $FBVCF > $PIT_VCF
bcftools query -l $PIT_VCF > pit_samples.txt
awk 'NR==FNR{a[$1];next} $1 in a' pit_samples.txt $FULL_G > ${DIR}/BayPass/pit_Group_list.txt
rm pit*

# TEMP
cut -d ' ' -f 1 $TEMP > temp_indv.txt
TEMP_VCF=$(echo $FBVCF | sed 's/vcf/temp.vcf/')
bcftools view --force-samples --no-update --samples-file temp_indv.txt $FBVCF > $TEMP_VCF
bcftools query -l $TEMP_VCF > temp_samples.txt
awk 'NR==FNR{a[$1];next} $1 in a' temp_samples.txt $FULL_G > ${DIR}/BayPass/temp_Group_list.txt
rm temp*



###### LEA ######################

plink --bfile $LAT_BED --recode --out $LAT_BED --allow-extra-chr

PIT_PRUNED_PED=$(echo $PRUNED_BED | sed 's/pruned/pruned.pit/')
PIT_PED=${BED}.pit
plink --bfile $PRUNED_BED --keep $PIT --recode --out $PIT_PRUNED_PED --allow-extra-chr
plink --bfile $BED --keep $PIT --recode --out $PIT_PED --allow-extra-chr


TEMP_PRUNED_PED=$(echo $PRUNED_BED | sed 's/pruned/pruned.temp/')
TEMP_PED=${BED}.temp
plink --bfile $PRUNED_BED --keep $TEMP --recode --out $TEMP_PRUNED_PED --allow-extra-chr
plink --bfile $BED --keep $TEMP --recode --out $TEMP_PED --allow-extra-chr

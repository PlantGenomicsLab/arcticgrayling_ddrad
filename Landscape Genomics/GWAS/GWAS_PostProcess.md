GWAS\_Analysis
================
Gabriel Barrett
6/9/2021

``` r
header_key <- read.delim("../BWA/headers.txt",sep="",header=FALSE, col.names = c("chrom","chrom_id")) %>% mutate(chrom = gsub("[^0-9]+","", chrom))
BayPass <- read.table("BayPass/output/baypass_results.txt",sep="\t",h=T)
LEA <- read.table("LEA/output/lea_zscores.txt",h=T,sep="")
entap <- read.table("annotations/EnTAP/final_annotations_lvl1-2.tsv", h=T, fill=T,sep="\t") %>% select(Query.Sequence,EggNOG.Predicted.Gene,EggNOG.Description,EggNOG.GO.Biological)
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec = dec, :
    ## EOF within quoted string

``` r
vcfR_obj <- read.vcfR("annotations/snpEff/fb.F.maf.01.minQ20.minDP3.maxDP127.miss65.ab.bs.mq.ps.pp.snps.ri.hwe.beagle.ann.vcf")
```

    ## Scanning file to determine attributes.
    ## File attributes:
    ##   meta lines: 13
    ##   header_line: 14
    ##   variant count: 3363
    ##   column count: 486
    ## Meta line 13 read in.
    ## All meta lines processed.
    ## gt matrix initialized.
    ## Character matrix gt created.
    ##   Character matrix gt rows: 3363
    ##   Character matrix gt cols: 486
    ##   skip: 0
    ##   nrows: 3363
    ##   row_num: 0
    ## Processed variant 1000Processed variant 2000Processed variant 3000Processed variant: 3363
    ## All variants processed

``` r
vcfR_df <- vcfR2tidy(vcfR_obj)
```

    ## Extracting gt element GT

    ## Extracting gt element DS

``` r
vcf_df_fix <- vcfR_df$fix %>% 
  select(chrom_id=CHROM,pos=POS,ANN) %>% 
  separate(col = ANN,into = c("allele","annotation","annotation_impact","gene_name",
                                   "gene_id","feature_type","feature_id","transcript_biotype",
                                   "rank","HGVS.c","HGVS.p","cDNA.pos/cDNA.length","cDNS.pos/cDNS.length",
                                   "AA.pos/AA.length","distance","err"), sep = "\\|",convert=T,remove=F,fill="right") %>%
  select(!(ANN)) %>% 
  left_join(.,entap, by = c("feature_id"="Query.Sequence")) %>%
  left_join(.,header_key,by="chrom_id") %>%
  mutate_all(na_if,"") %>% select(-feature_id) %>%  cbind(.,BayPass,LEA)
```

    ## Warning: Expected 16 pieces. Additional pieces discarded in 1233 rows [1, 2, 3,
    ## 4, 13, 16, 19, 21, 29, 32, 33, 36, 37, 38, 40, 41, 42, 43, 50, 52, ...].

``` r
View(vcf_df_fix)
```

``` r
for (i in c("lat","pit","temp","temp2")) {
  layout(matrix(1:4,2,2))
  for (t in c("XtX","GD","BF","EA")) {
    # Build column name through paste function
    column <- paste0(i,"_",t)
    # draw histogram of that column
    hist(vcf_df_fix[[column]],main = paste0(i,"_",t))
    
    # p-values from LEA z-scores for GD and EA
    if (t == "GD"){
      if (i == "lat" || i == "temp" || i == "temp2"){
        
        assign(paste0(i,"_GD_lambda"), median(vcf_df_fix[[column]]^2)/qchisq(1/2, df = 5 - 1))
        GD_lambda <- get(paste0(i,"_GD_lambda"))
        assign(paste0(column,"_pval"),pchisq(vcf_df_fix[[column]]^2/GD_lambda, df = 5 - 1, lower = FALSE))
      
      }
      else if (i == "pit"){
        # Changed lambda to 1.38 from 3.3 
        assign(paste0(i,"_GD_lambda"), median(vcf_df_fix[[column]]^2)/qchisq(1/2, df = 3 - 1))
        GD_lambda <- get(paste0(i,"_GD_lambda"))
        assign(paste0(column,"_pval"),pchisq(vcf_df_fix[[column]]^2/1.38, df = 3 - 1, lower = FALSE))
        
      }
    }
    else if (t == "EA"){
        
      EA_lambda = median(vcf_df_fix[[column]]^2)/qchisq(1/2, df = 1)
      assign(paste0(column,"_pval"),pchisq(vcf_df_fix[[column]]^2/EA_lambda, df = 1, lower = FALSE))
        
      }
    
    # Multiple correction w/ qvalue fdr = 10%
    if (t == "GD" || t == "EA"){
      
      GD_pval <- get(paste0(column,"_pval"))
      assign(paste0(column, "_qval"), qvalue(GD_pval,fdr.level=.1)$qvalues)
    
      }

      
      # 1% quantile from BayPass BF & spearman || psuedo threshold for XtX  
    if (t == "XtX" || t == "BF" || t == "Pearson") {
    
    assign(paste0(i,"_",t,"_99"), quantile(abs(vcf_df_fix[[column]]), 0.95))
      
    sig <- get(paste0(i,"_",t,"_99"))
    
    assign(paste0(i,"_sig_",t), vcf_df_fix %>% filter(abs(vcf_df_fix[[column]]) > sig))
    
    sig_uni <- get(paste0(i,"_sig_",t))
    
    sig_uni %>%
      group_by(gene_name = sig_uni$gene_name) %>%
      summarise(gene_count=n()) %>% 
      ungroup() %>%
      # join annotation information
      left_join(.,sig_uni, by = "gene_name") %>%
      select(chrom, pos, annotation, gene_count,
            annotation_impact,HGVS.p,
            EggNOG.Predicted.Gene,EggNOG.Description,EggNOG.GO.Biological,column) %>% 
      
      # ARRANGE NOT WORKING 
      #arrange( desc(sig_multi[[t]]) ) %>%
      write.table(x=.,file=paste0("./BayPass/",i,"/",i,"_sig_",t,".txt"),quote = F,sep = "\t",row.names = F,col.names = T)  
    }
  }
}
```

    ## Note: Using an external vector in selections is ambiguous.
    ## i Use `all_of(column)` instead of `column` to silence this message.
    ## i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
    ## This message is displayed once per session.

![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/univariate%20GWAS-1.png)<!-- -->![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/univariate%20GWAS-2.png)<!-- -->![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/univariate%20GWAS-3.png)<!-- -->![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/univariate%20GWAS-4.png)<!-- -->

``` r
# BayPass core model top 5% sites within Pearson & Bayes Factor
for (i in c("lat","pit","temp","temp2")) {
  for (d in c("BF", "Pearson")){
      if (d == "BF"){
        
        bf_column <- paste0(i,"_",d)
        assign(paste0(i,"_",d,"_95"), quantile(abs(vcf_df_fix[[bf_column]]), 0.95))
      
        bf_sig <- get(paste0(i,"_",d,"_95"))
    
        #assign(paste0(i,"_sig_",t), vcf_df_fix %>% filter(abs(vcf_df_fix[[bf_column]]) > sig))
    
        #sig_uni <- get(paste0(i,"_sig_",t))
    }
      else if (d == "Pearson"){
        
        spear_column <- paste0(i,"_",d)
        assign(paste0(i,"_",d,"_95"), quantile(abs(vcf_df_fix[[spear_column]]), 0.95))
      
        spear_sig <- get(paste0(i,"_",d,"_95"))
    
        assign(paste0(i,"_sig_",t), vcf_df_fix %>% filter(abs(vcf_df_fix[[column]]) > sig))
    
        spear_uni <- get(paste0(i,"_sig_",t))
        }
  }
  assign(paste0(i,"_sig_Baycore"), vcf_df_fix %>% filter(abs(vcf_df_fix[[bf_column]]) >= bf_sig & abs(vcf_df_fix[[spear_column]]) >= spear_sig))
  sig_baycore <- get(paste0(i,"_sig_Baycore"))
  
      sig_baycore %>%
      group_by(gene_name = sig_baycore$gene_name) %>%
      summarise(gene_count=n()) %>% 
      ungroup() %>%
      # join annotation information
      left_join(.,sig_baycore, by = "gene_name") %>%
      select(chrom, pos, annotation, gene_count,
            annotation_impact,HGVS.p,
            EggNOG.Predicted.Gene,EggNOG.Description,EggNOG.GO.Biological,bf_column,spear_column) %>% 
      
      # ARRANGE NOT WORKING 
      #arrange( desc(sig_multi[[t]]) ) %>%
      write.table(x=.,file=paste0("./BayPass/",i,"/",i,"_sig_baycore",".txt"),quote = F,sep = "\t",row.names = F,col.names = T)
  
}  
```

    ## Note: Using an external vector in selections is ambiguous.
    ## i Use `all_of(bf_column)` instead of `bf_column` to silence this message.
    ## i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
    ## This message is displayed once per session.

    ## Note: Using an external vector in selections is ambiguous.
    ## i Use `all_of(spear_column)` instead of `spear_column` to silence this message.
    ## i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
    ## This message is displayed once per session.

``` r
lea_qval <- cbind(vcf_df_fix,lat_EA_qval=lat_EA_qval,lat_GD_qval=lat_GD_qval,
                  pit_EA_qval=pit_EA_qval,pit_GD_qval=pit_GD_qval,
                  temp_EA_qval=temp_EA_qval,temp_GD_qval=temp_GD_qval,
                  temp2_EA_qval=temp2_EA_qval,temp2_GD_qval=temp2_GD_qval)


# LEA sig dataframe
for (i in c("lat","pit","temp","temp2")) {
  for (d in c("EA", "GD")){
    
    col <- paste0(i,"_",d,"_","qval")
    
    assign(paste0(i,"_sig_",d), lea_qval %>% filter(lea_qval[[col]] < .05))
    
    sig_qval <- get(paste0(i,"_sig_",d))
    
    sig_qval %>%
    group_by(gene_name = sig_qval$gene_name) %>%
      summarise(gene_count=n()) %>% 
      ungroup() %>%
      # join annotation information
      left_join(.,sig_qval, by = "gene_name") %>%
      select(chrom, pos, annotation, gene_count,
             annotation_impact,HGVS.p,
             EggNOG.Predicted.Gene,EggNOG.Description,EggNOG.GO.Biological,col) %>% 
      
      # ARRANGE NOT WORKING 
      #arrange( desc(sig_multi[[t]]) ) %>%
      write.table(x=.,file=paste0("./LEA/",i,"/",i,"_sig_",d,".txt"),quote = F,sep = "\t",row.names = F,col.names = T)  
    
  }
  
}
```

    ## Note: Using an external vector in selections is ambiguous.
    ## i Use `all_of(col)` instead of `col` to silence this message.
    ## i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
    ## This message is displayed once per session.

``` r
# of shared variants between XtX vs Fst && BF vs lfmm
```

``` r
lat <- vcf_df_fix %>% select(!(starts_with(c("pit","temp","temp2","n","lat_Pearson"))))
pit <- vcf_df_fix %>% select(!(starts_with(c("lat","temp","temp2","n","pit_Pearson"))))
temp <- vcf_df_fix %>% select(!(starts_with(c("pit","lat","temp2","n","temp_Pearson"))))
temp2 <- vcf_df_fix %>% select(!(starts_with(c("pit","temp_","lat","n","temp2_Pearson"))))

for (i in c("lat","pit","temp","temp2")){
  
  dataset <- get(paste0(i))
  # Calculate multi -> BF | XtX | EA | GD
  assign(paste0(i,"_harmony"), MINOTAUR::harmonicDist(dataset[,22:25]))
  assign(paste0(i,"_kerneldev"), MINOTAUR::kernelDist(dataset[,22:25]))
  assign(paste0(i,"_kernaldis"), MINOTAUR::neighborDist(dataset[,22:25]))
  assign(paste0(i,"_maha"), MINOTAUR::Mahalanobis(dataset[,22:25]))
  
  #}

#for (i in c("lat","pit","temp","temp2")) {
  layout(matrix(1:4,4,1))
  for (t in c("_maha","_harmony","_kerneldev","_kernaldis")) {
    y_min <- min(get(paste0(i,t)))
    y_max <- max(get(paste0(i,t)))
    if (t == "_maha"){
      plot(get(paste0(i,t)),ylim=c(y_min,y_max), ylab = "mahalanobis distance", xlab = "", main = i)
    }
    else if (t == "_harmony"){
      plot(get(paste0(i,t)),ylim=c(y_min,y_max), ylab = "harmonic distance", xlab = "")
    }
    else if (t == "_kerneldev"){
      plot(get(paste0(i,t)),ylim=c(y_min,y_max), ylab = "Kernel Density Deviance", xlab = "")
    }
    else if (t == "_kernaldis"){
      plot(get(paste0(i,t)),ylim=c(y_min,y_max), ylab = "Nearest Neighbor Distance", xlab = "SNPs")
    } 
  }  
}
```

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/multivariate%20GWAS-1.png)<!-- -->

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used
    
    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/multivariate%20GWAS-2.png)<!-- -->

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used
    
    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

    ## Warning in if (class(try(df.vars[subset, ], silent = TRUE)) == "try-error")
    ## stop("subset must contain valid indexes for choosing rows in dfv"): the
    ## condition has length > 1 and only the first element will be used

    ## Warning in if (class(try(solve(S), silent = TRUE)) == "try-error")
    ## stop("covariance matrix S is exactly singular"): the condition has length > 1
    ## and only the first element will be used

![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/multivariate%20GWAS-3.png)<!-- -->![](D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape%20Genomics/GWAS/plots/multivariate%20GWAS-4.png)<!-- -->

``` r
##################
# Create Dataframe of Significanct SNP's with annotation metadata
##################

for (i in c("lat","pit","temp","temp2")) {
  
  df <- get(paste0(i))
  
  for (t in c("maha","harmony","kerneldev","kernaldis")) {
    
    p <- data.frame(get(paste0(i,"_",t)))
    
    df <- cbind(df,p)
    colnames(df)[ncol(df)] <- paste0(t)
    
    assign(paste0(i,"_multi_df"), df)
  
    multi <- get(paste0(i,"_multi_df"))
    
    # Calculate 95% threshold
    assign(paste0(i,"_",t,"_95"), quantile(multi[[t]], 0.95))
    
    sig <- get(paste0(i,"_",t,"_95"))
    #print(sig)
    
    assign(paste0(i,"_sig_",t), multi %>% filter(multi[[t]] > sig))
    
    sig_multi <- get(paste0(i,"_sig_",t))
    
    num_rows <- nrow(sig_multi)
    print(paste0(i, " ",t,": ",num_rows))
    
    obj <- paste0(t)
    
    sig_multi %>%
      # count number of SNPs within gene ID/name
      group_by(gene_name = sig_multi$gene_name) %>%
      summarise(gene_count=n()) %>% 
      ungroup() %>%
      # join annotation information
      left_join(.,sig_multi, by = "gene_name") %>%
      select(chrom, pos, annotation, gene_count,
             annotation_impact,HGVS.p,
             EggNOG.Predicted.Gene,EggNOG.Description,EggNOG.GO.Biological,t) %>% arrange_at(obj,desc) %>%

      write.table(x=.,file=paste0("./annotations/",i,"_sig_",t,".txt"),quote = F,sep = "\t",row.names = F,col.names = T)

      
    ###### Count number of occurencess #######----------------------------------------------------------------------------------
    # Annotation -> SNPs region
    assign(paste0(i,"_",t,"ann_sum"),sig_multi %>% group_by(annotation) %>% summarise(n()))
    # Impact on protein
    assign(paste0(i,"_",t,"impact_sum"),sig_multi %>% group_by(annotation_impact) %>% summarise(n()))
    # Biological process
    assign(paste0(i,"_",t,"sepGO"), sig_multi %>% separate(EggNOG.GO.Biological,"GO.Biological",extra="drop",sep = ",") %>% group_by(GO.Biological) %>% summarise(n()))
    # Gene names
    assign(paste0(i,"_",t,"gene"), sig_multi %>% select(EggNOG.Predicted.Gene) %>% filter(!(is.na(EggNOG.Predicted.Gene))) %>% count())
    # Gene descriptions
    assign(paste0(i,"_",t,"genedesc"), sig_multi %>% select(EggNOG.Description) %>% filter(!(is.na(EggNOG.Description))) %>% count())
    }
}
```

    ## [1] "lat maha: 169"

    ## Note: Using an external vector in selections is ambiguous.
    ## i Use `all_of(t)` instead of `t` to silence this message.
    ## i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
    ## This message is displayed once per session.

    ## [1] "lat harmony: 169"
    ## [1] "lat kerneldev: 169"
    ## [1] "lat kernaldis: 169"
    ## [1] "pit maha: 169"
    ## [1] "pit harmony: 169"
    ## [1] "pit kerneldev: 169"
    ## [1] "pit kernaldis: 169"
    ## [1] "temp maha: 169"
    ## [1] "temp harmony: 169"
    ## [1] "temp kerneldev: 169"
    ## [1] "temp kernaldis: 169"
    ## [1] "temp2 maha: 169"
    ## [1] "temp2 harmony: 169"
    ## [1] "temp2 kerneldev: 169"
    ## [1] "temp2 kernaldis: 168"

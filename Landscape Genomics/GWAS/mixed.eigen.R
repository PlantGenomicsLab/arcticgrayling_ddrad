mixed.eigen <- function(dataframe,kk.eigen,lambda=NULL){
###   
   d <- dataframe
   y <- as.matrix(d[,1])
   x <- as.matrix(d[,-1])   
   n <- nrow(d)
   r <- ncol(d)-1 
###
   uu <- kk.eigen$vectors
   delta <- kk.eigen$values
   xu <- crossprod(uu,x)
   yu <- crossprod(uu,y)
### 
###log likelihood function
   loglikfn <- function(theta, xu, yu){
      r <- ncol(xu)
      lambda <- exp(theta)
      H <- delta*lambda+1
      H_sqrt <- sqrt(H)
      d0 <- sum(log(abs(H)))
      ##
      xh <- sweep(xu, 1, H_sqrt, "/")
      xx <- crossprod(xh)       ##xx<-t(xu) %*% solve(D) * xu
      d1 <- unlist(determinant(xx))[1]      
      ##
      yh <- yu/H_sqrt
      yy <- sum(yh^2)           ##yy<-t(yu) %*% solve(D) %*% yu
      xy <- crossprod(xh,yh)    ##yx<-t(yu) %*% solve(D) %*% xu
      p <- abs(yy - t(xy) %*% solve(xx,tol=1e-100) %*% xy)
      ##
      #cc_fn <- (n-r)*(1-log(n-r))
      rr <- -0.5*( d0 + d1 + (n-r)*log(p/(n-r))) #+ cc_fn)
      ##
      return(-rr)   
   }

   ##optimization for estimation parameters
   if (is.null(lambda)){
      theta.ls <- seq(-40, 40,by=0.05)    ###seq(-50, 50,by=0.01)
      nth <- length(theta.ls)
      fn.ls <- sapply(1:nth,function(i){
          th0 <- as.numeric(theta.ls[i])
          fn0 <- loglikfn(theta=th0,xu=xu,yu=yu)
          return(fn0)
      })
      imin <- which.min(fn.ls)
      theta0 <- theta.ls[imin]
      parms <- optim(par=theta0,fn=loglikfn,xu=xu,yu=yu,hessian=TRUE,method="L-BFGS-B",lower=-100,upper=100)
      lambda <- exp(parms$par)
      fn1 <- -parms$value
   }

   ##
   H <- delta*lambda + 1
   H_sqrt <- sqrt(H)
   yh <- yu/H_sqrt
   yy <- sum(yh^2)
   xh <- sweep(xu, 1, H_sqrt, "/")
   xx <- crossprod(xh)
   xy <- crossprod(xh,yh)   
   ##
   xx_inv <- solve(xx, tol=1e-100)
   p <- abs(yy - t(xy) %*% xx_inv %*% xy)
   se2 <- drop(p/(n-r))    ### 
   sg2 <- lambda*se2
   tau <- c(sg2, se2, lambda)
   ### fixed effects
   b <- xx_inv %*% xy
   vb <- xx_inv * se2
   ### random  effects
   H0 <- delta*lambda
   g <- uu %*% ( (H0/H) * (yu - xu%*%as.matrix(b)))
   
   ###
   ### return results
   parr <-list(b=b, vb=vb, g=g, tau=tau, fn=fn1, p_null=p)
   return(parr)
}
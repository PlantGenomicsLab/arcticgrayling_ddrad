library(dplyr)
library(ggplot2)
setwd("Landscape Genomics/")

header_key <- read.delim("../BWA/headers.txt",sep="",header=FALSE, col.names = c("chrom","chrom_id"))
ordered_header_key <- header_key %>% arrange(nchar(chrom))


fst_watershed <- read.delim("POP_STATS/vcftools_per_site_fst/Itk_Kup_Oks.weir.fst", sep="",header=TRUE, 
                            col.names = c("chrom_id", "pos", "fst"))

fst_updownlower <- read.delim("POP_STATS/vcftools_per_site_fst/lower_down_upper.weir.fst", sep="",header=TRUE, 
                              col.names = c("chrom_id", "pos", "fst"))


fst_WS <- left_join(fst_watershed, header_key , by = "chrom_id") %>%
  mutate(chrom = factor(chrom, levels=ordered_header_key$chrom))

ggplot(fst_WS, aes(y=fst,x=chrom, colour = fst>0.5)) + geom_jitter() +  
  theme_classic() +
  scale_colour_manual(name = 'Fst > 0.5', values = setNames(c('red','blue'),c(T, F))) +
  scale_shape_manual(values=16) +
  xlab("Chromosome") + ylab("Fst") +
  facet_grid(~chrom, scales = 'free_x', space = 'free_x', switch = 'x') + 
  theme(axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        panel.spacing = unit(0, "lines")) + 
  ggtitle("Fst between Watersheds")


fst_udl <- left_join(fst_updownlower, header_key , by = "chrom_id") %>%
  mutate(chrom = factor(chrom, levels=ordered_header_key$chrom))

ggplot(fst_udl, aes(y=fst,x=chrom, colour = fst>0.5)) + geom_jitter() +
  theme_classic() +
  scale_colour_manual(name = 'Fst > 0.5', values = setNames(c('red','blue'),c(T, F))) +
  scale_shape_manual(values=16) +
  xlab("Chromosome") + ylab("Fst") +
  facet_grid(~chrom, scales = 'free_x', space = 'free_x', switch = 'x') + 
  theme(axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        panel.spacing = unit(0, "lines")) + 
  ggtitle("Fst between up/down/lower")


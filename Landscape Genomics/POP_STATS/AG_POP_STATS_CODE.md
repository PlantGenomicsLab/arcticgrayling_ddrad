Arctic Grayling Population Statistics
================
Gabriel Barrett
10/3/2020

``` r
Phi<-read.delim("STACKS_POPSTATS_FILES/LG.snps1.FFP5.MAF.008.MISS.5.MD.69.p.sumstats_summary.tsv", sep = "\t",skip = 46,nrows = 92)
Phi_stats<-Phi%>%
  select(pop=Pop.ID,Private,Obs_Het,Var.2,Exp_Het,Var.4,Pi,Var.6,Fis,Var.7)

# Round all rows in columns 3 to 6 to 3 decimal places
Phi_stats[,3:8] <- lapply(Phi_stats[,3:8], round, 3)

# Combine Variation with respective column
Phi_stats$Obs_Het <- paste(Phi_stats$Obs_Het, "\u00B1", Phi_stats$Var.2)
Phi_stats$Exp_Het <- paste(Phi_stats$Exp_Het, "\u00B1", Phi_stats$Var.4)
Phi_stats$Pi <- paste(Phi_stats$Pi, "\u00B1", Phi_stats$Var.6)
Phi_stats$Fis <- paste(Phi_stats$Fis, "\u00B1", Phi_stats$Var.7)

Phi_dataframe <- Phi_stats%>%
  select(Location=pop,Private,Obs_Het,Exp_Het,Pi,Fis)

# Rename columns, asterisks convert to subscripts in markdown language
names(Phi_dataframe) <- c("Location", "Private", "H~o~", "H~e~", "pi", "F~is~")

knitr::kable(Phi_dataframe)
```

| Location | Private | H<sub>o</sub> | H<sub>e</sub> | pi            | F<sub>is</sub>      |
| :------- | ------: | :------------ | :------------ | :------------ | :------------------ |
| IMO      |       1 | 0.237 ± 0.06  | 0.208 ± 0.031 | 0.215 ± 0.033 | \-0.03983 ± 0.07427 |
| IMR1     |       0 | 0.224 ± 0.109 | 0.166 ± 0.042 | 0.223 ± 0.085 | \-0.0018 ± 0.06034  |
| IMR2     |       0 | 0.228 ± 0.105 | 0.169 ± 0.042 | 0.22 ± 0.078  | \-0.0126 ± 0.05971  |
| BJANE    |       0 | 0.227 ± 0.096 | 0.172 ± 0.039 | 0.216 ± 0.07  | \-0.01682 ± 0.05734 |
| LIMS     |       0 | 0.232 ± 0.056 | 0.209 ± 0.031 | 0.215 ± 0.032 | \-0.0259 ± 0.07575  |
| IMR3     |       0 | 0.233 ± 0.09  | 0.183 ± 0.038 | 0.216 ± 0.057 | \-0.03095 ± 0.06828 |
| IM3      |       0 | 0.232 ± 0.066 | 0.201 ± 0.033 | 0.215 ± 0.04  | \-0.03245 ± 0.07278 |
| LL3      |       0 | 0.229 ± 0.074 | 0.194 ± 0.035 | 0.215 ± 0.045 | \-0.02231 ± 0.0785  |
| Itk3     |       0 | 0.237 ± 0.073 | 0.201 ± 0.035 | 0.222 ± 0.045 | \-0.02596 ± 0.08022 |
| Itk4     |       0 | 0.231 ± 0.071 | 0.198 ± 0.035 | 0.218 ± 0.043 | \-0.02234 ± 0.07921 |
| Itk4.5   |       0 | 0.229 ± 0.177 | 0.115 ± 0.044 | 0.229 ± 0.177 | 0 ± 0               |
| GCL      |       0 | 0.192 ± 0.063 | 0.168 ± 0.034 | 0.174 ± 0.037 | \-0.0316 ± 0.06494  |
| CGK      |       0 | 0.196 ± 0.061 | 0.171 ± 0.034 | 0.177 ± 0.036 | \-0.03502 ± 0.0631  |
| Kup2     |       1 | 0.198 ± 0.08  | 0.151 ± 0.037 | 0.158 ± 0.04  | \-0.08853 ± 0.05721 |
| KUS      |       0 | 0.194 ± 0.065 | 0.165 ± 0.034 | 0.173 ± 0.037 | \-0.04049 ± 0.06131 |
| LTER     |       0 | 0.21 ± 0.085  | 0.162 ± 0.037 | 0.185 ± 0.05  | \-0.04593 ± 0.05686 |
| KupR2    |       0 | 0.193 ± 0.092 | 0.147 ± 0.038 | 0.177 ± 0.058 | \-0.02632 ± 0.0611  |
| Kup6     |       0 | 0.196 ± 0.063 | 0.168 ± 0.034 | 0.174 ± 0.036 | \-0.04335 ± 0.06117 |
| KupR4    |       0 | 0.201 ± 0.111 | 0.138 ± 0.04  | 0.186 ± 0.081 | \-0.02443 ± 0.04579 |
| Kup7     |       0 | 0.202 ± 0.068 | 0.17 ± 0.035  | 0.177 ± 0.038 | \-0.0501 ± 0.06288  |
| KLAS5    |       0 | 0.198 ± 0.085 | 0.156 ± 0.038 | 0.18 ± 0.052  | \-0.02965 ± 0.06653 |
| Kup      |       0 | 0.212 ± 0.112 | 0.143 ± 0.04  | 0.194 ± 0.082 | \-0.02943 ± 0.04144 |
| Kup8     |       0 | 0.209 ± 0.066 | 0.178 ± 0.034 | 0.185 ± 0.037 | \-0.04971 ± 0.06664 |
| LKup     |       0 | 0.205 ± 0.07  | 0.171 ± 0.035 | 0.185 ± 0.041 | \-0.03615 ± 0.06388 |
| LKup0    |       0 | 0.205 ± 0.069 | 0.172 ± 0.035 | 0.183 ± 0.039 | \-0.04371 ± 0.06306 |
| LKup1    |       0 | 0.203 ± 0.067 | 0.171 ± 0.034 | 0.182 ± 0.038 | \-0.04047 ± 0.06564 |
| LKup3    |       0 | 0.201 ± 0.084 | 0.159 ± 0.037 | 0.185 ± 0.053 | \-0.02711 ± 0.06329 |
| OksLCS   |       0 | 0.168 ± 0.07  | 0.14 ± 0.035  | 0.152 ± 0.044 | \-0.02951 ± 0.05866 |
| UZ       |       0 | 0.172 ± 0.069 | 0.141 ± 0.035 | 0.15 ± 0.039  | \-0.04077 ± 0.05651 |
| OKm1     |       0 | 0.165 ± 0.086 | 0.121 ± 0.035 | 0.144 ± 0.053 | \-0.03438 ± 0.0523  |
| OksR1    |       0 | 0.175 ± 0.079 | 0.135 ± 0.036 | 0.149 ± 0.045 | \-0.04801 ± 0.05755 |
| Oks0     |       0 | 0.175 ± 0.069 | 0.144 ± 0.035 | 0.15 ± 0.039  | \-0.04987 ± 0.05837 |
| OksLTER  |       0 | 0.17 ± 0.1    | 0.119 ± 0.037 | 0.158 ± 0.071 | \-0.01778 ± 0.04847 |
| Oks2     |       3 | 0.179 ± 0.073 | 0.145 ± 0.036 | 0.15 ± 0.038  | \-0.05521 ± 0.06554 |
| OksRN4   |       0 | 0.18 ± 0.083  | 0.139 ± 0.037 | 0.163 ± 0.052 | \-0.03 ± 0.0537     |
| Oks3     |       0 | 0.189 ± 0.068 | 0.158 ± 0.036 | 0.164 ± 0.038 | \-0.0489 ± 0.0605   |
| OKS3     |       0 | 0.185 ± 0.066 | 0.156 ± 0.035 | 0.163 ± 0.039 | \-0.04387 ± 0.05649 |
| OksSag   |       0 | 0.183 ± 0.113 | 0.119 ± 0.038 | 0.175 ± 0.09  | \-0.01162 ± 0.03274 |
| LSag     |       0 | 0.181 ± 0.073 | 0.149 ± 0.036 | 0.164 ± 0.044 | \-0.03022 ± 0.06153 |
| CGO3     |       0 | 0.183 ± 0.073 | 0.149 ± 0.036 | 0.163 ± 0.043 | \-0.03673 ± 0.05743 |
| HV       |       0 | 0.181 ± 0.076 | 0.143 ± 0.036 | 0.157 ± 0.044 | \-0.04527 ± 0.05601 |
| LSag2    |       0 | 0.182 ± 0.099 | 0.13 ± 0.038  | 0.17 ± 0.071  | \-0.01847 ± 0.04856 |
| TC       |       6 | 0.188 ± 0.071 | 0.152 ± 0.035 | 0.157 ± 0.038 | \-0.06494 ± 0.05654 |

``` r
meta <- read_csv("../../GenomicsBioinformatics_EnvVersion_20210111.csv") %>% 
  select(pop=location, reg=region, udl=H3_WShd_up.down.lower)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   .default = col_character(),
    ##   year = col_double(),
    ##   number = col_double(),
    ##   tag = col_double(),
    ##   latitude = col_double(),
    ##   longitude = col_double(),
    ##   Lngth.cm.x = col_double(),
    ##   Wt.g.x = col_double(),
    ##   Otoliths.x = col_double(),
    ##   max.dist = col_double(),
    ##   days.tracked = col_double(),
    ##   Freq = col_double(),
    ##   H3.JulyAug.Temp.Ave = col_double(),
    ##   H3.JulyAug.Temp.Med = col_double(),
    ##   H3.JulyAug.Temp.Min = col_double(),
    ##   H3.JulyAug.Temp.Max = col_double(),
    ##   H3.JulyAug.Temp.Var = col_double(),
    ##   H3.JulyAug.Temp.se = col_double(),
    ##   H3.JulyAug.Temp.n = col_double()
    ## )
    ## i Use `spec()` for the full column specifications.

``` r
full_phi_stats <- left_join(Phi_stats, meta, by = "pop") %>% unique()

# Col
col <- c("IMO", "IMR1", "IMR2", "BJANE", "LIMS", "IMR3", "IM3", "LL3", "Itk3", "Itk4", "Itk4.5")
col_phi_stats <- full_phi_stats %>% filter(pop %in% col)
col_phi_stats$pop <- factor(col_phi_stats$pop, levels = col)
# Kup
kup <- c("GCL", "CGK", "Kup2", "KUS", "Toolik-S3", "LTER", "KupR2", "Toolik-TOAS", "Kup6", "KupR4", "Kup7", "KLAS5", "Kup", "Kup8", "LKup", "LKup0", "LKup1", "LKup3")
kup_phi_stats <- full_phi_stats %>% filter(pop %in% kup)
col_phi_stats$pop <- factor(col_phi_stats$pop, levels = col)
# Sag
sag <- c("OksLCS", "UZ", "OKm1", "OksR1", "Oks0","OksLTER", "OksBH", "Oks2", "OksRN4", "Oks3", "OKS3", "OksSag", "CGO3", "LSag", "HV", "LSag2", "TC")
sag_phi_stats <- full_phi_stats %>% filter(pop %in% sag)
sag_phi_stats$pop <- factor(sag_phi_stats$pop, levels = sag)

# Nucleotide Diversity in order of up, down, lower
ggplot(data=col_phi_stats, aes(x=pop,y=Pi)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-1.png)<!-- -->

``` r
ggplot(data=kup_phi_stats, aes(x=pop,y=Pi)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-2.png)<!-- -->

``` r
ggplot(data=sag_phi_stats, aes(x=pop,y=Pi)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-3.png)<!-- -->

``` r
# Observed Heterozygosity
ggplot(data=col_phi_stats, aes(x=pop,y=Obs_Het)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-4.png)<!-- -->

``` r
ggplot(data=kup_phi_stats, aes(x=pop,y=Obs_Het)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-5.png)<!-- -->

``` r
ggplot(data=sag_phi_stats, aes(x=pop,y=Obs_Het)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-6.png)<!-- -->

``` r
# Inbreeding Coefficient
ggplot(data=col_phi_stats, aes(x=pop,y=Fis)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-7.png)<!-- -->

``` r
ggplot(data=kup_phi_stats, aes(x=pop,y=Fis)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-8.png)<!-- -->

``` r
ggplot(data=sag_phi_stats, aes(x=pop,y=Fis)) + geom_point(stat = "identity") + theme_bw() + xlab("Location") + geom_abline()
```

![](AG_POP_STATS_CODE_files/figure-gfm/diversity%20correlations-9.png)<!-- -->

``` r
Het.long<-pivot_longer(Phi1, cols=3:4,names_to="n_Het", values_to="Het")
Het.long <- tibble(Het.long)
Het_plot<-ggplot(Het.long,aes(Population, Het)) + geom_bar(aes(fill=n_Het),stat="identity",position="dodge") + 
  scale_fill_manual(values = c("#00AFBB","#FC4E07")) +
  labs(y = "Heterozygosity") +
  theme(axis.title.x = element_text(size = 13)) +
  theme_light()
  
ggsave(plot=Het_plot,filename = "Het_plot.png", dpi = 180, width = 18, height = 6)
```

``` r
# Read Fst statistic
Fst<-read.delim("LG.snps.FF.p.phistats_summary.tsv", sep = "\t",skip = 6,nrows = 2,skipNul = TRUE)
# Elongate the data set
Fst.long<-pivot_longer(Fst, cols=3:4, names_to="pop2", values_to="Fst")
# Remove 2nd column due to containing only NAs
Fst.long<-Fst.long[,-2,drop=FALSE]
names(Fst.long)<-c("pop1","pop2","Fst")
Fst.long<-na.omit((Fst.long))
```

``` r
fst_heatmap <- ggplot(data=Fst.long, mapping = aes(x=pop1,y=pop2,fill=Fst)) + geom_tile() + 
  labs(x = "Population 1", y = "population 2") + 
  theme(axis.title.x = element_text(size = 30)) +
         guides(color = guide_legend(override.aes = list(size=10))) +
  theme_light()

ggsave(plot=fst_heatmap,filename = "fst_heatmap.png", dpi = 180, width = 14, height = 7)
```

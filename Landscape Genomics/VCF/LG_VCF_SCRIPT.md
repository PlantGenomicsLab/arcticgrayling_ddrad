---
title: "LG_VCF_Script"
author: "Gabriel Barrett"
date: "10/5/2020"
output: 
  html_document:
    keep_md: True
---






```r
var_depth <- read_delim("VCF_STATS/LG.snps.ldepth.mean", delim = "\t",
                        col_names = c("chr", "pos", "mean_depth", "var_depth"), skip = 1)
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   mean_depth = col_double(),
##   var_depth = col_double()
## )
```

```r
depth_sum<-var_depth%>%
  summarise(avg=mean(mean_depth))
summary(var_depth$mean_depth)
```

```
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
##    1.000    2.294    3.883   17.971   10.667 1938.990
```

```r
a <- ggplot(var_depth, aes(mean_depth)) + geom_density(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic() + xlab("Mean Site Depth") + ylab("Density") + theme(plot.margin=grid::unit(c(0,0,0,0), "cm")) + coord_cartesian(expand=FALSE)
a100 <- a + theme_classic() + xlab("Mean Site Depth (0-100)")  + coord_cartesian(xlim = c(-0.01,100), ylim = c(0,.25), expand=F)
```

```
## Coordinate system already present. Adding new coordinate system, which will replace the existing one.
```




```r
var_miss <- read_delim("VCF_STATS/LG.snps.lmiss", delim = "\t",
                       col_names = c("chr", "pos", "nchr", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   nchr = col_double(),
##   nfiltered = col_double(),
##   nmiss = col_double(),
##   fmiss = col_double()
## )
```

```r
summary(var_miss$fmiss)
```

```
##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
## 0.00000 0.02653 0.32245 0.41337 0.81020 0.98980
```

```r
b <- ggplot(var_miss, aes(fmiss)) + geom_density(fill = "dodgerblue2", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Fraction Missing Per Site") + ylab("Density") +  theme(plot.margin=grid::unit(c(0,0,0,0), "cm"))  + coord_cartesian(xlim = c(-0.01,1),expand=F)
```


```r
var_freq <- read_delim("VCF_STATS/LG.snps.frq", delim = "\t",
                       col_names = c("chr", "pos", "nalleles", "nchr", "a1", "a2"), skip = 1)
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   nalleles = col_double(),
##   nchr = col_double(),
##   a1 = col_double(),
##   a2 = col_double()
## )
```

```r
# calculate the minor allele frequency
var_freq$maf <- var_freq %>% select(a1, a2) %>% apply(1, function(z) min(z))

freq_sum<-var_freq%>%
  summarise(avg=mean(maf), med=median(maf))

# Distribution of allele frequencies
c <- ggplot(var_freq, aes(maf)) + geom_density(fill = "dodgerblue3", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Minor Allele Frequency (maf)") + ylab("Density") +
  geom_text(x = 0.3, y = 40,aes(label=paste0("Mean:",round(avg,3)," Median:",round(med,3))),data = freq_sum) +  theme(plot.margin=grid::unit(c(0,0,0,0), "cm"))  + coord_cartesian(xlim = c(-0.009,.51),expand=F)
```


```r
arrange<-ggarrange(a+ labs(tag="A"), a100+ labs(tag="B"), b+ labs(tag="C"), c+ labs(tag="D"),nrow = 2, ncol = 2, heights=c(2,2))
ggsave(arrange, file="VCF_PLOTS/SNP_stats1.png", width=25,height=15,unit="cm",dpi=70) 
```


```r
ind_depth <- read_delim("VCF_STATS/LG.snps.FP.idepth", delim = "\t",
                        col_names = c("ind", "nsites", "depth"), skip = 1) 
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   ind = col_character(),
##   nsites = col_double(),
##   depth = col_double()
## )
```

```r
bad_ind_depth <- ind_depth %>%
  filter(depth > 30 | depth < 3) %>% select(ind)

d <- ggplot(ind_depth, aes(depth)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = .3) + ylab("# of Individuals") + xlab("Mean Depth")

ind.depth.plot <- d + theme_classic() + coord_cartesian(xlim = c(-0.01,50),expand=F)
  #scale_x_continuous(limits = c(0,50), expand = c(0, 0)) #+ 
  #scale_y_continuous(limits = c(0,110), expand = c(0,0))
```


```r
ind_miss  <- read_delim("VCF_STATS/LG.snps.FP.imiss", delim = "\t",
                        col_names = c("ind", "ndata", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   ind = col_character(),
##   ndata = col_double(),
##   nfiltered = col_double(),
##   nmiss = col_double(),
##   fmiss = col_double()
## )
```

```r
# Focus on the fmiss column telling us the proportion missing
ind.miss.plot <- ggplot(ind_miss, aes(fmiss)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic() + 
  ylab("# of Individuals") + xlab("Fraction Genotypes Missing")  + coord_cartesian(expand=F)

high_missing<-ind_miss %>%
  filter(fmiss > .65)

knitr::kable(
  high_missing, format = "html",caption = "Individuals Missing Greater than .4 Genotypes",
  booktabs = TRUE
)
```

<table>
<caption>Individuals Missing Greater than .4 Genotypes</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ind </th>
   <th style="text-align:right;"> ndata </th>
   <th style="text-align:right;"> nfiltered </th>
   <th style="text-align:right;"> nmiss </th>
   <th style="text-align:right;"> fmiss </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Golden4H11 </td>
   <td style="text-align:right;"> 4559 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 4531 </td>
   <td style="text-align:right;"> 0.993858 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden1A09 </td>
   <td style="text-align:right;"> 4559 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 3290 </td>
   <td style="text-align:right;"> 0.721649 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden3H10 </td>
   <td style="text-align:right;"> 4559 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 4547 </td>
   <td style="text-align:right;"> 0.997368 </td>
  </tr>
</tbody>
</table>


```r
ind_het <- read_delim("VCF_STATS/LG.snps.FP.het", delim = "\t",
                      col_names = c("ind","ho", "he", "nsites", "f"), skip = 1) 
```

```
## 
## -- Column specification --------------------------------------------------------
## cols(
##   ind = col_character(),
##   ho = col_double(),
##   he = col_double(),
##   nsites = col_double(),
##   f = col_double()
## )
```

```r
ind.het.plot <- ggplot(ind_het, aes(f)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic() +
  ylab("# of Individuals")+ xlab("Heterozygosity")  + coord_cartesian(expand=F)

ind_het %>%
  filter(f < -.4 | f > .5)
```

```
## # A tibble: 4 x 5
##   ind           ho     he nsites      f
##   <chr>      <dbl>  <dbl>  <dbl>  <dbl>
## 1 Golden4A03  3040 3366     4101 -0.444
## 2 Golden1A03  2866 3324.    4049 -0.632
## 3 Golden4H11    26   22.8     28  0.615
## 4 Golden3H10    11   10       12  0.509
```





```r
data <- read.table("VCF_STATS/LG.snps.FP.relatedness", header = T, stringsAsFactors = F, row.names = NULL)

same = data[data[,1] == data[,2], ]
diff = data[data[,1] != data[,2], ]
diff = na.omit(diff[diff[,3] > .7, ])
#diff = tibble(diff) %>%
#  filter(INDV1 == "Golden1C01" | INDV1 == "Golden1C03" | INDV1 == "Golden1D02" | INDV1 == #"Golden1F03" | INDV1 == "Golden5F03" | INDV1 == "Golden1A03")
diffgrob<-tableGrob(diff)

diff1<-diff%>%
  group_by(INDV1)%>%
  summarise(n())
```

```
## `summarise()` ungrouping output (override with `.groups` argument)
```

```r
diff2<-diff%>%
  group_by(INDV2)%>%
  summarise(n())
```

```
## `summarise()` ungrouping output (override with `.groups` argument)
```

```r
knitr::kable(
  diff, format = "html",caption = "Individuals Expressing a High Relatedness AJK statistic",
  booktabs = TRUE
)
```

<table>
<caption>Individuals Expressing a High Relatedness AJK statistic</caption>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> INDV1 </th>
   <th style="text-align:left;"> INDV2 </th>
   <th style="text-align:right;"> RELATEDNESS_AJK </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 6785 </td>
   <td style="text-align:left;"> Golden4F03 </td>
   <td style="text-align:left;"> Golden1C01 </td>
   <td style="text-align:right;"> 0.923133 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 7260 </td>
   <td style="text-align:left;"> Golden4F04 </td>
   <td style="text-align:left;"> Golden1C01 </td>
   <td style="text-align:right;"> 0.962919 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 12891 </td>
   <td style="text-align:left;"> Golden1A01 </td>
   <td style="text-align:left;"> Golden1G03 </td>
   <td style="text-align:right;"> 1.241260 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 16557 </td>
   <td style="text-align:left;"> Golden1D03 </td>
   <td style="text-align:left;"> Golden1E03 </td>
   <td style="text-align:right;"> 0.750549 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 17467 </td>
   <td style="text-align:left;"> Golden1F03 </td>
   <td style="text-align:left;"> Golden1H03 </td>
   <td style="text-align:right;"> 1.070460 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 27207 </td>
   <td style="text-align:left;"> Golden1A03 </td>
   <td style="text-align:left;"> Golden1H02 </td>
   <td style="text-align:right;"> 0.786431 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 28062 </td>
   <td style="text-align:left;"> Golden1C03 </td>
   <td style="text-align:left;"> Golden1D02 </td>
   <td style="text-align:right;"> 1.000280 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 28064 </td>
   <td style="text-align:left;"> Golden1C03 </td>
   <td style="text-align:left;"> Golden1F02 </td>
   <td style="text-align:right;"> 1.003020 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 28492 </td>
   <td style="text-align:left;"> Golden1D02 </td>
   <td style="text-align:left;"> Golden1F02 </td>
   <td style="text-align:right;"> 1.145890 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 95991 </td>
   <td style="text-align:left;"> Golden5F03 </td>
   <td style="text-align:left;"> Golden5H04 </td>
   <td style="text-align:right;"> 1.018670 </td>
  </tr>
</tbody>
</table>


```r
arrange<-ggarrange(ind.depth.plot + labs(tag="A"), ind.miss.plot+ labs(tag="B"), ind.het.plot+ labs(tag="C"), diffgrob, nrow = 2, ncol = 2, heights=c(3,3))
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

```r
ggsave(arrange, file="VCF_PLOTS/SNP_stats2.png", width=25,height=15,unit="cm",dpi=70) 
```


```r
join_individual<- left_join(ind_depth,ind_miss,by="ind")
bad_sam<- join_individual %>%
  filter(depth > 35 | depth < 3 | fmiss > .65)%>%
  mutate("fmiss (%)" = fmiss * 100) %>%
  select(!(c(nfiltered,fmiss)))

#bad_samples <- bad_sam %>%
  #select(ind)
#rel_samples <- diff %>%
  #select("ind"=INDV1)
#rel_samples <- tibble(ind = rel_samples[match(unique(rel_samples$ind), rel_samples$ind),])

#final_bad_samples<-bind_rows(list(rel_samples, bad_samples))

#high_rel <- data.frame(unique(diff$INDV1))

#ggsave("VCF_PLOTS/bad_ind_samples.png", tableGrob(tibble(bad_sam)))
#ggsave("VCF_PLOTS/bad_rel_samples.png", diffgrob)
#write.table(bad_sam %>% select(ind), file = "./bad_samples.txt", quote = FALSE, row.names = FALSE, col.names = FALSE)
```


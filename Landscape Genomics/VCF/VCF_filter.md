---
title: "VCF_filter"
author: "Gabriel Barrett"
date: "10/5/2020"
output: 
  html_document:
    keep_md: True
---






```r
var_depth <- read_delim("VCF_STATS/LG.snps.ldepth.mean", delim = "\t",
                        col_names = c("chr", "pos", "mean_depth", "var_depth"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   mean_depth = col_double(),
##   var_depth = col_double()
## )
```

```r
summary(var_depth$mean_depth)
```

```
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
##    1.000    2.952    5.011   22.789   19.784 1822.070
```

```r
a <- ggplot(var_depth, aes(mean_depth)) + geom_density(fill = "dodgerblue1", colour = "black", alpha = 0.3)

a + theme_light()
```

![](VCF_PLOTS/mean site depth-1.png)<!-- -->

```r
a + theme_light() + xlim(0,100)
```

```
## Warning: Removed 2154 rows containing non-finite values (stat_density).
```

![](VCF_PLOTS/mean site depth-2.png)<!-- -->

## Including Plots

You can also embed plots, for example:


```r
var_miss <- read_delim("VCF_STATS/LG.snps.lmiss", delim = "\t",
                       col_names = c("chr", "pos", "nchr", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   nchr = col_double(),
##   nfiltered = col_double(),
##   nmiss = col_double(),
##   fmiss = col_double()
## )
```

```r
summary(var_miss$fmiss)
```

```
##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
## 0.00000 0.04286 0.36429 0.41798 0.77857 0.97857
```

```r
b <- ggplot(var_miss, aes(fmiss)) + geom_density(fill = "dodgerblue2", colour = "black", alpha = 0.3)
b + theme_light()
```

![](VCF_PLOTS/Missing Sites-1.png)<!-- -->


```r
var_freq <- read_delim("VCF_STATS/LG.snps.frq", delim = "\t",
                       col_names = c("chr", "pos", "nalleles", "nchr", "a1", "a2"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   chr = col_character(),
##   pos = col_double(),
##   nalleles = col_double(),
##   nchr = col_double(),
##   a1 = col_double(),
##   a2 = col_double()
## )
```

```r
# calculate the minor allele frequency
var_freq$maf <- var_freq %>% select(a1, a2) %>% apply(1, function(z) min(z))
summary(var_freq$maf)
```

```
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
## 0.002358 0.019176 0.052632 0.131777 0.201439 0.500000
```

```r
# Distribution of allele frequencies
c <- ggplot(var_freq, aes(maf)) + geom_density(fill = "dodgerblue3", colour = "black", alpha = 0.3)
c + theme_light()
```

![](VCF_PLOTS/Allele Frequency-1.png)<!-- -->


```r
ind_depth <- read_delim("VCF_STATS/LG.snps.idepth", delim = "\t",
                        col_names = c("ind", "nsites", "depth"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   ind = col_character(),
##   nsites = col_double(),
##   depth = col_double()
## )
```

```r
ind_depth %>%
  filter(depth > 20)
```

```
## # A tibble: 3 x 3
##   ind        nsites depth
##   <chr>       <dbl> <dbl>
## 1 Golden2G11   2397  34.9
## 2 Golden3D10   2438  21.2
## 3 Golden3F10   2411  22.0
```

```r
d <- ggplot(ind_depth, aes(depth)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = .3)

d + theme_light() 
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](VCF_PLOTS/unnamed-chunk-1-1.png)<!-- -->


```r
ind_miss  <- read_delim("VCF_STATS/LG.snps.imiss", delim = "\t",
                        col_names = c("ind", "ndata", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   ind = col_character(),
##   ndata = col_double(),
##   nfiltered = col_double(),
##   nmiss = col_double(),
##   fmiss = col_double()
## )
```

```r
# Focus on the fmiss column telling us the proportion missing
e <- ggplot(ind_miss, aes(fmiss)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3)
e + theme_light()
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](VCF_PLOTS/unnamed-chunk-2-1.png)<!-- -->

```r
high_missing<-ind_miss %>%
  filter(fmiss > .4)

knitr::kable(
  high_missing, format = "html",caption = "Individuals Missing Greater than .4 Genotypes",
  booktabs = TRUE
)
```

<table>
<caption>Individuals Missing Greater than .4 Genotypes</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ind </th>
   <th style="text-align:right;"> ndata </th>
   <th style="text-align:right;"> nfiltered </th>
   <th style="text-align:right;"> nmiss </th>
   <th style="text-align:right;"> fmiss </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Golden1H02 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 995 </td>
   <td style="text-align:right;"> 0.405791 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden1A09 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 2150 </td>
   <td style="text-align:right;"> 0.876835 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden2G09 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1011 </td>
   <td style="text-align:right;"> 0.412316 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden2G11 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 2094 </td>
   <td style="text-align:right;"> 0.853997 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden2H12 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1320 </td>
   <td style="text-align:right;"> 0.538336 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden3B02 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1132 </td>
   <td style="text-align:right;"> 0.461664 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden3D10 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1115 </td>
   <td style="text-align:right;"> 0.454731 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden3F10 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1179 </td>
   <td style="text-align:right;"> 0.480832 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Golden3H10 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 2452 </td>
   <td style="text-align:right;"> 1.000000 </td>
  </tr>
</tbody>
</table>


```r
ind_het <- read_delim("VCF_STATS/LG.snps.het", delim = "\t",
                      col_names = c("ind","ho", "he", "nsites", "f"), skip = 1)
```

```
## Parsed with column specification:
## cols(
##   ind = col_character(),
##   ho = col_double(),
##   he = col_double(),
##   nsites = col_double(),
##   f = col_double()
## )
```

```r
f <- ggplot(ind_het, aes(f)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3)
f + theme_light()
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](VCF_PLOTS/Heterozygosity-1.png)<!-- -->

```r
ind_het %>%
  filter(f < -.5)
```

```
## # A tibble: 1 x 5
##   ind           ho    he nsites      f
##   <chr>      <dbl> <dbl>  <dbl>  <dbl>
## 1 Golden1A03  1419  1710   2140 -0.677
```



```r
data <- read.table("VCF_STATS/LG.snps.relatedness", header = T, stringsAsFactors = F)

threshold = .75

same = data[data[,1] == data[,2], ]
diff = data[data[,1] != data[,2], ]
diff = na.omit(diff[diff[,3] > .8, ])

knitr::kable(
  diff, format = "html",caption = "Individuals Expressing a High Relatedness AJK statistic",
  booktabs = TRUE
)
```

<table>
<caption>Individuals Expressing a High Relatedness AJK statistic</caption>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> INDV1 </th>
   <th style="text-align:left;"> INDV2 </th>
   <th style="text-align:right;"> RELATEDNESS_AJK </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> Golden1A01 </td>
   <td style="text-align:left;"> Golden1G03 </td>
   <td style="text-align:right;"> 1.052030 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 3028 </td>
   <td style="text-align:left;"> Golden1D02 </td>
   <td style="text-align:left;"> Golden1F02 </td>
   <td style="text-align:right;"> 1.064750 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 3033 </td>
   <td style="text-align:left;"> Golden1D02 </td>
   <td style="text-align:left;"> Golden1C03 </td>
   <td style="text-align:right;"> 0.900824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 3568 </td>
   <td style="text-align:left;"> Golden1F02 </td>
   <td style="text-align:left;"> Golden1C03 </td>
   <td style="text-align:right;"> 0.966551 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5673 </td>
   <td style="text-align:left;"> Golden1F03 </td>
   <td style="text-align:left;"> Golden1H03 </td>
   <td style="text-align:right;"> 0.968375 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 8480 </td>
   <td style="text-align:left;"> Golden1A05 </td>
   <td style="text-align:left;"> Golden1A09 </td>
   <td style="text-align:right;"> 1.296420 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10428 </td>
   <td style="text-align:left;"> Golden1F06 </td>
   <td style="text-align:left;"> Golden1A09 </td>
   <td style="text-align:right;"> 0.876510 </td>
  </tr>
</tbody>
</table>


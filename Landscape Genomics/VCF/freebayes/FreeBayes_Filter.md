FreeBayes\_Filter
================
Gabriel Barrett
6/21/2021

``` r
#1. Filter LQ Sites & Indels
#  a.   minQ 20 – minDP 3 – meanDP 3 – mac 3
#  b.   –max-missing 0.5
#  c.   Missing Site Per Population
#2. Remove Problematic Individuals Population
#  a.   Missing
#  b.   Depth
#  c.   Relatedness
#  d.  Heterozygosity
#3. INFO Flags
#  a.   Allele balance
#  c.   Quality score
#  d.   Mapping quality
#  e.   Strand balance
#  f.   Properly paired status
#  b.   MaxDP - meanDP
#4. Filter Missingness 
#  a.   Genotype call rate < 80% -> 90% -> 95%
#  b.   Individuals > 30%
```

``` r
knitr::opts_knit$set(root.dir = "D:/Wegrzyn_Lab/Grayling/arcticgrayling_ddrad/Landscape Genomics/VCF/freebayes/")

knitr::opts_chunk$set(fig.path = "PLOTS/")

library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.0 --

    ## v ggplot2 3.3.5     v purrr   0.3.4
    ## v tibble  3.1.3     v dplyr   1.0.7
    ## v tidyr   1.1.2     v stringr 1.4.0
    ## v readr   1.4.0     v forcats 0.5.0

    ## Warning: package 'ggplot2' was built under R version 4.0.5

    ## Warning: package 'dplyr' was built under R version 4.0.5

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
library(ggpubr)
library(gridExtra)
```

    ## 
    ## Attaching package: 'gridExtra'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     combine

``` r
library(readxl)
```

``` r
var_qual <- read_delim("STATS/fb.lqual", delim = "\t",
           col_names = c("chr", "pos", "qual"), skip = 1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   chr = col_character(),
    ##   pos = col_double(),
    ##   qual = col_double()
    ## )

``` r
q <- ggplot(var_qual, aes(qual)) + geom_density(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic()
q 
```

![](PLOTS/QUALITY-1.png)<!-- -->

``` r
q + coord_cartesian(xlim = c(-0.01,50000))
```

![](PLOTS/QUALITY-2.png)<!-- -->

``` r
var_depth <- read_delim("STATS/fb.ldepth", delim = "\t",
                        col_names = c("chr", "pos", "sum_depth", "sumsq_depth"), skip = 1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   chr = col_character(),
    ##   pos = col_double(),
    ##   sum_depth = col_double(),
    ##   sumsq_depth = col_double()
    ## )

``` r
sd <- ggplot(var_depth, aes(sum_depth)) + geom_density(fill = "dodgerblue1", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Sum Site Depth") + ylab("Density") + theme(plot.margin=grid::unit(c(0,0,0,0), "cm")) + coord_cartesian(expand=FALSE)

sd100 <- sd + xlab("Sum Site Depth (0-100)") + coord_cartesian(xlim = c(-0.01,100), ylim = c(0,.25), expand=F)
```

    ## Coordinate system already present. Adding new coordinate system, which will replace the existing one.

``` r
ggarrange(sd + labs(tag="A"), sd100 + labs(tag="B"), ncol=2)
```

![](PLOTS/Site%20DEPTH-1.png)<!-- -->

``` r
summary(var_depth$sum_depth)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##       1    1697    3140    9110   11830   54895

``` r
var_mean_depth <- read_delim("STATS/fb.ldepth.mean", delim = "\t",
                        col_names = c("chr", "pos", "mean_depth", "var_depth"), skip = 1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   chr = col_character(),
    ##   pos = col_double(),
    ##   mean_depth = col_double(),
    ##   var_depth = col_double()
    ## )

``` r
msd <- ggplot(var_mean_depth, aes(mean_depth)) + geom_density(fill = "dodgerblue1", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Mean Site Depth") + ylab("Density") + theme(plot.margin=grid::unit(c(0,0,0,0), "cm")) + coord_cartesian(expand=FALSE)

msd100 <- msd + xlab("Mean Site Depth (0-100)") + coord_cartesian(xlim = c(-0.01,100), ylim = c(0,.25), expand=F)
```

    ## Coordinate system already present. Adding new coordinate system, which will replace the existing one.

``` r
summary(var_mean_depth$mean_depth)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   1.000   4.337   7.040  19.448  25.257 149.403

``` r
mean <- mean(var_mean_depth$mean_depth)
std <- sd(var_mean_depth$mean_depth)
cutoff <- sum(mean + (2*std))

ggarrange(msd + labs(tag="A"), msd100+ labs(tag="B"), ncol=2)
```

![](PLOTS/MEAN%20SITE%20DEPTH-1.png)<!-- -->

``` r
var_miss <- read_delim("STATS/fb.lmiss", delim = "\t",
                       col_names = c("chr", "pos", "nchr", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   chr = col_character(),
    ##   pos = col_double(),
    ##   nchr = col_double(),
    ##   nfiltered = col_double(),
    ##   nmiss = col_double(),
    ##   fmiss = col_double()
    ## )

``` r
summary(var_miss$fmiss)
```

    ##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    ## 0.000000 0.002045 0.024033 0.067217 0.104848 0.995927

``` r
ms <- ggplot(var_miss, aes(fmiss)) + geom_density(fill = "dodgerblue2", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Fraction Missing Per Site") + ylab("Density") +  theme(plot.margin=grid::unit(c(0,0,0,0), "cm"))  + coord_cartesian(xlim = c(-0.01,1),expand=F)
ms
```

![](PLOTS/Missing%20Sites-1.png)<!-- -->

``` r
var_freq <- read_delim("STATS/fb.frq", delim = "\t",
                       col_names = c("chr", "pos", "nalleles", "nchr", "a1", "a2"), skip = 1)

# calculate the minor allele frequency
var_freq$maf <- var_freq %>% select(a1, a2) %>% apply(1, function(z) min(z))

freq_sum<-var_freq%>%
  summarise(avg=mean(maf), med=median(maf))

# Distribution of allele frequencies
af <- ggplot(var_freq, aes(maf)) + geom_density(fill = "dodgerblue3", colour = "black", alpha = 0.3) + 
  theme_classic() + xlab("Minor Allele Frequency (maf)") + ylab("Density") +
  geom_text(x = 0.3, y = 40,aes(label=paste0("Mean:",round(avg,3)," Median:",round(med,3))),data = freq_sum) +  theme(plot.margin=grid::unit(c(0,0,0,0), "cm"))  + coord_cartesian(xlim = c(-0.009,.51),expand=F)
af
```

``` r
var_hwe <- read_delim("STATS/fb.hwe", delim = "\t",
                      col_names = c("chr", "pos", "obs", "exp", "chi_sq", "p_hwe", "p_het_def", "p_het_exc"), skip=1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   chr = col_character(),
    ##   pos = col_double(),
    ##   obs = col_character(),
    ##   exp = col_character(),
    ##   chi_sq = col_double(),
    ##   p_hwe = col_double(),
    ##   p_het_def = col_double(),
    ##   p_het_exc = col_double()
    ## )

``` r
summary(var_hwe$p_hwe)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##  0.0000  1.0000  1.0000  0.8725  1.0000  1.0000

``` r
hwe <- ggplot(var_hwe, aes(p_hwe)) + geom_density(fill = "dodgerblue3", colour = "black", alpha = 0.3) + theme_classic()
hwe
```

![](PLOTS/Hardy-Weinberg%20Equilibrium-1.png)<!-- -->

## INFO Stats

``` r
AB <- read.delim("STATS/LG.haps.fb.AB", sep=" ", header=F, col.names = "AB")
hist(AB$AB)
```

![](PLOTS/Allele%20Balance-1.png)<!-- -->

``` r
SAF <- read.delim("STATS/LG.haps.fb.SAF",header = F)
SAR <- read.delim("STATS/LG.haps.fb.SAR",header = F)
SRF <- read.delim("STATS/LG.haps.fb.SRF",header = F)
SRR <- read.delim("STATS/LG.haps.fb.SRR",header = F)

MQM <- read.delim("STATS/LG.haps.fb.MQM",header = F)
MQMR <- read.delim("STATS/LG.haps.fb.MQMR",header = F)

hist(MQM$V1 / MQMR$V1, xlim=c(.8,1.2))
```

![](PLOTS/Allele%20Balance-2.png)<!-- -->

``` r
PAIRED <- read.delim("STATS/LG.haps.fb.PAIRED",header = F)
PAIREDR <- read.delim("STATS/LG.haps.fb.PAIREDR",header = F)

hist(PAIREDR$V1 / PAIRED$V1, xlim=c(.1,3))
```

    ## Warning in PAIREDR$V1/PAIRED$V1: longer object length is not a multiple of
    ## shorter object length

![](PLOTS/Allele%20Balance-3.png)<!-- -->

``` r
QUAL <- read.delim("STATS/LG.haps.fb.QUAL",header = F)
DP <- read.delim("STATS/LG.haps.fb.DEPTH",header = F)
hist(QUAL$V3 / DP$V1)
```

![](PLOTS/Allele%20Balance-4.png)<!-- --> \#\# Individual Stats

``` r
ind_miss  <- read_delim("STATS/fb_F.imiss", delim = "\t",
                        col_names = c("ind", "ndata", "nfiltered", "nmiss", "fmiss"), skip = 1)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   ind = col_character(),
    ##   ndata = col_double(),
    ##   nfiltered = col_double(),
    ##   nmiss = col_double(),
    ##   fmiss = col_double()
    ## )

``` r
ind_miss %>% arrange(desc(fmiss))
```

    ## # A tibble: 490 x 5
    ##    ind        ndata nfiltered nmiss fmiss
    ##    <chr>      <dbl>     <dbl> <dbl> <dbl>
    ##  1 Golden3H10  9102         0  9078 0.997
    ##  2 Golden4H11  9102         0  9057 0.995
    ##  3 Golden1A09  9102         0  6388 0.702
    ##  4 Golden4E07  9102         0  5361 0.589
    ##  5 Golden5E11  9102         0  4620 0.508
    ##  6 Golden5H11  9102         0  4519 0.496
    ##  7 Golden2H12  9102         0  4289 0.471
    ##  8 Golden3B02  9102         0  4238 0.466
    ##  9 Golden5H06  9102         0  4172 0.458
    ## 10 Golden2G09  9102         0  4018 0.441
    ## # ... with 480 more rows

``` r
# Focus on the fmiss column telling us the proportion missing
ind.miss.plot <- ggplot(ind_miss, aes(fmiss)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic() + 
  ylab("# of Individuals") + xlab("Fraction Genotypes Missing")  + coord_cartesian(expand=F)

high_missing<-ind_miss %>%
  filter(fmiss > .5)

knitr::kable(
  high_missing, format = "html",caption = "Individuals Missing Greater than .4 Genotypes",
  booktabs = TRUE
)
```

<table>

<caption>

Individuals Missing Greater than .4 Genotypes

</caption>

<thead>

<tr>

<th style="text-align:left;">

ind

</th>

<th style="text-align:right;">

ndata

</th>

<th style="text-align:right;">

nfiltered

</th>

<th style="text-align:right;">

nmiss

</th>

<th style="text-align:right;">

fmiss

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:left;">

Golden4H11

</td>

<td style="text-align:right;">

9102

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:right;">

9057

</td>

<td style="text-align:right;">

0.995056

</td>

</tr>

<tr>

<td style="text-align:left;">

Golden3H10

</td>

<td style="text-align:right;">

9102

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:right;">

9078

</td>

<td style="text-align:right;">

0.997363

</td>

</tr>

<tr>

<td style="text-align:left;">

Golden1A09

</td>

<td style="text-align:right;">

9102

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:right;">

6388

</td>

<td style="text-align:right;">

0.701824

</td>

</tr>

<tr>

<td style="text-align:left;">

Golden4E07

</td>

<td style="text-align:right;">

9102

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:right;">

5361

</td>

<td style="text-align:right;">

0.588991

</td>

</tr>

<tr>

<td style="text-align:left;">

Golden5E11

</td>

<td style="text-align:right;">

9102

</td>

<td style="text-align:right;">

0

</td>

<td style="text-align:right;">

4620

</td>

<td style="text-align:right;">

0.507581

</td>

</tr>

</tbody>

</table>

``` r
ind_depth <- read_delim("STATS/fb_F.idepth", delim = "\t",
                        col_names = c("ind", "nsites", "depth"), skip = 1) 
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   ind = col_character(),
    ##   nsites = col_double(),
    ##   depth = col_double()
    ## )

``` r
ind_depth %>% arrange(depth)
```

    ## # A tibble: 490 x 3
    ##    ind        nsites depth
    ##    <chr>       <dbl> <dbl>
    ##  1 Golden3H10    827  1.18
    ##  2 Golden4H11   1130  1.28
    ##  3 Golden4E07   6805  4.71
    ##  4 Golden5E11   7133  6.09
    ##  5 Golden5H06   7549  6.16
    ##  6 Golden5H11   7173  6.29
    ##  7 Golden1H02   7891  6.47
    ##  8 Golden1A09   5939  6.59
    ##  9 Golden5F07   8045  6.92
    ## 10 Golden3B02   7416  6.95
    ## # ... with 480 more rows

``` r
ind_depth %>% arrange(desc(depth))
```

    ## # A tibble: 490 x 3
    ##    ind        nsites depth
    ##    <chr>       <dbl> <dbl>
    ##  1 Golden2G11   8933  67.2
    ##  2 Golden4B03   9054  49.1
    ##  3 Golden3F10   9018  37.6
    ##  4 Golden3D10   9021  37.1
    ##  5 Golden6A01   8962  34.8
    ##  6 Golden4G05   8990  34.2
    ##  7 Golden5D05   8948  31.8
    ##  8 Golden4D10   9003  31.6
    ##  9 Golden5E06   8952  31.0
    ## 10 Golden1D05   8941  29.8
    ## # ... with 480 more rows

``` r
ind_depth %>%
  filter(depth > 40 | depth < 3) %>% select(ind, depth)
```

    ## # A tibble: 4 x 2
    ##   ind        depth
    ##   <chr>      <dbl>
    ## 1 Golden4H11  1.28
    ## 2 Golden3H10  1.18
    ## 3 Golden4B03 49.1 
    ## 4 Golden2G11 67.2

``` r
d <- ggplot(ind_depth, aes(depth)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = .3) + ylab("# of Individuals") + xlab("Mean Depth")

#ind.depth.plot <- d + theme_classic() + coord_cartesian(xlim = c(-0.01,50),expand=F)
  #scale_x_continuous(limits = c(0,50), expand = c(0, 0)) #+ 
  #scale_y_continuous(limits = c(0,110), expand = c(0,0))
```

``` r
ind_het <- read_delim("STATS/fb_F.het", delim = "\t",
                      col_names = c("ind","ho", "he", "nsites", "f"), skip = 1) 
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   ind = col_character(),
    ##   ho = col_double(),
    ##   he = col_double(),
    ##   nsites = col_double(),
    ##   f = col_double()
    ## )

``` r
ind.het.plot <- ggplot(ind_het, aes(f)) + geom_histogram(fill = "dodgerblue1", colour = "black", alpha = 0.3) + theme_classic() +
  ylab("# of Individuals")+ xlab("Heterozygosity")  + coord_cartesian(expand=F)
ind_het %>% arrange(desc(f))
```

    ## # A tibble: 490 x 5
    ##    ind           ho     he nsites     f
    ##    <chr>      <dbl>  <dbl>  <dbl> <dbl>
    ##  1 Golden3H10    14   10.8     14 1    
    ##  2 Golden3B02  3073 2819.    3448 0.404
    ##  3 Golden3H01  4306 3973.    4872 0.371
    ##  4 Golden5H07  4334 4017.    4932 0.346
    ##  5 Golden3H04  4542 4212.    5167 0.345
    ##  6 Golden3A10  4723 4380     5375 0.345
    ##  7 Golden5B07  4264 3957.    4871 0.336
    ##  8 Golden5D06  4559 4234.    5204 0.335
    ##  9 Golden3G01  4609 4284.    5255 0.335
    ## 10 Golden3G06  4468 4153.    5103 0.332
    ## # ... with 480 more rows

``` r
ind_het %>%
  filter(f < -.5 | f > .5)
```

    ## # A tibble: 2 x 5
    ##   ind           ho     he nsites      f
    ##   <chr>      <dbl>  <dbl>  <dbl>  <dbl>
    ## 1 Golden3H10    14   10.8     14  1    
    ## 2 Golden1A03  3796 4395     5392 -0.601

``` r
meta <- read.table("../../../GenomicsBioinformatics_EnvVersion_20210111.csv",sep=",",header=T) %>% select(INDV1=bioinformatics.id, pop=location)

data <- read.table("STATS/fb.mac3.minQ20.minDP3.miss5.ab.bs.mq.ps.relatedness", header = T, stringsAsFactors = F, row.names = NULL) %>% left_join(.,meta,by="INDV1")


same = data[data[,1] == data[,2], ]
diff = data[data[,1] != data[,2], ]
diff = na.omit(diff[diff[,3] > .3, ])
#diff = tibble(diff) %>%
  #filter(INDV1 == "Golden1C01" | INDV1 == "Golden1C03" | INDV1 == "Golden1D02" | INDV1 == "Golden1F03" | INDV1 == "Golden5F03" | INDV1 == "Golden1A03")
#diffgrob<-tableGrob(diff %>% arrange(RELATEDNESS_AJK))
head(diff %>% arrange(desc(RELATEDNESS_AJK)))
```

    ##        INDV1      INDV2 RELATEDNESS_AJK  pop
    ## 1 Golden6D04 Golden1A09         3.94488 CGO3
    ## 2 Golden6A02 Golden1A09         3.73665  CGK
    ## 3 Golden4E11 Golden1A09         3.19173  GCL
    ## 4 Golden6B01 Golden1A09         2.80135  CGK
    ## 5 Golden4D11 Golden1A09         1.78829  GCL
    ## 6 Golden6G02 Golden1A09         1.65303  CGK

``` r
diff %>% filter(pop == "Oks0" | pop == "Kup2") %>% arrange(desc(RELATEDNESS_AJK))
```

    ##         INDV1      INDV2 RELATEDNESS_AJK  pop
    ## 1  Golden3F04 Golden3G02        0.522268 Oks0
    ## 2  Golden1D11 Golden1G10        0.477582 Kup2
    ## 3  Golden1D11 Golden1A11        0.460876 Kup2
    ## 4  Golden1A11 Golden1G10        0.413662 Kup2
    ## 5  Golden1C10 Golden1A11        0.411789 Kup2
    ## 6  Golden1D10 Golden1C09        0.408003 Kup2
    ## 7  Golden1H09 Golden1G10        0.402616 Kup2
    ## 8  Golden1H09 Golden1F09        0.400773 Kup2
    ## 9  Golden3F04 Golden3D04        0.392829 Oks0
    ## 10 Golden1C11 Golden1B10        0.392563 Kup2
    ## 11 Golden1C10 Golden1B10        0.387933 Kup2
    ## 12 Golden3D04 Golden3G02        0.380763 Oks0
    ## 13 Golden1B10 Golden1G10        0.376893 Kup2
    ## 14 Golden1D09 Golden1G09        0.376884 Kup2
    ## 15 Golden1C10 Golden1H10        0.367817 Kup2
    ## 16 Golden3C03 Golden5F06        0.365397 Oks0
    ## 17 Golden1F09 Golden1H10        0.364526 Kup2
    ## 18 Golden1D11 Golden1C11        0.363676 Kup2
    ## 19 Golden1C11 Golden1G10        0.363207 Kup2
    ## 20 Golden1B10 Golden1H10        0.361235 Kup2
    ## 21 Golden1E10 Golden1G09        0.360828 Kup2
    ## 22 Golden1D10 Golden1A10        0.360471 Kup2
    ## 23 Golden1D11 Golden1C10        0.358051 Kup2
    ## 24 Golden1C10 Golden1F09        0.356402 Kup2
    ## 25 Golden1C10 Golden1H09        0.353985 Kup2
    ## 26 Golden1F10 Golden1C09        0.352722 Kup2
    ## 27 Golden1H09 Golden1H10        0.351722 Kup2
    ## 28 Golden1D11 Golden1B10        0.348907 Kup2
    ## 29 Golden1C10 Golden1G10        0.346436 Kup2
    ## 30 Golden1C11 Golden1C10        0.340291 Kup2
    ## 31 Golden1A10 Golden1C09        0.337214 Kup2
    ## 32 Golden1F10 Golden1D10        0.334885 Kup2
    ## 33 Golden3G04 Golden3E04        0.333960 Oks0
    ## 34 Golden1C11 Golden1H09        0.331799 Kup2
    ## 35 Golden1F10 Golden1A10        0.328706 Kup2
    ## 36 Golden3E02 Golden5C06        0.326665 Oks0
    ## 37 Golden1H09 Golden1B10        0.325289 Kup2
    ## 38 Golden1H10 Golden1A11        0.323929 Kup2
    ## 39 Golden1D10 Golden1G09        0.322544 Kup2
    ## 40 Golden1D09 Golden1E09        0.321423 Kup2
    ## 41 Golden1G09 Golden1E09        0.318851 Kup2
    ## 42 Golden1C11 Golden1A11        0.318837 Kup2
    ## 43 Golden1H09 Golden1A11        0.309786 Kup2
    ## 44 Golden1F09 Golden1A11        0.308442 Kup2
    ## 45 Golden1C11 Golden1H10        0.303986 Kup2
    ## 46 Golden1D11 Golden1H09        0.303585 Kup2
    ## 47 Golden1F09 Golden1B10        0.302163 Kup2
    ## 48 Golden1G09 Golden1G10        0.301352 Kup2
    ## 49 Golden1H09 Golden1G09        0.300156 Kup2

``` r
diff1<-diff%>%
  filter(RELATEDNESS_AJK > .6) %>%
  #group_by(INDV1)%>% summarise(sum=n()) %>% 
  arrange(desc(RELATEDNESS_AJK))

diff2<-diff%>%
  group_by(INDV2)%>%
  summarise(n())

#knitr::kable(
    #diff %>% arrange(RELATEDNESS_AJK), format = "html",caption = "Individuals Expressing a High Relatedness AJK statistic",
  #booktabs = TRUE
#)
ggplot(data=diff) + geom_density(aes(RELATEDNESS_AJK), fill = "dodgerblue2", alpha = .3) #+ xlim(-.5,.5) + theme_bw()
```

![](PLOTS/Relatedness-1.png)<!-- -->

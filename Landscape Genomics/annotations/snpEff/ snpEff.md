```shell
#!/bin/bash
#SBATCH --job-name=snpeff
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=gabriel.barrett@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
module load snpEff

# Build Database ASM434828v1
java -jar $SNPEFF build -gff3 -v ASM434828v1 | tee ASM434828v1.build

# Annotate VCF
java -Xmx8g -jar $SNPEFF -v ASM434828v1 ./data/populations.snps.vcf >> ./data/populations.snps.ann.vcf | tee snpEff.ann

```
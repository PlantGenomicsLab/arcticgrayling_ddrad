chrom	pos	annotation	gene_count	annotation_impact	HGVS.p	EggNOG.Predicted.Gene	EggNOG.Description	EggNOG.GO.Biological	harmony
13	25368882	downstream_gene_variant	1	MODIFIER	NA	ZNF862	Zinc finger protein 862	GO:0065007-biological regulation(L=1),	7.36034521392447
13	33746499	intergenic_region	2	MODIFIER	NA	NA	NA	NA	6.59744995152541
13	33746498	intergenic_region	2	MODIFIER	NA	NA	NA	NA	6.56753198441414
17	37406744	missense_variant	2	MODERATE	p.Ala35Glu	DDIT3	DNA-damage-inducible transcript 3	GO:0065007-biological regulation(L=1),	6.24511793451329
17	37406722	synonymous_variant	2	LOW	p.Val42Val	DDIT3	DNA-damage-inducible transcript 3	GO:0065007-biological regulation(L=1),	6.09871244427991
12	6079958	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	5.77480133615122
17	2711745	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	5.68178615003743
18	26860655	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	5.65458740344815
17	41042677	intron_variant	1	MODIFIER	NA	CTSA	cathepsin A	GO:0008152-metabolic process(L=1),	5.41894668873563
17	15124396	intergenic_region	2	MODIFIER	NA	NA	NA	NA	5.39538022512207
17	24043083	intergenic_region	2	MODIFIER	NA	NA	NA	NA	5.3243476875304
15	14902460	intron_variant	2	MODIFIER	NA	NA	NA	NA	5.26622665966096
14	16141627	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.26600459447544
11	829335	intron_variant	1	MODIFIER	NA	NA	NA	NA	5.18769804678846
18	17712895	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.1774442463858
11	12453433	downstream_gene_variant	2	MODIFIER	NA	NA	Homeobox domain	GO:0065007-biological regulation(L=1),	5.13319073442306
18	139879	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	5.12785387817843
16	23803085	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.11410170161339
15	20054159	intron_variant	2	MODIFIER	NA	NA	NA	NA	5.06939495410542
17	24043056	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.99740474168187
15	14902196	intron_variant	2	MODIFIER	NA	NA	NA	NA	4.99335762772692
14	8419267	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.93135716226223
13	16897245	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.93093854290838
15	20054033	intron_variant	2	MODIFIER	NA	NA	NA	NA	4.92683580554633
13	13476882	intron_variant	1	MODIFIER	NA	GRID2	Glutamate receptor, ionotropic, delta 2	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	4.88576785511064
17	37788066	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.88213805482635
18	15463262	synonymous_variant	1	LOW	p.Leu504Leu	NA	Transmembrane protein	NA	4.86879215727458
11	10150898	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.86193262179177
17	41066807	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.83832538398548
17	33706857	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.78297607545636
10	10708780	downstream_gene_variant	1	MODIFIER	NA	KRIT1	KRIT1, ankyrin repeat containing	GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	4.77752537366061
11	12453417	downstream_gene_variant	2	MODIFIER	NA	NA	Homeobox domain	GO:0065007-biological regulation(L=1),	4.7762250645638
11	29783984	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.7297041841642
12	25255530	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	4.70610444281754
11	3061900	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.70308637846317
11	32121775	synonymous_variant	4	LOW	p.Pro116Pro	MCM5	minichromosome maintenance complex component 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.70132756612962
11	3061610	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.69552467295216
11	2535030	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.68631665706779
18	20218603	intron_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.64820228068781
16	9784262	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.64792842217681
17	43870778	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.62896848536529
12	11287650	upstream_gene_variant	1	MODIFIER	NA	TSHZ2	teashirt zinc finger homeobox 2	GO:0065007-biological regulation(L=1),	4.62674892614319
10	15905477	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.61371085871498
12	25255531	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	4.59087385770139
15	1004716	intron_variant	2	MODIFIER	NA	NA	NA	NA	4.56211371821944
16	10919869	intron_variant	1	MODIFIER	NA	NCK2	NCK adaptor protein	NA	4.56168361587801
11	29573932	synonymous_variant	2	LOW	p.Gly916Gly	NA	NA	NA	4.53383767392333
16	36872523	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.52456143539644
11	13257845	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.52447302778914
17	43870783	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.52221399429431
11	12883791	intron_variant	6	MODIFIER	NA	NA	NA	NA	4.51701420836696
18	24619926	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.50837404210899
13	26296478	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.5025121758606
11	32121471	synonymous_variant	4	LOW	p.Arg168Arg	MCM5	minichromosome maintenance complex component 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.48068871442868
15	1004699	intron_variant	2	MODIFIER	NA	NA	NA	NA	4.45482696721309
13	10240880	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.45086473657301
17	43870986	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.44859495023283
17	15123986	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.42404087025382
10	13186047	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.41559609627479
11	29573964	missense_variant	2	MODERATE	p.Thr906Ala	NA	NA	NA	4.41465591564272
13	453675	synonymous_variant	1	LOW	p.Ser659Ser	NA	NA	NA	4.41226605510399
13	14313936	downstream_gene_variant	3	MODIFIER	NA	NA	NA	NA	4.40470846430733
11	3838462	upstream_gene_variant	2	MODIFIER	NA	NA	BAH domain and coiled-coil containing 1	NA	4.4035089222248
13	948988	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.40136325519701
10	13762955	intron_variant	1	MODIFIER	NA	NA	protein tyrosine phosphatase, receptor type, U	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.39639231876537
11	32121790	synonymous_variant	4	LOW	p.Val111Val	MCM5	minichromosome maintenance complex component 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.36520047726345
11	32121472	missense_variant	4	MODERATE	p.Arg168Lys	MCM5	minichromosome maintenance complex component 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.35105023883238
14	24887462	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.34958514424376
18	7727373	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.33965238944916
14	14103377	splice_region_variant&intron_variant	2	LOW	NA	SCARF2	scavenger receptor class F, member 2	NA	4.33232496179988
17	12638055	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.31254753368179
12	13492636	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.30499705045725
12	30916411	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.30392311254625
11	12883792	intron_variant	6	MODIFIER	NA	NA	NA	NA	4.29935142810819
11	32121839	downstream_gene_variant	1	MODIFIER	NA	NA	heme oxygenase (decycling) 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0050896-response to stimulus(L=1),	4.28750835885841
16	16221397	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.28241801436206
12	7842211	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.27433905670219
15	10313235	intron_variant	1	MODIFIER	NA	NA	adenylate kinase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.27096979809927
14	6201779	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.26919012900269
12	32821335	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.25959009944092
13	14313932	downstream_gene_variant	3	MODIFIER	NA	NA	NA	NA	4.25459243347346
14	14103327	synonymous_variant	2	LOW	p.Cys202Cys	SCARF2	scavenger receptor class F, member 2	NA	4.23987539100012
15	23201997	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.22793758294609
17	1767085	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.19746977528734
16	21449975	missense_variant	2	MODERATE	p.Cys42Tyr	NA	NA	NA	4.19736944446458
17	18118004	intron_variant	1	MODIFIER	NA	NA	RhoGEF domain	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.17808384568844
10	7866438	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.16341580984315
15	18956115	missense_variant	1	MODERATE	p.Ser602Arg	NA	NA	NA	4.13971498257015
10	7866261	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.12818798720281
11	12883778	intron_variant	6	MODIFIER	NA	NA	NA	NA	4.12075206256619
18	5912730	intron_variant	1	MODIFIER	NA	NA	Exostosin family	GO:0008152-metabolic process(L=1),	4.11815625386902
11	3838458	upstream_gene_variant	2	MODIFIER	NA	NA	BAH domain and coiled-coil containing 1	NA	4.09738212721715
14	23198759	synonymous_variant	1	LOW	p.Asp53Asp	NA	NA	NA	4.09360628109818
11	29783894	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.09261844960076
17	11361124	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.09144644162202
15	26493620	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.07016575714239
14	22183080	intron_variant	1	MODIFIER	NA	NA	RIMS binding protein 2	NA	4.06563855613121
11	12883797	intron_variant	6	MODIFIER	NA	NA	NA	NA	4.05951655149928
11	12883796	intron_variant	6	MODIFIER	NA	NA	NA	NA	4.05829034647871
13	26296631	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.05617717157757
15	7949175	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.05006330498631
14	21485448	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.04922402136763
17	24229646	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.04805389982103
17	22624337	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.04702223848328
10	305490	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	4.03626933189442
11	2061023	upstream_gene_variant	1	MODIFIER	NA	ASB16	ankyrin repeat and SOCS box containing 16	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	4.02924765350567
13	5910082	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.02719746423217
16	21449974	missense_variant	2	MODERATE	p.Cys42Ser	NA	NA	NA	4.02110236206138
10	28897516	downstream_gene_variant	1	MODIFIER	NA	CSMD2	CUB and Sushi multiple domains 2	NA	4.01914293349237
10	11197294	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.01389102636472
12	29752624	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.00163482924428
18	19585573	intron_variant	1	MODIFIER	NA	ANKRD5	ankyrin repeat domain 5	NA	3.98828737574944
10	305491	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	3.979673452773
13	14313937	downstream_gene_variant	3	MODIFIER	NA	NA	NA	NA	3.97394641431404
10	12733774	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.97241645824779
14	30284889	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.96982627200172
18	20438704	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.95396540703731
17	24229649	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.94040627471127
16	34044751	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.93810607164441
11	29783892	intron_variant	3	MODIFIER	NA	NA	NA	NA	3.93553408615439
11	28198603	missense_variant	1	MODERATE	p.Ser437Thr	ATF7IP	activating transcription factor 7 interacting protein	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	3.93191281112374
13	4773009	missense_variant	1	MODERATE	p.His420Tyr	NA	NA	NA	3.92796529281272
12	23701905	synonymous_variant	1	LOW	p.Gly1260Gly	NA	NA	NA	3.92000720192824
13	18814273	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.91918936670598
12	32415807	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.9083670774244
11	15877087	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.90597188709435
11	15759760	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.89881239749308
18	20867106	upstream_gene_variant	1	MODIFIER	NA	KBTBD11	kelch repeat and BTB (POZ) domain containing 11	NA	3.89624410895187
16	14498185	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.89491774947538
18	15815486	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.89197053068031
13	15427579	intron_variant	1	MODIFIER	NA	NA	multiple C2 domains, transmembrane 1	NA	3.89156426481877
16	16219587	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.88881947691231
13	4849413	synonymous_variant	2	LOW	p.Leu21Leu	NA	NA	NA	3.86030666378758
16	7634197	downstream_gene_variant	1	MODIFIER	NA	NA	Inherit from NOG: negative regulation of cellular process	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	3.8570265277169
12	6128328	synonymous_variant	1	LOW	p.Gly225Gly	NA	Diacylglycerol kinase catalytic domain	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.85317649810102
17	35891471	3_prime_UTR_variant	1	MODIFIER	NA	NA	Hemimethylated DNA-binding protein YccV like	NA	3.84655208138881
14	13215778	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.84145735375915
12	13227473	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.8339477357753
11	12883779	intron_variant	6	MODIFIER	NA	NA	NA	NA	3.83060409800178
13	18814028	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.82904003036504
18	18112602	intron_variant	1	MODIFIER	NA	CDC40	cell division cycle 40 homolog (S. cerevisiae)	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	3.82344930988042
11	13628055	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.82025361776382
15	17272056	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.8195179854291
17	11361118	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.81812281678621
18	26916276	synonymous_variant	1	LOW	p.Leu106Leu	PTCHD1	patched domain containing	GO:0000003-reproduction(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040007-growth(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	3.81253216798377
12	9895272	3_prime_UTR_variant	1	MODIFIER	NA	NA	Retinoic acid receptor gamma	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.8064412366431
15	31817693	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.797856915767
11	22516643	missense_variant	1	MODERATE	p.Ile1316Leu	BRD4	bromodomain containing 4	NA	3.79466547930432
10	1707549	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.79230329638474
17	28304294	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.7918732770249
18	22968011	downstream_gene_variant	1	MODIFIER	NA	KCNK13	Potassium channel, subfamily K, member 13	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.78384449760011
16	8626432	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.78224002496047
10	12898671	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.77710356974235
13	22163517	intron_variant	1	MODIFIER	NA	HIP1R	huntingtin interacting protein 1 related	NA	3.77445667341142
16	29770508	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.76700195348246
11	9559550	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.7557884312258
10	3181040	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.75339310616339
14	20195582	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.75302806475731
14	8569522	downstream_gene_variant	1	MODIFIER	NA	SIPA1	signal-induced proliferation-associated 1	GO:0065007-biological regulation(L=1),	3.74968351830161
10	1707522	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.74661327324693
18	24480565	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.74311901653999
10	13715847	upstream_gene_variant	1	MODIFIER	NA	NA	transmembrane protein 222	NA	3.73469155361632
12	8946481	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.73413276098125
11	31091819	synonymous_variant	1	LOW	p.Tyr83Tyr	NA	NA	NA	3.7279712506272
16	21599576	upstream_gene_variant	1	MODIFIER	NA	ANKRD44	ankyrin repeat domain 44	NA	3.72764431178439
12	10125420	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.71890273487566
16	9068592	upstream_gene_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.7117157663753
13	4849417	missense_variant	2	MODERATE	p.Phe20Ser	NA	NA	NA	3.697707871885
11	24889219	synonymous_variant	1	LOW	p.Ala440Ala	NA	NA	NA	3.6970044478394

chrom	pos	annotation	gene_count	annotation_impact	HGVS.p	EggNOG.Predicted.Gene	EggNOG.Description	EggNOG.GO.Biological	kernaldis
11	10150865	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.25532431308289
18	23112488	intergenic_region	1	MODIFIER	NA	NA	NA	NA	2.97214471377702
17	4029117	intron_variant	1	MODIFIER	NA	NA	NA	NA	2.7279595403682
10	12898671	intergenic_region	1	MODIFIER	NA	NA	NA	NA	2.51218883858903
15	7949175	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.89643389636248
17	19993417	intron_variant	3	MODIFIER	NA	PSMA5	The proteasome is a multicatalytic proteinase complex which is characterized by its ability to cleave peptides with Arg, Phe, Tyr, Leu, and Glu adjacent to the leaving group at neutral or slightly basic pH. The proteasome has an ATP-dependent proteolytic activity (By similarity)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	1.89175287021113
14	20195582	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.87635801878945
14	30284889	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.86569621663423
11	28729796	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.8247151172014
18	139879	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.71389308123588
15	26493620	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.70217478232456
12	6079958	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.70217478232456
15	5968961	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.67192516819586
10	1707522	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.64635742457195
14	20048492	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.61160903911309
15	15532578	intron_variant	2	MODIFIER	NA	NA	ryanodine receptor 3	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	1.59675019503926
13	26593046	intron_variant	1	MODIFIER	NA	GOLGA3	golgin A3	NA	1.57480610235009
14	23739706	intron_variant	1	MODIFIER	NA	ASTN2	astrotactin 2	NA	1.39481051489873
13	8450084	splice_region_variant&intron_variant	2	LOW	NA	NA	NA	NA	1.38793617863935
14	24155219	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.34914061660574
10	305491	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	1.31740579462565
17	42503392	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.30836426602853
17	242580	synonymous_variant	1	LOW	p.Tyr258Tyr	AHCY	adenosylhomocysteinase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	1.29171180823905
15	15322571	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.26133063197017
18	22676836	synonymous_variant	1	LOW	p.Ser267Ser	NA	NA	NA	1.258204975326
17	37214337	upstream_gene_variant	1	MODIFIER	NA	NA	ArfGAP with coiled-coil, ankyrin repeat and PH domains	GO:0065007-biological regulation(L=1),	1.25445186458693
10	14273813	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.23219814723749
12	21456242	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.22543315005349
12	32821335	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.21713690140068
13	948988	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.20824582756944
11	6448715	intron_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	1.20353247180913
15	10552702	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.1792396687492
17	14121865	intergenic_region	2	MODIFIER	NA	NA	NA	NA	1.1698973414985
10	5881514	downstream_gene_variant	1	MODIFIER	NA	NA	cyclin-dependent kinase-like 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	1.15047827664795
16	34193287	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.14645868320679
15	15504289	intron_variant	2	MODIFIER	NA	NA	ryanodine receptor 3	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	1.14645868320679
14	22145505	intron_variant	1	MODIFIER	NA	NA	Inherit from meNOG: Transmembrane protein 132E	NA	1.13901763064319
14	15152363	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.1335659186564
12	32415807	intron_variant	1	MODIFIER	NA	NA	NA	NA	1.12119535606466
12	25255530	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	1.0842666580141
11	12453423	downstream_gene_variant	1	MODIFIER	NA	NA	Homeobox domain	GO:0065007-biological regulation(L=1),	1.08337396213368
11	31749851	synonymous_variant	2	LOW	p.Ala934Ala	NA	NA	NA	1.08337396213368
14	10682718	intron_variant	1	MODIFIER	NA	SBNO1	strawberry notch homolog 1 (Drosophila)	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	1.0496336875249
14	21485448	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.04728799812238
18	22280001	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.04597021196469
10	23081743	intron_variant	1	MODIFIER	NA	ELP2	elongator acetyltransferase complex subunit 2	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	1.04573952004178
13	5939452	upstream_gene_variant	1	MODIFIER	NA	NAA35	N-alpha-acetyltransferase 35, NatC auxiliary subunit	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	1.03632251032609
10	26791711	intron_variant	1	MODIFIER	NA	NA	transmembrane protein 57	NA	1.0359035525906
12	23715215	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	1.03412272485847
12	16754197	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.03222133652648
18	11147084	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.02882302348719
14	6783342	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.01383109613272
17	22469477	intergenic_region	1	MODIFIER	NA	NA	NA	NA	1.00430899434885
13	17887037	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.998440165606836
13	30609276	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.992572426567088
16	217426	intron_variant	2	MODIFIER	NA	NA	Glucagon	NA	0.988905309042672
18	20218603	intron_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	0.988098371567142
10	797157	intergenic_region	2	MODIFIER	NA	NA	NA	NA	0.978902811158025
15	14601031	intron_variant	1	MODIFIER	NA	C14ORF166B	chromosome 14 open reading frame 166B	NA	0.976877113347128
13	22079276	intron_variant	2	MODIFIER	NA	NA	NA	NA	0.956329992805751
14	5245297	missense_variant	1	MODERATE	p.Glu1323Lys	NA	EH domain binding protein 1-like	NA	0.955241117443988
10	28761790	downstream_gene_variant	1	MODIFIER	NA	NA	spire homolog 1 (Drosophila)	GO:0051179-localization(L=1),	0.944760374393854
11	6479402	intron_variant	2	MODIFIER	NA	NA	NA	NA	0.938363787635245
14	18570532	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.931614536166468
11	10380164	intron_variant	1	MODIFIER	NA	TBC1D16	TBC1 domain family, member 16	GO:0065007-biological regulation(L=1),	0.931500100260276
13	22079264	intron_variant	2	MODIFIER	NA	NA	NA	NA	0.929380519019701
13	23079162	synonymous_variant	1	LOW	p.Gln124Gln	NA	V-type ATPase 116kDa subunit family  	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	0.924726377911704
12	17799294	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.919227535375481
12	25255531	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	0.911928552976736
11	11731987	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.910888431046644
11	26781781	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.910697277215213
16	5192887	intron_variant	1	MODIFIER	NA	LRRFIP1	leucine rich repeat (in FLII) interacting protein	NA	0.897666048356858
15	31841642	synonymous_variant	1	LOW	p.Leu50Leu	NA	NA	NA	0.891836266603947
18	21326434	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.891836266603947
16	665473	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.89114354142801
15	26628548	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.886413429283429
13	14378566	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.881433951145818
17	33780681	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.880383230021686
18	25518755	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.879376861480658
11	28412585	upstream_gene_variant	1	MODIFIER	NA	MICALL1	MICAL-like 1	NA	0.876732992661169
13	16767458	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.873255031553717
16	10356378	synonymous_variant	1	LOW	p.Ala193Ala	NA	NA	NA	0.868885784700029
11	8414773	downstream_gene_variant	1	MODIFIER	NA	NA	AAA	GO:0008152-metabolic process(L=1),	0.868885784700029
12	8267044	downstream_gene_variant	1	MODIFIER	NA	NFATC2	nuclear factor of activated T-cells, cytoplasmic, calcineurin-dependent 2	GO:0065007-biological regulation(L=1),	0.864973919562157
12	6128328	synonymous_variant	1	LOW	p.Gly225Gly	NA	Diacylglycerol kinase catalytic domain	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	0.863784985149634
14	27068143	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.863784985149634
17	16865968	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.86128435068621
15	3255324	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.86128435068621
17	19993415	intron_variant	3	MODIFIER	NA	PSMA5	The proteasome is a multicatalytic proteinase complex which is characterized by its ability to cleave peptides with Arg, Phe, Tyr, Leu, and Glu adjacent to the leaving group at neutral or slightly basic pH. The proteasome has an ATP-dependent proteolytic activity (By similarity)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	0.861090342313047
11	24480713	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.861090342313047
16	2572429	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.856713727832281
18	5408823	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.853942284799943
18	12580879	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.853942284799943
12	12205884	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.852946737657701
11	11735297	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.852946737657701
16	23803310	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.848492070115302
13	10240880	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.835796067111179
11	32024265	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.831738944601438
18	27821092	missense_variant	1	MODERATE	p.Gln617Pro	ZNF512	zinc finger protein 512	NA	0.82933225655993
10	5514324	upstream_gene_variant	1	MODIFIER	NA	NA	Cingulin	GO:0008152-metabolic process(L=1),	0.82933225655993
17	16221088	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.828845463409246
18	2986344	intron_variant	1	MODIFIER	NA	NA	fasciculation and elongation protein zeta 2 (zygin II)	NA	0.818529697906849
13	2031874	intron_variant	1	MODIFIER	NA	GOLGA3	golgin A3	NA	0.815874993822363
12	27863290	intron_variant	1	MODIFIER	NA	CELSR3	cadherin, EGF LAG seven-pass G-type receptor 3 (flamingo homolog, Drosophila)	GO:0009987-cellular process(L=1),GO:0022610-biological adhesion(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	0.810773707206449
12	12981706	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.808258447668899
10	12733774	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.801692975756837
10	10133406	upstream_gene_variant	1	MODIFIER	NA	NA	teashirt zinc finger homeobox 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	0.799826457321916
17	22663882	missense_variant	1	MODERATE	p.Glu299Lys	EOS1	IKAROS family zinc finger 4 (Eos)	NA	0.799366448073684
14	2648478	synonymous_variant	1	LOW	p.Leu900Leu	NA	NA	NA	0.794183318889186
13	8450352	intron_variant	2	MODIFIER	NA	NA	NA	NA	0.794183318889186
17	5797173	intron_variant	1	MODIFIER	NA	NA	FYVE, RhoGEF and PH domain containing 5	GO:0065007-biological regulation(L=1),	0.790524752782879
17	14128733	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.785838180588862
15	16077967	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.785838180588862
12	4395672	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.784415342388285
16	14832723	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.78397802049641
15	19132361	downstream_gene_variant	1	MODIFIER	NA	ZDHHC14	zinc finger, DHHC-type containing 14	NA	0.78397802049641
11	31749806	synonymous_variant	2	LOW	p.Thr919Thr	NA	NA	NA	0.779453111607841
12	3407970	downstream_gene_variant	1	MODIFIER	NA	NFYA	nuclear transcription factor Y, alpha	GO:0065007-biological regulation(L=1),	0.773259885446858
12	18725068	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.773259885446858
10	15905477	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.768671188238369
12	11503762	upstream_gene_variant	1	MODIFIER	NA	NA	Formin Homology 2 Domain	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	0.76671047895353
10	305490	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	0.765934095994078
11	13628055	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.765934095994078
13	20187299	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.760711299549924
17	14122083	intergenic_region	2	MODIFIER	NA	NA	NA	NA	0.754828148430136
10	23326008	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.754828148430136
17	19993434	intron_variant	3	MODIFIER	NA	PSMA5	The proteasome is a multicatalytic proteinase complex which is characterized by its ability to cleave peptides with Arg, Phe, Tyr, Leu, and Glu adjacent to the leaving group at neutral or slightly basic pH. The proteasome has an ATP-dependent proteolytic activity (By similarity)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	0.754642641132991
17	40654877	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.754642641132991
18	23995112	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.743400870696121
14	12456701	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.743400870696121
11	19168917	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.743067774765413
16	23475967	synonymous_variant	1	LOW	p.Gln1103Gln	NA	NA	NA	0.735832600659859
10	797135	intergenic_region	2	MODIFIER	NA	NA	NA	NA	0.735832600659859
13	13175005	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.734629549679143
13	20139644	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.726014824025009
11	3061900	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.725921921998589
17	40615965	downstream_gene_variant	1	MODIFIER	NA	APCDD1L	adenomatosis polyposis coli down-regulated 1-like	NA	0.721626177241534
17	4005273	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.720302001759554
13	5714492	intron_variant	1	MODIFIER	NA	FANCC	Fanconi anemia, complementation group C	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),	0.715539518642158
14	16208315	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.711186980938946
11	35650182	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.710583593139486
10	11656423	intron_variant	1	MODIFIER	NA	KIAA0895	kiaa0895	NA	0.708255662218138
13	4378110	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.706218342767465
11	30036570	missense_variant	1	MODERATE	p.Asn1042Ser	NA	NA	NA	0.704038489851234
17	24043083	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.703506786962798
11	15243795	synonymous_variant	1	LOW	p.Leu323Leu	NA	NA	NA	0.703506786962798
18	21117544	intron_variant	1	MODIFIER	NA	PLB1	GDSL-like Lipase/Acylhydrolase	GO:0008152-metabolic process(L=1),	0.702072407685888
16	27687374	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.701320435077176
15	32116890	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.700296910390667
11	29783984	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.700064900483064
15	22204176	upstream_gene_variant	1	MODIFIER	NA	ARID1B	Domain of unknown function (DUF3518)	NA	0.699855785853255
10	23603734	downstream_gene_variant	1	MODIFIER	NA	EVX1	Even-skipped homeobox 1	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	0.699282927849242
12	28029931	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.698709298272832
11	6479372	intron_variant	2	MODIFIER	NA	NA	NA	NA	0.69643331775607
16	217364	intron_variant	2	MODIFIER	NA	NA	Glucagon	NA	0.696190482513719
18	21346483	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.696190482513719
12	26750313	intron_variant	1	MODIFIER	NA	CCDC30	Inherit from veNOG: coiled-coil domain containing 30	NA	0.695906642981013
16	31336809	3_prime_UTR_variant	1	MODIFIER	NA	NA	NA	NA	0.695449589453314
17	18118004	intron_variant	1	MODIFIER	NA	NA	RhoGEF domain	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	0.694831458800703
18	14694490	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.694831458800703
15	31817703	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.694652639603679
17	3379961	intron_variant	1	MODIFIER	NA	NA	ZnF_C2H2	NA	0.69140786337443
11	29574045	synonymous_variant	1	LOW	p.Leu879Leu	NA	NA	NA	0.69082329730579
16	10031734	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	0.689817333714646
13	15427645	synonymous_variant	1	LOW	p.Ile614Ile	NA	multiple C2 domains, transmembrane 1	NA	0.683355975907052
15	1004716	intron_variant	1	MODIFIER	NA	NA	NA	NA	0.683355975907052
12	11593193	missense_variant	1	MODERATE	p.Ile490Val	PLEKHG5	pleckstrin homology domain containing, family G (with RhoGef domain) member 5	GO:0065007-biological regulation(L=1),	0.68328368773107
12	28884348	downstream_gene_variant	1	MODIFIER	NA	SNX21	sorting nexin family member 21	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	0.682688915424668
17	39326230	intergenic_region	1	MODIFIER	NA	NA	NA	NA	0.681827763998425

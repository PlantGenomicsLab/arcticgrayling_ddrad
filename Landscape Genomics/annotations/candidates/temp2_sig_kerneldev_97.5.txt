chrom	pos	annotation	gene_count	annotation_impact	HGVS.p	EggNOG.Predicted.Gene	EggNOG.Description	EggNOG.GO.Biological	kerneldev
5	21004791	intergenic_region	1	MODIFIER	NA	NA	NA	NA	85.4401798118931
19	13284660	intron_variant	1	MODIFIER	NA	NA	NA	NA	61.0503829816022
1	16444557	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	60.0237222821676
1	2465649	intron_variant	1	MODIFIER	NA	NA	NA	NA	55.2487527439058
8	22498258	missense_variant	3	MODERATE	p.Ala588Thr	NA	NA	NA	53.6826501181442
23	4471951	missense_variant	1	MODERATE	p.Gly263Arg	E2F7	E2F transcription factor 7	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0060707-trophoblast giant cell differentiation(L=10),GO:0065007-biological regulation(L=1),	43.6126800754757
NA	44977	intergenic_region	9	MODIFIER	NA	NA	NA	NA	43.584307217214
1	24058373	intron_variant	1	MODIFIER	NA	NA	NA	NA	42.3385482200107
6	33153917	missense_variant	1	MODERATE	p.Met696Thr	NA	NA	NA	39.1351505502665
21	2471639	intergenic_region	9	MODIFIER	NA	NA	NA	NA	36.2546684251169
12	27016164	intergenic_region	1	MODIFIER	NA	NA	NA	NA	34.826315416221
6	10107254	intron_variant	1	MODIFIER	NA	NA	NA	NA	33.1841709253511
20	25040361	intergenic_region	1	MODIFIER	NA	NA	NA	NA	32.8409159838766
7	30995960	upstream_gene_variant	1	MODIFIER	NA		ryanodine receptor 1 (skeletal)	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	32.7285448820499
5	19555035	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	31.8305219940103
4	29824527	intergenic_region	1	MODIFIER	NA	NA	NA	NA	31.7250742560804
13	25368882	downstream_gene_variant	1	MODIFIER	NA	ZNF862	Zinc finger protein 862	GO:0065007-biological regulation(L=1),	31.0001576805636
17	18249238	intergenic_region	1	MODIFIER	NA	NA	NA	NA	30.7594998648162
2	14716067	intergenic_region	1	MODIFIER	NA	NA	NA	NA	30.4402550118715
15	29501655	intergenic_region	1	MODIFIER	NA	NA	NA	NA	28.2832791374646
24	5353105	intron_variant	2	MODIFIER	NA	NA	NA	NA	28.2601284646942
4	23515749	intergenic_region	1	MODIFIER	NA	NA	NA	NA	28.2295247517017
NA	183	intergenic_region	9	MODIFIER	NA	NA	NA	NA	28.2043527069884
15	9221905	upstream_gene_variant	1	MODIFIER	NA	NPAS3	neuronal PAS domain protein	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	28.0299146503953
17	41066807	intron_variant	1	MODIFIER	NA	NA	NA	NA	27.5912234356646
24	5353088	intron_variant	2	MODIFIER	NA	FAM163A	family with sequence similarity 163, member A		27.0995747760535
18	24619926	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	26.9837683203266
2	17552677	intron_variant	1	MODIFIER	NA	NA	NA	NA	26.9340144020442
10	19381303	intron_variant	1	MODIFIER	NA	NA	NA	NA	26.3684930859981
12	7842211	intergenic_region	1	MODIFIER	NA	NA	NA	NA	26.3311613560048
3	5234040	intron_variant	1	MODIFIER	NA	NA	NA	NA	26.0651444328244
3	23473753	intergenic_region	1	MODIFIER	NA	NA	NA	NA	25.9033598442427
11	10496502	intergenic_region	1	MODIFIER	NA	NA	NA	NA	25.8968067250058
18	22769890	intron_variant	1	MODIFIER	NA	NA	NA	NA	25.6741944398084
11	12453417	downstream_gene_variant	2	MODIFIER	NA		Homeobox domain	GO:0065007-biological regulation(L=1),	25.5989745142204
16	25663185	intergenic_region	1	MODIFIER	NA	NA	NA	NA	25.3675382934856
5	23586692	upstream_gene_variant	1	MODIFIER	NA	ANKRD1	ankyrin repeat domain 1 (cardiac muscle)		24.8819495287847
23	8633048	3_prime_UTR_variant	1	MODIFIER	NA	NA	NA	NA	24.7655913776545
5	8485830	intron_variant	1	MODIFIER	NA	NA	NA	NA	24.6319891812366
16	9068592	upstream_gene_variant	1	MODIFIER	NA		Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	24.6088679388784
11	15243795	synonymous_variant	1	LOW	p.Leu323Leu	NA	NA	NA	24.594249547709
4	4720615	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	24.5852455931191
21	6840541	intergenic_region	9	MODIFIER	NA	NA	NA	NA	24.5095732619612
25	4756925	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	24.5087490257351
14	20195582	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	24.3491771803956
13	10240880	intergenic_region	1	MODIFIER	NA	NA	NA	NA	24.2086654850307
11	12453433	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	24.1260536250276
14	6201779	intron_variant	1	MODIFIER	NA	NA	NA	NA	23.8965461963682
17	1767085	intergenic_region	1	MODIFIER	NA	NA	NA	NA	23.6044378297735
13	26207805	intergenic_region	1	MODIFIER	NA	NA	NA	NA	23.5861617127943
8	22498303	missense_variant	3	MODERATE	p.Gln603Lys	NA	NA	NA	23.540099460046
8	12249575	intron_variant	1	MODIFIER	NA		ankyrin repeat domain 13C		23.4998341031467
NA	17919	intergenic_region	9	MODIFIER	NA	NA	NA	NA	23.3971105389518
18	24619960	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	23.3757535725799
20	747636	missense_variant	1	MODERATE	p.Leu511Met	PHF20L1	PHD finger protein 20-like 1		23.3660474685309
7	9657916	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	23.2720648620234
23	20411531	intron_variant	1	MODIFIER	NA	TTLL12	tubulin tyrosine ligase-like family, member 12	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	23.091117544583
11	10150898	intergenic_region	1	MODIFIER	NA	NA	NA	NA	23.0560151320831
3	12110774	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.9553098149815
3	7229084	downstream_gene_variant	1	MODIFIER	NA		Sema	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	22.9059377370213
13	13476882	intron_variant	1	MODIFIER	NA	GRID2	Glutamate receptor, ionotropic, delta 2	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	22.7350671497693
5	10393714	intron_variant	1	MODIFIER	NA	MYO1D	myosin ID	GO:0008152-metabolic process(L=1),	22.7071529795516
1	16520334	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.4856617645236
6	3488176	5_prime_UTR_variant	1	MODIFIER	NA	NA	NA	NA	22.4337547387824
9	7206632	missense_variant	2	MODERATE	p.Asn24Ser	NA	NA	NA	22.4290787017019
17	7991787	intron_variant	1	MODIFIER	NA	NA	NA	NA	22.3870457150362
7	13934734	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.1765758245027
3	29964204	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.0461567262082
7	24903597	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	22.0035040347473
8	21737938	missense_variant	1	MODERATE	p.Val102Leu	NA	NA	NA	21.9373864838674
12	8946481	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.8603792148017
3	12565155	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.6849019264943
25	3173440	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.5576192351156
15	11684435	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.5266344755737
1	3796970	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.4724467155992
3	15785946	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.4104251804496
6	6429712	synonymous_variant	1	LOW	p.Phe79Phe	NA	NA	NA	21.3419843611809
9	3449511	intron_variant	1	MODIFIER	NA	NA	NA	NA	21.2733646915926
11	13872232	intron_variant	1	MODIFIER	NA	NA	NA	NA	21.0923232479515
1	26623936	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.0017886215199
24	11484057	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.9566273342247
10	13186047	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.8728352596638
21	4250650	intergenic_region	9	MODIFIER	NA	NA	NA	NA	20.8660842341061
16	12831367	intron_variant	1	MODIFIER	NA	GLRA2	Neurotransmitter-gated ion-channel ligand binding domain	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	20.8030194384474
8	22699047	intron_variant	1	MODIFIER	NA	NEK7	NIMA-related kinase 7	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	20.7657013103917
9	4225634	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.694805566955
10	3181391	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.6192111687298
8	22498246	missense_variant	3	MODERATE	p.Lys584Glu	NA	NA	NA	20.5870241290541
4	27536984	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.5731184611251
15	12825272	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.5474836139832
7	10892379	synonymous_variant	1	LOW	p.Asp1543Asp	ANKHD1	ankyrin repeat and KH domain containing 1	GO:0032501-multicellular organismal process(L=1),GO:0044699-single-organism process(L=1),	20.5005706847214
14	24030399	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.4607038607328
19	1099673	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	20.4010980484495
17	19993415	intron_variant	1	MODIFIER	NA	PSMA5	The proteasome is a multicatalytic proteinase complex which is characterized by its ability to cleave peptides with Arg, Phe, Tyr, Leu, and Glu adjacent to the leaving group at neutral or slightly basic pH. The proteasome has an ATP-dependent proteolytic activity (By similarity)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	20.3541335732779
3	26104525	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.3192015776123
9	3733853	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.3109547952141
18	20218603	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.3108730307855
19	7124027	missense_variant	1	MODERATE	p.Thr132Ala	NA	NA	NA	20.3105460836287
17	33581780	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.2878172224796
18	139879	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	20.2824332790407
20	15444451	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.2787960754759
2	8873167	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.2706191135865
7	28683579	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.2507689369826
16	15266327	intron_variant	1	MODIFIER	NA	STXBP5L	syntaxin binding protein 5-like	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	20.1508250934674
3	1210591	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.0818217139898
14	3646234	upstream_gene_variant	1	MODIFIER	NA				20.0339990219145
14	31687760	intron_variant	1	MODIFIER	NA	LAMC3	laminin, gamma 3		19.9207993918117
1	13617905	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.792945770025
6	10201892	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.6943420239193
20	18517220	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.6386846077364
12	25255530	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	19.6330916469039
16	7615309	splice_region_variant&intron_variant	1	LOW	NA	NA	NA	NA	19.5972184684522
6	33524110	synonymous_variant	1	LOW	p.Ala282Ala	NA	NA	NA	19.5621696339097
11	35537194	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.5377550983534
20	16483951	intron_variant	1	MODIFIER	NA	ELP6	elongator acetyltransferase complex subunit 6	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	19.5330475479366
10	5514324	upstream_gene_variant	1	MODIFIER	NA		Cingulin	GO:0008152-metabolic process(L=1),	19.5072795177878
6	36898380	intron_variant	1	MODIFIER	NA		SH3 and PX domains 2A	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	19.4966343667271
18	17712895	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.4449311884449
6	3659611	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.3918065676176
21	1684616	intergenic_region	9	MODIFIER	NA	NA	NA	NA	19.3616984726409
4	23572452	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.359550701572
14	8569521	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.343491254402
3	30235544	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.3316421627539
7	29533995	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.3237502400295
3	7347169	intron_variant	1	MODIFIER	NA	SIRT6	sirtuin 6		19.3060659098118
13	33746498	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.2951485125927
3	19070825	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.2257679164205
1	24879505	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.2016532435034
12	25255531	intron_variant	2	MODIFIER	NA	NA	NA	NA	19.1688446589052
18	26916276	synonymous_variant	1	LOW	p.Leu106Leu	PTCHD1	patched domain containing	GO:0000003-reproduction(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040007-growth(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	19.0445576796827
3	20851384	synonymous_variant	1	LOW	p.Lys117Lys	MARCH6	membrane-associated ring finger (C3HC4) 6, E3 ubiquitin protein ligase		18.9989969433772
13	8290378	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.9629830692405
3	24883116	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.9544552610256
9	7206661	synonymous_variant	2	LOW	p.Thr14Thr	NA	NA	NA	18.9469087495173
11	16827070	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	18.9468619155227
NA	19422	intergenic_region	9	MODIFIER	NA	NA	NA	NA	18.9428492189483
7	9259751	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.940368733285
9	5880083	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	18.9131753481206
12	3420448	downstream_gene_variant	1	MODIFIER	NA				18.8339924802197
21	18446994	intergenic_region	9	MODIFIER	NA	NA	NA	NA	18.8283005873047
24	5331614	synonymous_variant	1	LOW	p.Thr663Thr	NA	NA	NA	18.8094466194771
16	31887015	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.7951252276415
11	13628055	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.7871500922198
23	23019209	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.7240190054256
1	22970073	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.6689671202333
2	39463645	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	18.6187429278135

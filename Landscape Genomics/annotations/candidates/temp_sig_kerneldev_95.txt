chrom	pos	annotation	gene_count	annotation_impact	HGVS.p	EggNOG.Predicted.Gene	EggNOG.Description	EggNOG.GO.Biological	kerneldev
17	15124396	intergenic_region	1	MODIFIER	NA	NA	NA	NA	87.3199907319972
11	10496502	intergenic_region	1	MODIFIER	NA	NA	NA	NA	80.1802331790471
25	3767473	intron_variant	1	MODIFIER	NA	WDR17	WD repeat domain 17	NA	73.0944091371475
16	3825459	intergenic_region	1	MODIFIER	NA	NA	NA	NA	58.0428515030949
13	23163328	upstream_gene_variant	1	MODIFIER	NA	LIMK2	LIM domain kinase 2	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	54.9465578658154
4	23161472	intergenic_region	1	MODIFIER	NA	NA	NA	NA	54.0436266412762
3	12110781	intergenic_region	2	MODIFIER	NA	NA	NA	NA	52.0118293497503
11	12453433	downstream_gene_variant	3	MODIFIER	NA	NA	NA	NA	46.4786929692482
NA	18946	intergenic_region	16	MODIFIER	NA	NA	NA	NA	45.1567643910655
17	2711745	upstream_gene_variant	1	MODIFIER	NA	PTPRT	protein tyrosine phosphatase, receptor type, T	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	39.5042363077313
16	34431653	intron_variant	1	MODIFIER	NA	NA	NA	NA	36.8625366772431
14	30529819	intergenic_region	1	MODIFIER	NA	NA	NA	NA	36.6892243198949
10	25408974	intron_variant	2	MODIFIER	NA	NA	Amino acid permease	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	34.888168996005
8	36408013	upstream_gene_variant	2	MODIFIER	NA	MMACHC	methylmalonic aciduria (cobalamin deficiency) cblC type, with homocystinuria	NA	34.3522222257654
3	12110774	intergenic_region	2	MODIFIER	NA	NA	NA	NA	33.5744569500237
11	12453423	downstream_gene_variant	3	MODIFIER	NA	NA	NA	NA	33.5629740165343
17	17621648	intron_variant	1	MODIFIER	NA	NA	RAP1 GTPase activating protein	GO:0065007-biological regulation(L=1),	33.4481909739291
11	12453417	downstream_gene_variant	3	MODIFIER	NA	NA	Homeobox domain	GO:0065007-biological regulation(L=1),	33.2488259857704
16	25663185	intergenic_region	1	MODIFIER	NA	NA	NA	NA	33.2400845695059
16	13855168	missense_variant	1	MODERATE	p.Ala275Gly	IFNAR1	interferon (alpha, beta and omega) receptor 1	GO:0002376-immune system process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	32.5759362886132
19	7091927	upstream_gene_variant	1	MODIFIER	NA	ST7	Suppression of tumorigenicity 7	GO:0000003-reproduction(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040007-growth(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	31.0829379541509
3	17239176	intergenic_region	2	MODIFIER	NA	NA	NA	NA	31.0577229060174
13	5638740	missense_variant	1	MODERATE	p.Arg145Cys	KIF24	kinesin family member 24	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	30.7957196548268
3	6534825	downstream_gene_variant	1	MODIFIER	NA	NDUFA13	NADH dehydrogenase (ubiquinone) 1 alpha subcomplex, 13	NA	30.5102647227935
13	12341941	intergenic_region	1	MODIFIER	NA	NA	NA	NA	29.7277565249385
10	1707522	intergenic_region	2	MODIFIER	NA	NA	NA	NA	29.6513009361603
21	20029212	intergenic_region	16	MODIFIER	NA	NA	NA	NA	29.5637477666901
5	7404675	downstream_gene_variant	1	MODIFIER	NA	ETV4	ets variant 4	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	28.828367453866
8	36408014	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	28.7870671291948
4	29824527	intergenic_region	1	MODIFIER	NA	NA	NA	NA	28.4549250271639
10	1707549	intergenic_region	2	MODIFIER	NA	NA	NA	NA	28.2223204928279
NA	17919	intergenic_region	16	MODIFIER	NA	NA	NA	NA	27.3034007348019
12	7431878	intergenic_region	1	MODIFIER	NA	NA	NA	NA	27.0830723259205
7	29533995	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	26.9365587865399
10	14258537	intron_variant	1	MODIFIER	NA	MANEAL	mannosidase, endo-alpha-like	GO:0008152-metabolic process(L=1),	26.6191481606002
8	22498258	missense_variant	3	MODERATE	p.Ala588Thr	NA	NA	NA	26.4995799160625
11	20074062	intron_variant	2	MODIFIER	NA	ARHGAP23	Rho GTPase activating protein 23	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	26.4509628509286
19	23936380	intergenic_region	1	MODIFIER	NA	NA	NA	NA	26.3161408624436
15	16749374	downstream_gene_variant	1	MODIFIER	NA	NA	v-myc myelocytomatosis viral oncogene homolog 1, lung carcinoma derived (avian)	GO:0065007-biological regulation(L=1),	26.1716618910645
8	1573208	intron_variant	1	MODIFIER	NA	NA	NA	NA	25.9881070974725
17	41066807	intron_variant	2	MODIFIER	NA	NA	NA	NA	25.6525668310337
NA	361852	intergenic_region	16	MODIFIER	NA	NA	NA	NA	25.5301577726688
11	26055082	upstream_gene_variant	1	MODIFIER	NA	RHBDF1	rhomboid 5 homolog 1 (Drosophila)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	25.3995630990439
8	11765791	intron_variant	1	MODIFIER	NA	DPP9	dipeptidylpeptidase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	25.2301087121763
24	24455259	intergenic_region	1	MODIFIER	NA	NA	NA	NA	25.0862750683194
18	19585573	intron_variant	1	MODIFIER	NA	ANKRD5	ankyrin repeat domain 5	NA	24.9141229147941
1	4601122	downstream_gene_variant	1	MODIFIER	NA	NA	Glutamate receptor, ionotropic, AMPA	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	24.309031720286
6	10201892	intron_variant	1	MODIFIER	NA	NA	NA	NA	24.3080357513059
14	31699111	intron_variant	1	MODIFIER	NA	LAMC3	laminin, gamma 3	NA	24.1512439416544
12	32821335	intergenic_region	1	MODIFIER	NA	NA	NA	NA	24.1222354587798
10	12733774	intron_variant	1	MODIFIER	NA	NA	protein tyrosine phosphatase, receptor type, U	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	24.0888194561517
6	10371955	intron_variant	2	MODIFIER	NA	NA	NA	NA	24.0687322449211
6	34307040	intron_variant	1	MODIFIER	NA	ZSWIM8	zinc finger, SWIM-type containing 8	NA	24.0541339629041
6	25501948	intron_variant	1	MODIFIER	NA	NA	NA	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	23.9233161034261
10	7901729	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	23.8331616019237
5	23284860	upstream_gene_variant	1	MODIFIER	NA	SRBD1	S1 RNA binding domain 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	23.7874720955953
1	15971911	intron_variant	1	MODIFIER	NA	TRIP12	thyroid hormone receptor interactor 12	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),GO:1900044-regulation of protein K63-linked ubiquitination(L=11),GO:1900045-negative regulation of protein K63-linked ubiquitination(L=11),GO:1901314-regulation of histone H2A K63-linked ubiquitination(L=10),GO:1901315-negative regulation of histone H2A K63-linked ubiquitination(L=10),	23.7414066472716
11	15243795	synonymous_variant	1	LOW	p.Leu323Leu	STXBP2	Syntaxin binding protein 2	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	23.6038157247252
12	17799405	intron_variant	3	MODIFIER	NA	NA	NA	NA	23.5162821984592
2	8873167	intergenic_region	1	MODIFIER	NA	NA	NA	NA	23.4839111098005
20	15444451	intron_variant	1	MODIFIER	NA	SLC39A1	solute carrier family 39 (zinc transporter), member 1	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),	23.0227393296569
24	5872993	intron_variant	1	MODIFIER	NA	NA	Membrane protein, palmitoylated 6 (MAGUK p55 subfamily member 6)	NA	23.0190941102668
5	17887774	3_prime_UTR_variant	2	MODIFIER	NA	NA	NA	NA	22.8848064570013
9	6580912	upstream_gene_variant	1	MODIFIER	NA	NA	Zinc-finger double-stranded RNA-binding	NA	22.8530475341375
11	15877087	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.8494998579116
23	17884875	intron_variant	1	MODIFIER	NA	NA	NA	NA	22.8103134825969
13	14313935	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	22.7744200073375
3	9417198	downstream_gene_variant	1	MODIFIER	NA	RX2	retina and anterior neural fold homeobox	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	22.7093429649124
14	23147360	upstream_gene_variant	1	MODIFIER	NA	RGS3	Regulator of G-protein signaling	GO:0000188-inactivation of MAPK activity(L=11),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	22.6638349394997
10	5514324	upstream_gene_variant	1	MODIFIER	NA	NA	Cingulin	GO:0008152-metabolic process(L=1),	22.6326023870986
24	1010943	intron_variant	2	MODIFIER	NA	NA	NA	NA	22.5556728364775
16	36872513	intergenic_region	3	MODIFIER	NA	NA	NA	NA	22.5278258316808
11	16827070	upstream_gene_variant	1	MODIFIER	NA	NPM2	Nucleophosmin nucleoplasmin 2	GO:0000003-reproduction(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	22.3707421604469
14	11227353	upstream_gene_variant	1	MODIFIER	NA	HIC2	hypermethylated in cancer	NA	22.3412381675394
16	35401530	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.321051895619
15	14628256	upstream_gene_variant	1	MODIFIER	NA	FCF1	FCF1 small subunit (SSU) processome component homolog (S. cerevisiae)	NA	22.2924302615472
7	28683579	intron_variant	1	MODIFIER	NA	NA	NA	NA	22.2848432027795
18	24205947	upstream_gene_variant	1	MODIFIER	NA	NA	Inherit from opiNOG: Transposable element tc1 transposase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	22.2538601191251
8	27398259	intergenic_region	1	MODIFIER	NA	NA	NA	NA	22.1902800564136
8	8655161	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	22.1068543677922
17	2551989	upstream_gene_variant	1	MODIFIER	NA	PTPRT	protein tyrosine phosphatase, receptor type, T	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	22.0672802941874
21	20425541	intergenic_region	16	MODIFIER	NA	NA	NA	NA	22.0212559671716
13	13662466	intron_variant	1	MODIFIER	NA	ENTPD2	ectonucleoside triphosphate diphosphohydrolase 2	GO:0008152-metabolic process(L=1),GO:0050896-response to stimulus(L=1),	22.0131653972008
21	25151807	intergenic_region	16	MODIFIER	NA	NA	NA	NA	22.0012963105747
9	5880083	upstream_gene_variant	1	MODIFIER	NA	DDX5	DEAD (Asp-Glu-Ala-Asp) box helicase 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	21.9874383204926
24	10698277	intron_variant	2	MODIFIER	NA	NA	Phorbol esters/diacylglycerol binding domain (C1 domain)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	21.9803382156846
14	21485448	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.9755267686431
16	35745605	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.9535412176918
2	13946383	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.9032991579911
15	19682385	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.693806287254
24	5599831	downstream_gene_variant	1	MODIFIER	NA	KLF5	Kruppel-like factor 5 (intestinal)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	21.5858896974847
18	15545260	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.5659966075022
7	9657916	upstream_gene_variant	1	MODIFIER	NA	NA	D1 dopamine receptor-interacting protein (calcyon)	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	21.550464319331
25	1918077	downstream_gene_variant	1	MODIFIER	NA	NA	Zinc finger, C2H2 type	NA	21.5295640980544
1	17845777	downstream_gene_variant	1	MODIFIER	NA	GRAMD1B	GRAM domain containing 1B	NA	21.5009266755574
1	18619292	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.4611352116604
25	8099854	missense_variant	1	MODERATE	p.Glu172Val	TBC1D2	TBC1 domain family, member 2	GO:0065007-biological regulation(L=1),	21.4578103283694
6	2447599	synonymous_variant	1	LOW	p.Val235Val	NA	centrosomal protein 170kDa	NA	21.449777075242
23	13102653	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	21.42860162099
1	5537767	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.4230230732375
7	20400605	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.4071046832714
21	16720838	intergenic_region	16	MODIFIER	NA	NA	NA	NA	21.3858431089699
15	23036311	3_prime_UTR_variant	1	MODIFIER	NA	TRAPPC12	trafficking protein particle complex 12	NA	21.3798938511845
13	5939452	upstream_gene_variant	1	MODIFIER	NA	NAA35	N-alpha-acetyltransferase 35, NatC auxiliary subunit	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	21.3526792778312
21	15812129	intergenic_region	16	MODIFIER	NA	NA	NA	NA	21.3042304006036
21	4942604	intergenic_region	16	MODIFIER	NA	NA	NA	NA	21.2932603938438
15	12242492	intron_variant	1	MODIFIER	NA	NA	Nuclear envelope localisation domain	NA	21.1376083050379
13	26207805	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.0410767800671
8	21764082	intergenic_region	1	MODIFIER	NA	NA	NA	NA	21.0354544370354
3	6209942	splice_region_variant&intron_variant	1	LOW	NA	NA	Inherit from veNOG: CDK5 regulatory subunit associated protein 2	NA	20.9947907759753
5	17887773	3_prime_UTR_variant	2	MODIFIER	NA	NA	phospholipase C, delta 3	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	20.8385148095509
3	36379120	missense_variant	1	MODERATE	p.Thr1489Met	NA	Domain of Unknown Function (DUF1081)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	20.8254852655018
14	14103327	synonymous_variant	1	LOW	p.Cys202Cys	NA	NA	NA	20.7949408909836
4	25624854	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.7046098491561
8	22498246	missense_variant	3	MODERATE	p.Lys584Glu	NA	Chromosome undetermined SCAF14724, whole genome shotgun sequence	NA	20.6782396907456
18	9653055	upstream_gene_variant	1	MODIFIER	NA	EPHA7	EPH receptor A7	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	20.6231974866017
1	11158646	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.6199980717618
14	8632755	upstream_gene_variant	1	MODIFIER	NA	KAT5	K(lysine) acetyltransferase 5	GO:0008152-metabolic process(L=1),GO:0065007-biological regulation(L=1),	20.6027516474415
24	12051830	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.5844495897266
12	1678940	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	20.5701821203852
11	29445015	upstream_gene_variant	1	MODIFIER	NA	LRRC53	LRR_TYP	NA	20.560756783299
5	1692473	intron_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	20.5321093763701
24	5914345	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	20.4826481325388
23	782299	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.4083112497162
18	26916276	synonymous_variant	1	LOW	p.Leu106Leu	PTCHD1	patched domain containing	GO:0000003-reproduction(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040007-growth(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	20.3761727497335
23	7116926	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.3540544755379
11	17734456	intron_variant	1	MODIFIER	NA	NA	Inherit from veNOG: shisa homolog 6 (Xenopus laevis)	NA	20.296772830165
3	17239170	intergenic_region	2	MODIFIER	NA	NA	NA	NA	20.2835622776805
12	9126614	5_prime_UTR_variant	1	MODIFIER	NA	HIPK1	homeodomain interacting protein kinase 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	20.2283617858829
15	12212367	3_prime_UTR_variant	2	MODIFIER	NA	NA	NA	NA	20.2254956668703
11	22516643	missense_variant	1	MODERATE	p.Ile1316Leu	NA	NA	NA	20.2006980168937
1	5043383	intron_variant	1	MODIFIER	NA	CTAGE5	CTAGE family, member 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	20.1283150050014
25	5112250	intron_variant	1	MODIFIER	NA	NA	NA	NA	20.1267798464089
17	6756782	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	20.1031057385904
7	25080330	intergenic_region	1	MODIFIER	NA	NA	NA	NA	20.086979569294
9	3449511	intron_variant	3	MODIFIER	NA	NA	NA	NA	20.0775207136556
21	21333971	intergenic_region	16	MODIFIER	NA	NA	NA	NA	20.0370950085886
9	3449557	intron_variant	3	MODIFIER	NA	NA	NA	NA	20.0264254498852
21	21333593	intergenic_region	16	MODIFIER	NA	NA	NA	NA	20.0174568177202
9	3449504	intron_variant	3	MODIFIER	NA	NA	neuralized homolog (Drosophila)	NA	19.9894511579867
12	23532954	intron_variant	2	MODIFIER	NA	NA	NA	NA	19.9082623123788
19	6179376	upstream_gene_variant	1	MODIFIER	NA	CEP290	centrosomal protein 290kDa	GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	19.9001428256156
8	36407482	synonymous_variant	1	LOW	p.Asp244Asp	NA	NA	NA	19.8931129083383
11	10150898	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.8872505641555
18	12448773	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.8553572067555
4	14511151	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.7949853483009
12	17799404	intron_variant	3	MODIFIER	NA	NA	NA	NA	19.7480384003575
16	16219587	intron_variant	1	MODIFIER	NA	NA	mitogen-activated protein kinase kinase kinase kinase 4	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	19.6794522571944
NA	301	intergenic_region	16	MODIFIER	NA	NA	NA	NA	19.674695812167
17	38549216	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.6661824076939
8	27573029	missense_variant	1	MODERATE	p.Gln481His	NA	aldehyde dehydrogenase 9 family, member	GO:0008152-metabolic process(L=1),	19.6629003629563
19	5275461	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.6575819374056
16	5011635	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.62650680486
8	6536919	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.6156637799447
17	33750234	intron_variant	1	MODIFIER	NA	NA	macrophage stimulating 1 receptor (c-met-related tyrosine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	19.6080561625783
5	9224071	intron_variant	1	MODIFIER	NA	PEMT	phosphatidylethanolamine N-methyltransferase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	19.5769877845921
12	7842211	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.4686792221012
15	5969142	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.4663422024462
10	25409057	synonymous_variant	2	LOW	p.Gly351Gly	NA	NA	NA	19.4515915581763
14	18570532	intergenic_region	1	MODIFIER	NA	NA	NA	NA	19.3748843579913
9	6580837	synonymous_variant	1	LOW	p.Ser122Ser	ZFYVE27	zinc finger, FYVE domain containing 27	NA	19.3150095043326
1	16444557	downstream_gene_variant	1	MODIFIER	NA	MECOM	MDS1 and EVI1 complex locus	NA	19.3044808023182
9	4839284	missense_variant	1	MODERATE	p.Leu14Phe	ZBTB45	zinc finger and BTB domain containing 45	NA	19.2929991012873
15	26906150	upstream_gene_variant	1	MODIFIER	NA	NA	SCAN domain	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0051704-multi-organism process(L=1),GO:0065007-biological regulation(L=1),	19.2911454124802
2	19076380	5_prime_UTR_variant	1	MODIFIER	NA	NA	NA	NA	19.2506917522884
25	2133974	intron_variant	3	MODIFIER	NA	NA	NA	NA	19.2487115953902
25	482121	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.2477577896379
10	906849	synonymous_variant	1	LOW	p.Ala477Ala	EXTL1	exostoses (multiple)-like 1	GO:0008152-metabolic process(L=1),	19.2463633100864
2	17584367	intergenic_region	2	MODIFIER	NA	NA	NA	NA	19.1882544445204
18	6167902	intron_variant	1	MODIFIER	NA	MLLT4	myeloid lymphoid or mixed-lineage leukemia (trithorax homolog, Drosophila)	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	19.1845316690939
5	7119732	intron_variant	1	MODIFIER	NA	PDE6C	Phosphodiesterase 6C, cGMP-specific, cone, alpha prime	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	19.1741179574665
14	3664180	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	19.1543435657499
2	11221478	intron_variant	1	MODIFIER	NA	LRRK1	leucine-rich repeat kinase 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	19.1223210330885
11	7482499	intron_variant	1	MODIFIER	NA	NA	NA	NA	19.1094384557824
14	24887462	downstream_gene_variant	1	MODIFIER	NA	NA	Inherit from veNOG: Proline rich 14-like	NA	19.1072021500682
6	33153917	missense_variant	1	MODERATE	p.Met696Thr	NA	NA	NA	19.1044742726711
25	4756925	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	19.0715716090077
17	37406722	synonymous_variant	1	LOW	p.Val42Val	DDIT3	DNA-damage-inducible transcript 3	GO:0065007-biological regulation(L=1),	19.0340300105298
17	13671934	intron_variant	1	MODIFIER	NA	PIK3C2B	phosphatidylinositol-4-phosphate 3-kinase, catalytic subunit type 2 beta	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	19.0080788989645
4	27537003	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.959271791249
2	760187	intron_variant	1	MODIFIER	NA	TSC1	tuberous sclerosis 1	NA	18.9143997522472
3	4544007	synonymous_variant	1	LOW	p.His277His	SLC1A7	solute carrier family 1 (glutamate transporter), member	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	18.8936163479614
18	11865273	upstream_gene_variant	1	MODIFIER	NA	NA	Akirin 2	NA	18.8758290612201
6	7750498	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.869445980769
17	24229649	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.7530353519018
25	4756920	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	18.7497141071255
11	22846268	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.7085232088257
12	3911076	intron_variant	1	MODIFIER	NA	PRKCZ	protein kinase C, zeta	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	18.696871285641
5	22777853	missense_variant	1	MODERATE	p.Arg165Ser	CTAGE5	CTAGE family, member 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	18.6678597705398
24	592578	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.6550583496015
9	6228693	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.508500082466
19	21043044	upstream_gene_variant	1	MODIFIER	NA	NA	S_TKc	GO:0002376-immune system process(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	18.4975432788251
19	32186499	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.4936860297394
15	29295215	missense_variant	1	MODERATE	p.Asn560Thr	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	18.4811906821034
5	21004791	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.4675213363821
19	22419463	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.4668917811063
7	13198601	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.4014426223962
18	10618809	missense_variant	1	MODERATE	p.Leu387Met	TIAM2	T-cell lymphoma invasion and metastasis 2	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	18.3786505778749
9	2524852	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	18.347604061891
20	21123719	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.2814558812204
2	6722802	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.2602193804847
7	38855699	missense_variant	1	MODERATE	p.Ser47Thr	STAG2	Stromal antigen 2	NA	18.2531799059793
15	12212265	intron_variant	2	MODIFIER	NA	NA	Nuclear envelope localisation domain	NA	18.19747074764
24	5353212	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.1704782621802
10	885827	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.1681300993874
7	2063293	intron_variant	1	MODIFIER	NA	DHX33	DEAH (Asp-Glu-Ala-His) box polypeptide 33	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	18.1657035529989
18	17712895	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.1473451663508
11	35537194	upstream_gene_variant	1	MODIFIER	NA	METTL23	methyltransferase like 23	NA	18.1443986710773
7	7031522	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.1394404673973
25	6546314	intron_variant	1	MODIFIER	NA	NA	NA	NA	18.1356444836996
2	17584333	intergenic_region	2	MODIFIER	NA	NA	NA	NA	18.1203858290361
NA	10004	intergenic_region	16	MODIFIER	NA	NA	NA	NA	18.1201950319471
13	19154333	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.1083414722162
8	8654852	upstream_gene_variant	2	MODIFIER	NA	TAL1	T-cell acute lymphocytic leukemia	GO:0002376-immune system process(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	18.0610739207157
20	16109175	downstream_gene_variant	1	MODIFIER	NA	GCA	grancalcin, EF-hand calcium binding protein	GO:0006880-intracellular sequestering of iron ion(L=10),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0042994-cytoplasmic sequestering of transcription factor(L=11),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),GO:1901385-regulation of voltage-gated calcium channel activity(L=10),	18.0579594635731
7	13402401	intergenic_region	1	MODIFIER	NA	NA	NA	NA	18.0553509305161
16	12756988	missense_variant	1	MODERATE	p.Ala2263Thr	LRP2	low density lipoprotein receptor-related protein 2	GO:0032501-multicellular organismal process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	18.0364714576084
11	51303	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	18.0044586109773
4	13689098	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.9997805524379
2	21880097	intron_variant	1	MODIFIER	NA	ANKRD11	ankyrin repeat domain 11	NA	17.9949746199154
4	23572452	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.9795032728622
19	21916074	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.9792263668042
25	2134054	intron_variant	3	MODIFIER	NA	NA	NA	NA	17.9743460211163
14	31687760	intron_variant	1	MODIFIER	NA	LAMC3	laminin, gamma 3	NA	17.9639101656956
15	8367316	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.9470220442054
16	8874561	intron_variant	1	MODIFIER	NA	NA	amyloid beta (A4) precursor protein	GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	17.940746555051
4	24930904	synonymous_variant	1	LOW	p.Gly158Gly	NA	NA	NA	17.9382129376115
24	16349643	intron_variant	1	MODIFIER	NA	RELL1	RELT-like 1	NA	17.9296519231223
24	12962301	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.8919932038935
25	3838235	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.8661296702999
20	24630117	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.8651619730748
5	1710565	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.8534892840174
19	8412302	synonymous_variant	1	LOW	p.Thr751Thr	NA	adenosine monophosphate deaminase 3	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.8290591692715
13	30407358	upstream_gene_variant	1	MODIFIER	NA	NA	AE binding protein 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0022610-biological adhesion(L=1),GO:0044699-single-organism process(L=1),	17.7878617825857
2	73356	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.7773758460118
25	229058	intron_variant	1	MODIFIER	NA	PRPF19	PRP19 PSO4 pre-mRNA processing factor 19 homolog (S. cerevisiae)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	17.7689745082491
19	27896126	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.7534155847356
11	6491569	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.7415004207509
24	11484057	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.7390000676213
14	26448352	upstream_gene_variant	1	MODIFIER	NA	RBPMS2	RNA binding protein with multiple splicing 2	NA	17.7250793779287
4	13739907	intron_variant	2	MODIFIER	NA	NA	NA	NA	17.7197688325213
12	23532826	synonymous_variant	2	LOW	p.Arg138Arg	NA	6-phosphofructo-2-kinase fructose-2,6-biphosphatase 2	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.7192886255304
2	22779112	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.6640977885425
6	9725153	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.6453955562746
6	26534985	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.611536810161
2	25117162	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.5987729745248
24	11196667	synonymous_variant	1	LOW	p.Thr1182Thr	ZNF219	zinc finger protein 219	NA	17.5875567668575
1	17750272	upstream_gene_variant	1	MODIFIER	NA	NA	Clathrin, heavy-chain linker	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	17.5777528423557
3	13647951	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.5478004077568
21	1226919	intergenic_region	16	MODIFIER	NA	NA	NA	NA	17.5343776858169
13	11593323	intron_variant	1	MODIFIER	NA	NA	NA	NA	17.5236358176134
23	14150759	intron_variant	1	MODIFIER	NA	CAPRIN2	Caprin family member 2	GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0048814-regulation of dendrite morphogenesis(L=11),GO:0050773-regulation of dendrite development(L=10),GO:0050775-positive regulation of dendrite morphogenesis(L=12),GO:0060998-regulation of dendritic spine development(L=11),GO:0060999-positive regulation of dendritic spine development(L=12),GO:0061001-regulation of dendritic spine morphogenesis(L=12),GO:0061003-positive regulation of dendritic spine morphogenesis(L=13),GO:0065007-biological regulation(L=1),	17.519724400657
12	17799401	intron_variant	3	MODIFIER	NA	NA	NA	NA	17.5044743023308
24	10698533	synonymous_variant	2	LOW	p.Ser222Ser	NA	NA	NA	17.4764329604771
15	13537292	synonymous_variant	1	LOW	p.Thr144Thr	ASPG	asparaginase homolog (S. cerevisiae)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.4662796803584
19	14173532	synonymous_variant	1	LOW	p.Asp983Asp	POLG	polymerase (DNA directed), gamma	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.4604365979898
17	34501395	intron_variant	1	MODIFIER	NA	NA	NA	NA	17.4369972231318
11	20074174	intron_variant	2	MODIFIER	NA	NA	NA	NA	17.4244917092909
16	17521830	synonymous_variant	1	LOW	p.Thr29Thr	NA	NA	NA	17.4022441299396
25	2134053	intron_variant	3	MODIFIER	NA	NA	NA	NA	17.4020295592135
6	8506417	missense_variant	1	MODERATE	p.Gly758Arg	NA	Ankyrin repeat	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	17.3966160501848
11	21233989	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.3736561588785
19	43054871	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	17.3723958603929
3	23473753	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.3540662989889
21	29454437	intergenic_region	16	MODIFIER	NA	NA	NA	NA	17.3517524004717
8	29648006	upstream_gene_variant	1	MODIFIER	NA	TSTA3	Tissue specific transplantation antigen P35B	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.3482850193787
16	36872520	intergenic_region	3	MODIFIER	NA	NA	NA	NA	17.3448821956895
7	4040198	intron_variant	1	MODIFIER	NA	EBF2	Early B-cell factor	GO:0002376-immune system process(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051704-multi-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	17.344195089756
13	28256787	intron_variant	1	MODIFIER	NA	NA	solute carrier family 4, sodium bicarbonate cotransporter, member 5	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	17.3431658953033
20	7720535	intron_variant	1	MODIFIER	NA	NA	NA	NA	17.3428395470587
6	10371824	intron_variant	2	MODIFIER	NA	NA	sparc osteonectin, cwcv and kazal-like domains proteoglycan (testican) 2	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	17.3296212868159
17	15444149	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.3192743020029
8	22498303	missense_variant	3	MODERATE	p.Gln603Lys	NA	NA	NA	17.3158094099607
11	14090619	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.3031579858808
15	1897660	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.295600100326
24	2624844	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.2885746201613
6	6986734	intron_variant	1	MODIFIER	NA	NA	heparan sulfate (glucosamine) 3-O-sulfotransferase 6	GO:0008152-metabolic process(L=1),	17.2815369035409
19	43054839	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	17.2586151870662
2	4331088	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.2452132995742
23	8221068	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.2414626858284
21	14669004	intergenic_region	16	MODIFIER	NA	NA	NA	NA	17.239499111883
10	7866261	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.2150054197829
5	11074156	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.2066752438569
16	36872516	intergenic_region	3	MODIFIER	NA	NA	NA	NA	17.193425619665
17	41066963	synonymous_variant	2	LOW	p.Thr5Thr	NA	NA	NA	17.1790449878073
4	13739896	intron_variant	2	MODIFIER	NA	NA	ADAM metallopeptidase with thrombospondin type 1 motif, 2	GO:0008152-metabolic process(L=1),	17.1681765946998
24	1010660	intron_variant	2	MODIFIER	NA	NA	Calcium calmodulin-dependent protein kinase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	17.1675879239615
2	16964818	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.1448782057987
9	2807084	intergenic_region	1	MODIFIER	NA	NA	NA	NA	17.1377280783643
20	7834332	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	17.1365081604757
3	24876011	upstream_gene_variant	1	MODIFIER	NA	NA	Inherit from opiNOG: Transposable element tc1 transposase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	17.1303335533368

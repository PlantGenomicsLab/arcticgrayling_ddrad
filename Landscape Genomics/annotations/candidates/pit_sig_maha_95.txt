chrom	pos	annotation	gene_count	annotation_impact	HGVS.p	EggNOG.Predicted.Gene	EggNOG.Description	EggNOG.GO.Biological	maha
14	20195582	downstream_gene_variant	1	MODIFIER	NA	HAUS4	HAUS augmin-like complex, subunit 4	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	7.75767330522096
8	22498258	missense_variant	3	MODERATE	p.Ala588Thr	NA	NA	NA	7.38545030500146
5	23284860	upstream_gene_variant	1	MODIFIER	NA	SRBD1	S1 RNA binding domain 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	7.13648145501416
12	32821335	intergenic_region	1	MODIFIER	NA	NA	NA	NA	6.326663280059
6	7750498	intergenic_region	2	MODIFIER	NA	NA	NA	NA	6.29500603093553
4	31268889	missense_variant	1	MODERATE	p.Gln1482Lys	NA	NA	NA	6.18988837999981
11	15243795	synonymous_variant	1	LOW	p.Leu323Leu	STXBP2	Syntaxin binding protein 2	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	6.17437083537695
4	13739896	intron_variant	2	MODIFIER	NA	NA	ADAM metallopeptidase with thrombospondin type 1 motif, 2	GO:0008152-metabolic process(L=1),	6.00717896925005
8	27573029	missense_variant	1	MODERATE	p.Gln481His	NA	aldehyde dehydrogenase 9 family, member	GO:0008152-metabolic process(L=1),	6.00318555044844
4	13739907	intron_variant	2	MODIFIER	NA	NA	NA	NA	5.91461165265572
2	19023186	downstream_gene_variant	1	MODIFIER	NA	NA	DnaJ (Hsp40) homolog, subfamily A, member 2	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0050896-response to stimulus(L=1),	5.79183991434416
15	26493620	upstream_gene_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	5.72451072610697
1	4005533	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.6864859757101
21	1226919	intergenic_region	15	MODIFIER	NA	NA	NA	NA	5.40250326230464
24	10993764	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.3434957460888
2	14716067	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.3154716117169
5	21826441	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.21456769710759
16	27657794	intron_variant	1	MODIFIER	NA	ZNF385B	Zinc finger protein 385B	NA	5.1918276733062
10	28042917	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.17570088022001
1	26365252	intron_variant	1	MODIFIER	NA	STRN4	striatin, calmodulin binding protein 4	NA	5.16585026424378
7	1952053	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.12979219439032
19	7091927	upstream_gene_variant	1	MODIFIER	NA	ST7	Suppression of tumorigenicity 7	GO:0000003-reproduction(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040007-growth(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	5.07337510979672
15	10552672	intergenic_region	3	MODIFIER	NA	NA	NA	NA	5.07003058337033
3	4544007	synonymous_variant	1	LOW	p.His277His	SLC1A7	solute carrier family 1 (glutamate transporter), member	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	5.05833743133823
1	22970073	intergenic_region	1	MODIFIER	NA	NA	NA	NA	5.0529822234015
18	139879	downstream_gene_variant	1	MODIFIER	NA	ARL15	ADP-ribosylation factor-like 15	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	5.04191379481154
8	10190075	synonymous_variant	1	LOW	p.Val20Val	SASS6	spindle assembly 6 homolog (C. elegans)	GO:0000003-reproduction(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	4.91096240418376
13	10240880	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.88444805525836
21	8727007	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.87982317698392
12	32289492	downstream_gene_variant	1	MODIFIER	NA	NA	SPOC domain	NA	4.86450495419669
10	305491	downstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	4.84853976354893
18	17857496	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.84272902581793
10	305490	downstream_gene_variant	2	MODIFIER	NA	C1ORF216	Chromosome 1 open reading frame 216	NA	4.84113672502449
12	4395672	upstream_gene_variant	1	MODIFIER	NA	NA	STYKc	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.83743005985729
6	3659577	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.80437274518228
8	15995465	upstream_gene_variant	1	MODIFIER	NA	NA	calcium channel, voltage-dependent, R type, alpha 1E subunit	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	4.77488632587735
23	21444879	intron_variant	1	MODIFIER	NA	NA	Calcium calmodulin-dependent protein kinase	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.76988526209778
6	36673553	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.76041600029552
19	31920421	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.74867969401386
11	29445015	upstream_gene_variant	1	MODIFIER	NA	LRRC53	LRR_TYP	NA	4.74282210141669
8	37574092	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.73614925485124
19	22419463	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.69780522478948
15	7949175	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.69762080944933
4	26930634	downstream_gene_variant	1	MODIFIER	NA	SOX3	SRY (sex determining region Y)-box 3	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.67285440293328
14	21485448	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.64759309172627
22	5235681	intron_variant	3	MODIFIER	NA	NA	NA	NA	4.61474148228875
NA	4330	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.60018430845222
1	24058373	intron_variant	1	MODIFIER	NA	NA	spectrin, beta, non-erythrocytic 4	NA	4.57264209387197
2	14097773	downstream_gene_variant	1	MODIFIER	NA	TRAF6	TNF receptor-associated factor 6, E3 ubiquitin protein ligase	GO:0002376-immune system process(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0043507-positive regulation of JUN kinase activity(L=11),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051704-multi-organism process(L=1),GO:0065007-biological regulation(L=1),GO:0070534-protein K63-linked ubiquitination(L=10),	4.56365034569481
17	14128733	intron_variant	1	MODIFIER	NA	NA	FYVE, RhoGEF and PH domain containing 5	GO:0065007-biological regulation(L=1),	4.5483853579524
4	29035372	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.54129522671279
16	15337147	synonymous_variant	1	LOW	p.Ala352Ala	GTF2E1	general transcription factor IIE polypeptide 1 alpha 56kDa	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0065007-biological regulation(L=1),	4.50298529082084
10	5514324	upstream_gene_variant	1	MODIFIER	NA	NA	Cingulin	GO:0008152-metabolic process(L=1),	4.50258004412906
11	31091819	synonymous_variant	1	LOW	p.Tyr83Tyr	FNBP1	formin binding protein	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	4.4832561404764
17	41066958	missense_variant	4	MODERATE	p.Ser7Phe	NA	NA	NA	4.44124337864452
4	23572452	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.39609051285449
2	17070181	synonymous_variant	1	LOW	p.Leu89Leu	NA	NA	NA	4.39047501976421
5	31776088	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.38651396129245
16	7634197	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.37909706781115
10	797157	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.37901494842045
14	23600499	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.37148595364357
7	28317299	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.36677834723088
2	37537974	3_prime_UTR_variant	1	MODIFIER	NA	NA	DENN MADD domain containing 4A	NA	4.3253132685344
8	22498303	missense_variant	3	MODERATE	p.Gln603Lys	NA	NA	NA	4.31808556195682
6	801162	synonymous_variant	1	LOW	p.Val480Val	FBXO11	F-box protein 11	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	4.27816748245234
23	2626546	intron_variant	2	MODIFIER	NA	NA	NA	NA	4.2673657750719
12	12205884	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.2618505472952
8	24240990	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.25369899063981
10	797135	intergenic_region	2	MODIFIER	NA	NA	NA	NA	4.24299475388212
15	5968961	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.23580849449595
1	5242971	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.19126698022766
3	7463043	upstream_gene_variant	1	MODIFIER	NA	PTPRD	protein tyrosine phosphatase receptor type	GO:0002376-immune system process(L=1),GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0022610-biological adhesion(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0048814-regulation of dendrite morphogenesis(L=11),GO:0050773-regulation of dendrite development(L=10),GO:0050775-positive regulation of dendrite morphogenesis(L=12),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	4.18931979225567
21	10313050	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.18520663621307
3	24883116	intron_variant	1	MODIFIER	NA	FAM110B	family with sequence similarity 110, member B	NA	4.18113205623272
NA	656	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.16885605790806
6	8963775	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.14120151701244
1	11682222	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.12185002297442
2	25230010	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.11650714203276
21	22191571	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.11571250924085
17	37214337	upstream_gene_variant	1	MODIFIER	NA	NA	ArfGAP with coiled-coil, ankyrin repeat and PH domains	GO:0065007-biological regulation(L=1),	4.11339170577963
6	5842378	synonymous_variant	1	LOW	p.Ser939Ser	NA	NA	NA	4.10810208216477
11	6479002	intron_variant	1	MODIFIER	NA	NA	cytochrome P450, family 2, subfamily W, polypeptide 1	GO:0008152-metabolic process(L=1),	4.10484330340739
17	16221088	upstream_gene_variant	1	MODIFIER	NA	UBE2R2	ubiquitin-conjugating enzyme E2R 2	GO:0008152-metabolic process(L=1),	4.10111114079566
23	22810470	upstream_gene_variant	1	MODIFIER	NA	NA	protein phosphatase 6, regulatory subunit	NA	4.09957305511284
7	10663631	missense_variant	1	MODERATE	p.Gln55His	NA	Immunoglobulin V-set domain	NA	4.08856222364878
10	15030463	synonymous_variant	1	LOW	p.His56His	NA	NA	NA	4.08292541014529
12	7842211	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.07635522592334
14	24155219	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.06170019417948
5	7119732	intron_variant	1	MODIFIER	NA	PDE6C	Phosphodiesterase 6C, cGMP-specific, cone, alpha prime	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	4.05714674968476
19	11898656	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.05687932916562
21	18446994	intergenic_region	15	MODIFIER	NA	NA	NA	NA	4.05547351436194
7	14831127	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.04909398431427
14	18108583	upstream_gene_variant	2	MODIFIER	NA	NA	NA	NA	4.04436364913474
24	592578	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.04404293040667
11	13628055	intergenic_region	1	MODIFIER	NA	NA	NA	NA	4.03613441137648
14	13559819	intron_variant	1	MODIFIER	NA	NA	NA	NA	4.0234639219865
10	2571621	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	4.01555109933301
1	24879365	synonymous_variant	1	LOW	p.Tyr130Tyr	CCNO	Cyclin, N-terminal domain	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	4.01467435395713
1	17750272	upstream_gene_variant	1	MODIFIER	NA	NA	Clathrin, heavy-chain linker	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	4.01236422447455
6	8784764	3_prime_UTR_variant	1	MODIFIER	NA	NCOA4	nuclear receptor coactivator 4	NA	3.99897325092864
6	9725153	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.99390957679931
12	11072635	synonymous_variant	1	LOW	p.Leu747Leu	ATP9A	ATPase, class II, type	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.97158846342768
7	20400605	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.96843100446956
2	25117162	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.96170619401236
11	29501901	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.95620260842487
15	18261642	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.95389442058391
3	1585958	intron_variant	1	MODIFIER	NA	SNTG1	syntrophin, gamma 1	NA	3.94868313825946
24	16349643	intron_variant	1	MODIFIER	NA	RELL1	RELT-like 1	NA	3.9283042864523
NA	587	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.92464441257025
15	10552702	intergenic_region	3	MODIFIER	NA	NA	NA	NA	3.92429366527968
5	7225922	downstream_gene_variant	1	MODIFIER	NA	FAM190B	family with sequence similarity 190, member B	NA	3.92091121004358
5	15935185	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.91206479278015
13	5638740	missense_variant	1	MODERATE	p.Arg145Cys	KIF24	kinesin family member 24	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	3.91150866814665
21	3822864	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.90755062720789
11	10496502	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.90694179099276
16	16221397	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.90655021870303
17	7518363	intron_variant	2	MODIFIER	NA	NA	SLIT-ROBO Rho GTPase activating protein 3	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.90377487821993
12	9137502	intron_variant	1	MODIFIER	NA	NA	ArfGAP with coiled-coil, ankyrin repeat and PH domains	GO:0065007-biological regulation(L=1),	3.90239724678331
11	9559550	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.89663026247611
14	18066270	synonymous_variant	2	LOW	p.Asp716Asp	NA	NA	NA	3.89450840611406
5	19150303	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.89219763462143
20	747636	missense_variant	1	MODERATE	p.Leu511Met	PHF20L1	PHD finger protein 20-like 1	NA	3.88938123039437
3	19070825	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.88895998041343
21	23058310	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.87487458373159
5	21265366	synonymous_variant	1	LOW	p.Val113Val	CNTD1	cyclin N-terminal domain containing 1	NA	3.8731982222406
14	25622738	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.87030721809256
1	10790772	upstream_gene_variant	1	MODIFIER	NA	RPS25	ribosomal protein S25	NA	3.86485648305034
23	3047713	intron_variant	1	MODIFIER	NA	IDH3A	isocitrate dehydrogenase 3 (NAD ) alpha	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.85812732274841
3	22378478	intron_variant	1	MODIFIER	NA	LAMA3	laminin, alpha 3	GO:0009987-cellular process(L=1),GO:0022610-biological adhesion(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	3.85757728180855
23	17884259	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.85356555322953
8	22498246	missense_variant	3	MODERATE	p.Lys584Glu	NA	Chromosome undetermined SCAF14724, whole genome shotgun sequence	NA	3.84951065289554
2	25417630	intron_variant	1	MODIFIER	NA	EXT2	exostosin 2	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0030516-regulation of axon extension(L=11),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0050770-regulation of axonogenesis(L=10),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	3.84634979783433
13	7624439	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.84289343100247
11	3061900	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.84191152708703
13	5939452	upstream_gene_variant	1	MODIFIER	NA	NAA35	N-alpha-acetyltransferase 35, NatC auxiliary subunit	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	3.83441813323141
6	7750376	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.83372759611787
8	14239339	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.822382993436
12	18582901	missense_variant	1	MODERATE	p.Trp33Cys	IFT122	intraflagellar transport 122 homolog (Chlamydomonas)	NA	3.80774000506136
10	19381299	intron_variant	1	MODIFIER	NA	NA	CUB and Sushi multiple domains 3	NA	3.8065016182629
19	34255189	intron_variant	1	MODIFIER	NA	NA	pleckstrin homology domain containing, family A member 7	NA	3.80595134969459
19	1819605	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.79588271970001
19	27896126	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.78465003131273
15	10552849	intergenic_region	3	MODIFIER	NA	NA	NA	NA	3.77572115995872
9	1490189	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.7627279257129
11	3061610	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.76254190204487
4	4720615	upstream_gene_variant	1	MODIFIER	NA	NA	Inherit from KOG: Retrotransposon protein	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.7614495365842
NA	239	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.75888537291691
18	6681906	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.7449233235986
12	25255530	intron_variant	2	MODIFIER	NA	MTOR	mechanistic target of rapamycin (serine threonine kinase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	3.742931725509
4	14105266	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.7338995863006
12	25255531	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.73052160624756
17	22469477	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.72911670102909
2	3138628	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.72564896352743
6	36898380	intron_variant	1	MODIFIER	NA	NA	SH3 and PX domains 2A	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	3.72253552177181
2	4653094	upstream_gene_variant	1	MODIFIER	NA	SLC12A1	solute carrier family 12 (sodium potassium chloride transporters), member 1	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.72054887081289
15	13226901	upstream_gene_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.71589446664019
10	28761790	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.70077778057327
19	26245247	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.70060816050136
12	11503762	upstream_gene_variant	1	MODIFIER	NA	NA	Formin Homology 2 Domain	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	3.68988736137033
13	13662466	intron_variant	1	MODIFIER	NA	ENTPD2	ectonucleoside triphosphate diphosphohydrolase 2	GO:0008152-metabolic process(L=1),GO:0050896-response to stimulus(L=1),	3.68071757681629
18	5085921	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.67765001517792
22	5235680	intron_variant	3	MODIFIER	NA	NA	Ezrin/radixin/moesin family	NA	3.66375569236226
3	30235458	upstream_gene_variant	1	MODIFIER	NA	NA	peroxiredoxin 6	GO:0008152-metabolic process(L=1),	3.65761368542906
4	17728159	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.6576020993415
14	20712712	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.65638383815711
12	1678940	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.65188611454712
6	10371824	intron_variant	1	MODIFIER	NA	NA	sparc osteonectin, cwcv and kazal-like domains proteoglycan (testican) 2	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.65094118745751
4	14105008	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.64820495083266
20	23374176	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.64517303417633
3	1210591	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.64475802351321
19	4645328	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.6411080333505
7	6140244	missense_variant	1	MODERATE	p.Pro161His	NA	NA	NA	3.63839937161207
15	12212265	intron_variant	1	MODIFIER	NA	NA	Nuclear envelope localisation domain	NA	3.6316970011164
1	5043383	intron_variant	2	MODIFIER	NA	CTAGE5	CTAGE family, member 5	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),	3.62395202204147
10	14273813	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.62233504623416
13	20644873	intron_variant	1	MODIFIER	NA	GPR124	G protein-coupled receptor 124	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.61766595879124
19	27337145	intron_variant	1	MODIFIER	NA	NA	homer homolog 3 (Drosophila)	NA	3.61274370147689
24	11484057	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.61273020522223
20	18996560	intron_variant	1	MODIFIER	NA	NA	ATPase, H transporting, lysosomal 42kDa, V1 subunit C1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.60439144086392
6	8506417	missense_variant	1	MODERATE	p.Gly758Arg	NA	Ankyrin repeat	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.60366208126626
5	15935167	intron_variant	2	MODIFIER	NA	NCR3LG1	natural killer cell cytotoxicity receptor 3 ligand 1	NA	3.59997669000637
17	41066750	intron_variant	4	MODIFIER	NA	PLTP	phospholipid transfer protein	NA	3.59516130746233
24	6044353	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.59198051860548
18	26730324	intron_variant	1	MODIFIER	NA	TNFRSF21	tumor necrosis factor receptor superfamily, member 21	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.58671359371572
8	15812651	synonymous_variant	1	LOW	p.Val79Val	NA	NA	NA	3.5854201134319
8	21764082	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.57677409405314
17	19993434	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.57644242942171
2	33129395	intron_variant	1	MODIFIER	NA	IQGAP1	IQ motif containing GTPase activating protein 1	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.57467707544213
18	22676836	synonymous_variant	1	LOW	p.Ser267Ser	CDC42BPA	CDC42 binding protein kinase alpha (DMPK-like)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	3.57406956559712
2	4625664	synonymous_variant	1	LOW	p.Arg385Arg	MYEF2	Myelin expression factor 2	NA	3.57260722783175
17	7518380	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.5668774361224
21	1684616	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.56391717385926
20	17399654	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.55866827025325
7	28683578	intron_variant	1	MODIFIER	NA	RAI14	retinoic acid induced 14	NA	3.55440192318071
24	12941950	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.54898885606388
13	15962961	intron_variant	1	MODIFIER	NA	NA	DAB2 interacting protein	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.54707570870821
11	24480713	upstream_gene_variant	1	MODIFIER	NA	TMEM184A	Transmembrane protein 184a	NA	3.54332250161616
17	41066763	intron_variant	4	MODIFIER	NA	NA	NA	NA	3.53625720336288
3	9417482	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.52473359479386
12	14399959	synonymous_variant	1	LOW	p.Ser122Ser	VSTM2L	V-set and transmembrane domain containing 2 like	NA	3.52332998212329
4	16704421	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.52274487524934
1	10564853	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.52137287228785
17	41066807	intron_variant	4	MODIFIER	NA	NA	NA	NA	3.51372398763953
17	12406385	intron_variant	1	MODIFIER	NA	GNL3L	Guanine nucleotide binding protein-like 3 (Nucleolar)-like	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	3.50884169306068
8	21737926	stop_gained	1	HIGH	p.Gln106*	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.50789809001529
6	21246280	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.50612059028459
1	5043384	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.50548506330826
14	8632755	upstream_gene_variant	1	MODIFIER	NA	KAT5	K(lysine) acetyltransferase 5	GO:0008152-metabolic process(L=1),GO:0065007-biological regulation(L=1),	3.50089891866559
21	3822866	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.50072474890491
6	10722286	intron_variant	1	MODIFIER	NA	HSPA12A	heat shock 70kDa protein 12A	NA	3.49953529591298
17	19570089	upstream_gene_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.49850103500112
19	15755169	intron_variant	1	MODIFIER	NA	PDHX	pyruvate dehydrogenase complex, component X	GO:0008152-metabolic process(L=1),	3.49384689438829
11	8414773	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.49277215862302
10	1707549	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.49177712525786
17	5440923	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.48967471027587
23	2626547	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.4896261469145
6	10201538	synonymous_variant	1	LOW	p.Val1505Val	PLCE1	phospholipase C, epsilon 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.48956977661696
4	16704222	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.48895814482492
11	11735301	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.48033146610039
8	17617016	intron_variant	1	MODIFIER	NA	NA	Pfam:GumN	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.47776863592325
19	26003593	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.46831198165467
22	2936266	downstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.46830789142772
21	6594413	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.46094525281712
14	2648478	synonymous_variant	1	LOW	p.Leu900Leu	PDS5B	PDS5, regulator of cohesion maintenance, homolog B (S. cerevisiae)	NA	3.46063425511359
13	16897245	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.45826669972296
12	18725068	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.45380066751437
1	19493737	downstream_gene_variant	1	MODIFIER	NA	BRIP1	BRCA1 interacting protein C-terminal helicase 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.44999681920194
4	21875367	synonymous_variant	2	LOW	p.Ala647Ala	NA	NA	NA	3.44865393078148
15	31841642	synonymous_variant	1	LOW	p.Leu50Leu	ORC3	origin recognition complex, subunit 3	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.447716723007
1	24499223	upstream_gene_variant	1	MODIFIER	NA	NA	NA	NA	3.44122809881364
10	10133406	upstream_gene_variant	1	MODIFIER	NA	NA	teashirt zinc finger homeobox 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	3.43692862801598
2	18650668	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.43428561710077
2	30027567	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.43415577266207
10	1707522	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.42498125766015
12	21869840	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.41941910998458
14	6201779	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.41739452424086
22	5235687	intron_variant	3	MODIFIER	NA	NA	NA	NA	3.4157533286236
12	21456242	intron_variant	1	MODIFIER	NA	CDH26	cadherin 26	GO:0009987-cellular process(L=1),GO:0022610-biological adhesion(L=1),GO:0044699-single-organism process(L=1),	3.41229695341503
7	26106973	synonymous_variant	1	LOW	p.Thr47Thr	NA	NA	NA	3.41039470614561
13	4644271	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.40962090567265
15	11684435	downstream_gene_variant	1	MODIFIER	NA	ZNF862	Zinc finger protein 862	GO:0065007-biological regulation(L=1),	3.40939943405272
19	19306391	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.40393564778384
7	13402410	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.40225180116707
15	16613754	synonymous_variant	2	LOW	p.Arg366Arg	NA	NA	NA	3.39793175654355
16	31336809	3_prime_UTR_variant	1	MODIFIER	NA	NA	SATB homeobox 2	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0065007-biological regulation(L=1),	3.39001027929994
5	16432498	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.38675479026368
17	23766455	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.38593750257924
4	21839440	intron_variant	2	MODIFIER	NA	ATP8A1	ATPase, aminophospholipid transporter (APLT), class I, type 8A, member 1	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.38111567102991
6	7263955	missense_variant	1	MODERATE	p.Arg518His	RHBDF1	rhomboid 5 homolog 1 (Drosophila)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),GO:0040011-locomotion(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	3.37968425213872
18	20355984	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.37794158030891
12	28639624	missense_variant	1	MODERATE	p.Ser1439Ile	LAMB2	laminin, beta 2 (laminin S)	GO:0009987-cellular process(L=1),GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),GO:0071840-cellular component organization or biogenesis(L=1),	3.37512184173783
6	724314	intron_variant	1	MODIFIER	NA	NA	Reverse transcriptase (RNA-dependent DNA polymerase)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.3698747892338
12	13481434	synonymous_variant	1	LOW	p.Thr513Thr	NA	Rac GTPase activating protein 1	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.36863035400798
14	16608090	intron_variant	1	MODIFIER	NA	NA	protein phosphatase 2, regulatory subunit B'', alpha	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.36691584018439
5	23586692	upstream_gene_variant	1	MODIFIER	NA	ANKRD1	ankyrin repeat domain 1 (cardiac muscle)	NA	3.36293730715398
5	20727277	downstream_gene_variant	1	MODIFIER	NA	NA	Sulfotransferase domain	GO:0008152-metabolic process(L=1),	3.36165074341464
12	17799294	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.36079780972099
10	4473857	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.35993135016596
19	13060733	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.35722791499777
21	6919144	intergenic_region	15	MODIFIER	NA	NA	NA	NA	3.35423800661029
20	18517220	intron_variant	1	MODIFIER	NA	LIMD1	LIM domains containing 1	NA	3.35117081882146
17	16608260	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.35055975215226
3	17582103	intron_variant	1	MODIFIER	NA	KNCN	Inherit from opiNOG: kinocilin	NA	3.34377954135982
4	26161092	intron_variant	1	MODIFIER	NA	GFRA3	GDNF family receptor alpha 3	GO:0032501-multicellular organismal process(L=1),GO:0032502-developmental process(L=1),GO:0044699-single-organism process(L=1),	3.33569985689345
18	20218603	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.33557087406741
7	26954885	intron_variant	1	MODIFIER	NA	NA	Pfam:efhand	NA	3.33428140220029
23	10664292	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.33255931384856
6	10107254	intron_variant	1	MODIFIER	NA	NA	solute carrier family 2, facilitated glucose transporter member	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),	3.32937950869253
1	22555763	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.32909248771423
18	24233793	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.32682279826781
24	17850088	downstream_gene_variant	1	MODIFIER	NA	FRRS1	Reeler domain	NA	3.3266581110483
16	5192652	intron_variant	1	MODIFIER	NA	LRRFIP1	leucine rich repeat (in FLII) interacting protein	NA	3.32041285818825
12	23715215	upstream_gene_variant	1	MODIFIER	NA	TTI1	TELO2 interacting protein 1	NA	3.31944080462814
3	6534825	downstream_gene_variant	1	MODIFIER	NA	NDUFA13	NADH dehydrogenase (ubiquinone) 1 alpha subcomplex, 13	NA	3.31908264044463
2	5066404	intron_variant	1	MODIFIER	NA	NA	malignant fibrous histiocytoma-amplified sequence	GO:0009987-cellular process(L=1),GO:0023052-signaling(L=1),GO:0044699-single-organism process(L=1),GO:0050896-response to stimulus(L=1),GO:0065007-biological regulation(L=1),	3.31740470359484
7	21769611	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.31637670032844
6	26534985	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.3161706073889
15	16613664	synonymous_variant	2	LOW	p.Thr396Thr	ASPG	asparaginase homolog (S. cerevisiae)	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.31327165571607
11	12582876	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.31298729077252
14	13106386	downstream_gene_variant	1	MODIFIER	NA	GLIS3	GLIS family zinc finger 3	NA	3.31215610762225
5	16432508	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.31181714176556
1	22555885	intergenic_region	2	MODIFIER	NA	NA	NA	NA	3.31091891870669
14	12456701	downstream_gene_variant	1	MODIFIER	NA	NA	1-acylglycerol-3-phosphate O-acyltransferase 9	GO:0008152-metabolic process(L=1),GO:0009987-cellular process(L=1),	3.30849517786897
14	22145505	intron_variant	1	MODIFIER	NA	NA	NA	NA	3.30770186594247
14	17270412	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.30477395162126
23	5172123	intergenic_region	1	MODIFIER	NA	NA	NA	NA	3.30018168650721
17	18890887	intron_variant	1	MODIFIER	NA	ARHGEF10L	Rho guanine nucleotide exchange factor (GEF) 10-like	GO:0065007-biological regulation(L=1),	3.29560072499609
24	15417599	downstream_gene_variant	1	MODIFIER	NA	CLCN3	chloride channel, voltage-sensitive 3	GO:0009987-cellular process(L=1),GO:0044699-single-organism process(L=1),GO:0051179-localization(L=1),GO:0065007-biological regulation(L=1),	3.29263382159114
17	19993417	intron_variant	2	MODIFIER	NA	NA	NA	NA	3.29008499426287
12	32061322	missense_variant	1	MODERATE	p.Ala61Val	CSDE1	cold shock domain containing E1, RNA-binding	GO:0065007-biological regulation(L=1),	3.28694784799529
20	22912882	downstream_gene_variant	1	MODIFIER	NA	EVA1B	eva-1 homolog B (C. elegans)	NA	3.28510389525877

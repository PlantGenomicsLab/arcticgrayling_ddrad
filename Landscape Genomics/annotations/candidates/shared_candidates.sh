#!/bin/bash
###############
#  Shared     #
#  Candidates #
###############

#### Collect Gene Names ------------------------------------------
rm all_maha_95.txt

for i in lat pit temp temp2;
do

	echo "------------------------------------------------------"
	echo "$i"
	echo "------------------------------------------------------"
	# Count of GO terms
	awk -F "\t" '{print $9}' ${i}_sig_maha_95.txt | tr "," "\n"| sort | uniq -c | sort > ${i}_GO.sum

	# List of GO Terms Present (lvl1)
	cat ${i}_GO.sum | cut -d"-" -f 2- | tail -n +2 | head -n -2 | grep "(L=1)" | while read line;

	do
	#for t in $GO;
	#do

		GO_prefix=$(echo $line | sed -e 's/ /_/g' -e 's/(L=1)//')
		#echo  "-----------------------------------------"
		#echo "$GO_prefix"
		#echo "------------------------------------------"
		grep "${line}" ${i}_sig_maha_95.txt | awk -F "\t" '{print $10"\t"$7"\t"$8"\t"$5"\t"$3}' | sort  > ${i}_${GO_prefix}.list

	done

	#cat "{i}_${GO_prefix}.out"
	#awk 'NR==FNR{a[$1];next} $1 in a' temp_${GO_prefix} temp2_${GO_prefix

	awk -F "\t" '{print $1"\t"$2"\t"$7"\t"$8"\t"$3}' ${i}_sig_maha_95.txt >> all_maha_95.txt

	#awk -F "\t" '{print $8}' ${i}_sig_maha_95.txt | sort | uniq -c | sort > all_maha_95_shared.txt

done

#cat all_maha_95.txt | sort | uniq -c | sort > all_maha_95_snps_shared.txt
awk -F "\t" '{print $1"\t"$2"\t"$3"\t"$4}' all_maha_95.txt | sort -k1,2 | uniq -c | sort > all_maha_95_genes_shared.txt

# subset uniq calls
awk '$1 == 1 {print $2"\t"$3}' all_maha_95_snps_shared.txt | sort > all_maha_95_snps_uniq.txt

# Merge based on shared values in column 1 and 2

for i in lat pit temp temp2;
do
	sort ${i}_sig_maha_95.txt > ${i}_sig_maha_95.sorted
	awk -F "\t" 'NR==FNR{a[$1,$2];next}($1, $2) in a' all_maha_95_snps_uniq.txt ${i}_sig_maha_95.sorted > ${i}_sig_maha_95_uniq.txt

done
# In Case of an Emergency
#rm *_metabolic





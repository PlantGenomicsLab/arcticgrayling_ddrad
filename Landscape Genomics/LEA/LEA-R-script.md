
#!/bin/Rscript

library("LEA", lib="~/local/R_libs/")
library("ggplot2")

#############
# STRUCTURE # ------------------------------------
#############
## Determine number of Latent Factors (K)

## PRUNED DATASET
pruned_ped <- "LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.pruned.ped"

pruned_geno <- ped2geno(pruned_ped, output.file="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.pruned.geno",force=FALSE)
pruned_lfmm <- ped2lfmm(pruned_ped, output.file="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.pruned.lfmm", force=FALSE)

# CREATE PCAPROJECT
pc <- pca(pruned_lfmm)
# calculate tracy widom test
tw <- tracy.widom(pc)
# graph percentage of variance explained per significant component
PC_plot <- plot(tw$percentage)

# CREATE SNMF
obj.snmf <- snmf(pruned_geno, K = 1:15, entropy = T, ploidy = 2, project="new", alpha= 50, tolerance = 0.0001, repetitions = 5, iterations = 100000, CPU = 10)
# plot cross-validation error all K runs
CV_plot <- plot(obj.snmf, col = "blue4", cex = 1.4, pch = 13)

# PLOT TW & CV
png(filename="STRUCTURE_PLOTS.png")
par(mfrow=c(2,1))
PC_plot
CV_plot
dev.off()

############
# GWAS LAT # ------------------------------------
############
# FULL DATASET
ped <- "LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.ped"
lfmm <- ped2lfmm(ped, output.file="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.lfmm", force=FALSE)

# IMPUTE
best <- which.min(cross.entropy(obj.snmf, K = 5))
impute(obj.snmf, "LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.lfmm", method = "mode", K = 5, run = best)
# read imputed lfmm dataset into environment
dat.imp <- read.lfmm("LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.RIND.lfmm_imputed.lfmm")

# LFMM
grayling_project <- lfmm("LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.RIND.lfmm_imputed.lfmm", "grayling.env", K = 5:10, repetitions = 10, project = "new", iterations = 100000, burnin = 50000, CPU = 20, missing.data = FALSE)
 
# INFLATION FACTOR
GIF <- NULL
for (i in c(5,6,7,8,9)) {
        zs.table <- z.scores(grayling_project,K=i)
        zs <- apply(zs.table, MARGIN = 1, median)
        lambda <- median(zs^2)/.456
	GIF <- rbind(GIF,data.frame(i,lambda))	
	print(paste0("K = ",i," lambda = ",lambda))
	}

# LFMM P-VALUES
pv <- lfmm.pvalues(grayling_project, K = GIF[which.min(GIF$lambda),1])
EA_pvalues <- pv$pvalues
write.table(x = EA_pvalues, file = "LEA_EA_pvalues.txt",quote = F,row.names = F,col.names=F)

# LFMM 2 
grayling_project2 <- lfmm2(input="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.RIND.lfmm_imputed.lfmm",env="grayling.env",K = GIF[which.min(GIF$lambda),1],lambda=.00001)
pv2 <- lfmm2.test(object=grayling_project2,input=dat.imp,env="grayling.env",linear=TRUE)

###############
# OUTLIER FST # -----------------------------
###############
# CALC P-VALUES
p <- snmf.pvalues(obj.snmf, entropy=T,ploidy=2,K=GIF[which.min(GIF$lambda),1])
GD_pvalues <- p$pvalues
write.table(x = GD_pvalues, file = "LEA_GD_pvalues.txt",quote = F,row.names = F,col.names = F)


##########################
# Evaluate FDR and POWER # ------------------
##########################
for (alpha in c(.05,.1,.15,.2)) {
  # expected FDR
  print(paste("expected FDR:", alpha))
  L = length(pv$pvalues)
  # Benjamini-Hochberg's method for an expected FDR = alpha.
  w = which(sort(pv$pvalues) < alpha * (1:L)/L)
  candidates = order(pv$pvalues)[w]

  # estimated FDR and True Positive Rate
  # The targets SNPs are loci 351 to 400
  Lc = length(candidates)
  estimated.FDR = length(which(candidates <= 350))/Lc
  estimated.TPR = length(which(candidates > 350))/50
  print(paste("FDR:", estimated.FDR, "True Positive Rate:", round(estimated.TPR, digits = 2)))
}

###################
# Genetic Offsets # -------------------------
###################

